<?php
    $models = ["Player","Card","GM","GameStatus","Tabs","WinnerRequest","WinningPattern","Message"];

    class Player
    {
        public $table = "player";
        public $fillable = [
            "pid",
            "name",
            "email",
            "codename",
            "refresh",
            "amount",
            "bet"
        ];
    }

    class Card
    {
        public $table = "card";
        public $fillable = [
            "cid",
            "pid",
            "ra",
            "rb",
            "rc",
            "rd",
            "re"
        ];
    }

    class GM
    {
        public $table = "gm";
        public $fillable = [
            "gmid",
            "name",
            "username",
            "password"
        ];
    }

    class GameStatus
    {
        public $table = "gamestatus";
        public $fillable = [
            "status",
            "tab",
            "jackpot",
            "rate"
        ];
    }

    class Tabs
    {
        public $table = "tabs";
        public $fillable = [
            "tabs"
        ];
    }

    class WinnerRequest
    {
        public $table = "winnerrequest";
        public $fillable = [
            "wid",
            "cid",
            "pid",
            "status"
        ];
    }

    class WinningPattern
    {
        public $table = "winningpattern";
        public $fillable = [
            "wpid",
            "r0",
            "r1",
            "r2",
            "r3",
            "r4",
        ];
    }

    class Message
    {
        public $table = "message";
        public $fillable = [
            "pid",
            "message"
        ];
    }
?>