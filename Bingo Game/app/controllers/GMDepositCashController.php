<?php
    include('../../vendor/invoker/invoke.php');
    class GMDepositCashController{
        public static function index(){
            Route::view("gm.depositcash");
        }
        public static function store(){
            //code here...
        }
        public static function show(){
            $player = new Player;
            $playerdata = DB::where($player,"codename","=",$_POST["codename"]);
            if(!count($playerdata)){
                Data::load("Message","Codename Not Found.");
            }
            Data::load("transaction",$playerdata);
            Route::index("GMDepositCashController");
        }
        public static function update(){
            $player = new Player;
            $playertemp = DB::prepare($player, Data::unload("transaction")[0]["id"]);
            $playertemp->amount = Data::unload("transaction")[0]["amount"] + $_POST["amount"];
            DB::update($playertemp);
            Data::load("Message","Balance Added.");
            header("location: GMHomeController?show");
        }
        public static function destroy(){
            //code here...
        }
    }
?>