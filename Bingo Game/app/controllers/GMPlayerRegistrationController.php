<?php
    include('../../vendor/invoker/invoke.php');
    class GMPlayerRegistrationController{
        public static function index(){
            Route::view("gm.registration");
        }
        public static function store(){
            $player = new Player;
            $player->pid = uniqid();
            $player->name = $_POST["name"];
            $player->email = $_POST["email"];
            $player->codename = $_POST["codename"];
            $player->amount = $_POST["amount"];
            $player->bet = 0;
            $player->refresh = "false";
            if(DB::validate($player,"codename",$_POST["codename"])){
                DB::save($player);    
                header("location: GMHomeController?show");
            }else{
                Data::load("Message","Codename Already Existed.");
                Route::index("GMPlayerRegistrationController");
            }
        }
        public static function show(){
            //code here...
        }
        public static function update(){
            //code here...
        }
        public static function destroy(){
            //code here...
        }
    }
?>