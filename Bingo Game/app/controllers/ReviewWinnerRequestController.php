<?php
    include('../../vendor/invoker/invoke.php');
    class ReviewWinnerRequestController{
        public static function index(){
            $wr = new WinnerRequest;
            $wrdata = DB::all($wr);

            $player = new Player;
            $playerdata = DB::all($player);

            $card = new Card;
            $carddata = DB::all($card);

            $tab = new Tabs;
            $tabdata = DB::all($tab);

            $gs = new GameStatus;
            $gsdata = DB::all($gs);

            $tabdatatemp = [];

            /**Get last ball from game status */
            $gstab = "";
            $gstabtemp = str_split($gsdata[0]["tab"]);
            array_shift($gstabtemp);
            for ($i=0; $i < count($gstabtemp); $i++) { 
                $gstab .= $gstabtemp[$i];
            }
            array_push($tabdatatemp, $gstab);

            /**Get all the previous ball and removing letter */
            foreach($tabdata as $tab){
                $temp = "";
                $tabtemp = str_split($tab["tabs"]);
                array_shift($tabtemp);
                for ($i=0; $i < count($tabtemp); $i++) { 
                    $temp .= $tabtemp[$i];
                }
                array_push($tabdatatemp, $temp);
            }
            $tabdata = $tabdatatemp;

            /**Pause game */
            $gstemp = DB::prepare($gs, 1);
            $gstemp->status = "pause";
            DB::update($gstemp);
            
            Route::view("gm.review",compact("wrdata","playerdata","carddata","tabdata"));
        }
        public static function store(){
            //code here...
        }
        public static function show(){
            //code here...
        }
        public static function update(){
            //code here...
        }
        public static function destroy(){
            //code here...
        }
    }
?>