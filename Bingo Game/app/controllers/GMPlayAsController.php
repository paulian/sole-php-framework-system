<?php
    include('../../vendor/invoker/invoke.php');
    class GMPlayAsController{
        public static function index(){
            $pl = Data::unload("playas-player");

            /**Make gamestatus to pending because it always update to a value of 1 for whatever reasons. */
            // $gs = new GameStatus;
            // $gsdata = DB::all($gs);
            // $gstemp = DB::prepare($gs, 1);
            // $gstemp->status = "pending";
            // DB::update($gstemp);


            /**Get list of cards */
            $card = new Card;
            $cards = DB::where($card,"pid","=",$pl[0]["pid"]);

            /**Update player refresh option */
            $pltemp = new Player;
            $pltemp = DB::prepare($pltemp, $pl[0]["id"]);
            $pltemp->refresh = "false";
            DB::update($pltemp);

            /**Get updated player data */
            $pltemp = new Player;
            $player = DB::find($pltemp, $pl[0]["id"]);

            Data::load("playas-player", $player);
            Data::load("playas-cards", $cards);

           Route::index("GMHomeController");
        }
        public static function store(){
            $card = new Card;
            $data = DB::where($card,"pid","=",$_POST["pid"]);

            $gs = new GameStatus;
            $gsdata = DB::all($gs);
            $bet = $_POST["cardnumber"] * $gsdata[0]["rate"];

            $player = new Player;
            $playerdata = DB::where($player,"pid","=",$_POST["pid"]);

            /**Return bet to player form the jackpot */
            $gstemp = DB::prepare($gs, 1);
            $gstemp->jackpot = $gsdata[0]["jackpot"] - $playerdata[0]["bet"];
            DB::update($gstemp);

            $playertempa = DB::prepare($player, $playerdata[0]["id"]);
            $playertempa->amount = $playerdata[0]["amount"] + $playerdata[0]["bet"];
            $playertempa->bet = "0";
            DB::update($playertempa);
            
            if($playerdata[0]["amount"] < $bet){
                /**Delete previous cards */
                foreach($data as $d){
                    DB::delete($card, $d["id"]);
                }

                Data::load("Message","Sorry You Don't Have Enough Balance To Purchase Bingo Card.");
            }else{
                /**Update amount and bet */
                $player = new Player;
                $playerdata = DB::where($player,"pid","=",$_POST["pid"]);
                $playertemp = DB::prepare($player, $playerdata[0]["id"]);
                $playertemp->amount = $playerdata[0]["amount"] - $bet;
                $playertemp->bet = $bet;
                DB::update($playertemp);

                /**Add bet to jackpot */
                $gs = new GameStatus;
                $gsdata = DB::all($gs);
                $gstemp = DB::prepare($gs, 1);
                $gstemp->jackpot = $gsdata[0]["jackpot"] + $bet;
                DB::update($gstemp);

                /**Update player board display */
                $player = new Player;
                $playerdata = DB::where($player,"pid","=",$_POST["pid"]);
                $player = Data::load("player",$playerdata);

                /**Delete previous cards */
                foreach($data as $d){
                    DB::delete($card, $d["id"]);
                }
                /**Create new random cards */
                for ($i=0; $i < $_POST["cardnumber"]; $i++) {
                    $card = new Card;
                    $card->cid = Data::generate(20,"alpha");
                    $card->pid = $_POST["pid"];

                    $br = new BingoRandomizer;
                    $card->ra = $br->ra();
                    $card->rb = $br->rb();
                    $card->rc = $br->rc();
                    $card->rd = $br->rd();
                    $card->re = $br->re();
                    DB::save($card);
                }    
            }
            /**Make gamestatus to pending because it always update to a value of 1 for whatever reasons. */
            $gs = new GameStatus;
            $gsdata = DB::all($gs);
            $gstemp = DB::prepare($gs, 1);
            $gstemp->status = "pending";
            DB::update($gstemp);
            
            Route::index("GMPlayAsController");
        }
        public static function show(){
            $pltemp = new Player;
            $pl = DB::find($pltemp, $_POST["id"]);
            Data::load("playas-player", $pl);
            Data::load("playas",true);
            Route::index("GMPlayAsController");
        }
        public static function update(){
            //code here...
        }
        public static function destroy(){
            Data::load("playas",false);
            $player = new Player;
            Data::load("playas-player",DB::where($player,"codename","=","gm"));
            Route::index("GMPlayAsController");
        }
    }
?>