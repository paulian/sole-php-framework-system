<?php
    include('../../vendor/invoker/invoke.api.php');
    class GMPlayerController{
        public static function index(){
            $player = new Player;
            Data::json_response(DB::all($player));
        }
        public static function store(){
            //code here...
        }
        public static function show(){
            //code here...
        }
        public static function update(){
            //code here...
        }
        public static function destroy(){
            $player = new Player;
            $playerdata = DB::find($player, $_POST["id"]);

            if($playerdata[0]["amount"] != 0){
                Data::json_response("Player ". $playerdata[0]["name"] ." have a remaining balance of ". $playerdata[0]["amount"] .", withdraw balance instead.");
            }else{
                $card = new Card;
                $data = DB::where($card,"pid","=",$playerdata[0]["pid"]);
                foreach($data as $d){
                    DB::delete($card, $d["id"]);
                }
                DB::delete($player, $_POST["id"]);
                Data::json_response("Player ". $playerdata[0]["name"] ." has been successfully removed.");
            }
        }
    }
?>