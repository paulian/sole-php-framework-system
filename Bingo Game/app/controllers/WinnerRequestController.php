<?php
    include('../../vendor/invoker/invoke.api.php');
    class WinnerRequestController{
        public static function index(){
            $wr = new WinnerRequest;
            
            if(DB::validate($wr,"pid",$_POST["pid"])){
                $wr->wid = uniqid();
                $wr->cid = $_POST["cid"];
                $wr->pid = $_POST["pid"];
                $wr->status = "pending";
                DB::save($wr);    
                $player = new Player;

                $plmessage = DB::find($player,$_POST["pid"]);
                foreach(DB::all($player) as $pl){
                    if($pl["id"] != 1 && $pl["id"] != $_POST["pid"]){
                        $message = new Message;
                        $message->pid = $pl["id"];
                        $message->message = "Player ".$plmessage[0]["name"]." submitted a winner request";
                        DB::save($message);

                        $message = new Message;
                        $message->pid = $pl["id"];
                        $message->message = "Game will be pause in moment";
                        DB::save($message);
                    }elseif($pl["id"] == $_POST["pid"]){
                        $message = new Message;
                        $message->pid = $pl["id"];
                        $message->message = "You submitted a winner request";
                        DB::save($message);

                        $message = new Message;
                        $message->pid = $pl["id"];
                        $message->message = "Game will be pause in moment";
                        DB::save($message);
                    }
                    
                }
            }
        }
        public static function store(){
            $player = new Player;
            Data::json_response(DB::find($player,$_POST["pid"]));
        }
        public static function show(){
            $wr = new WinnerRequest;
            Data::json_response(DB::all($wr));
        }
        public static function update(){
            $wr = new WinnerRequest;
            $wrtemp = DB::prepare($wr,$_POST["id"]);
            $wrtemp->status = $_POST["status"];
            DB::update($wrtemp);
        }
        public static function destroy(){
            //code here...
        }
    }
?>