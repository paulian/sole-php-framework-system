<?php
    include('../../vendor/invoker/invoke.api.php');
    class GMResetController{
        public static function index(){
            /**Reset bottle */
            BingoRandomizer::bottle_return_ball();

            /**Reset balls */
            $tabs = new Tabs;
            DB::wipe($tabs);

            /**Reset cards */
            $card = new Card;
            DB::wipe($card);

            /**Reset Gamestatus */
            $gs = new GameStatus;
            $gstemp = DB::prepare($gs, 1);
            $gstemp->status = "pending";
            $gstemp->tab = "...";
            $gstemp->jackpot = 0;
            DB::update($gstemp);

            /**Reset Players */
            $player = new Player;
            $playerdata = DB::all($player);

            /**Reset Winner Request */
            $wr = new WinnerRequest;
            DB::wipe($wr);

            /**Reset messages */
            $message = new Message;
            DB::wipe($message);

            foreach($playerdata as $p){
                $playertemp = DB::prepare($player, $p["id"]);
                $playertemp->amount = $p["amount"] + $p["bet"];
                $playertemp->bet = 0;
                $playertemp->refresh = "true";
                DB::update($playertemp);
            }
        }
        public static function store(){
            //code here...
        }
        public static function show(){
            //code here...
        }
        public static function update(){
            //code here...
        }
        public static function destroy(){
            //code here...
        }
    }
?>