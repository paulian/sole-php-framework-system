<?php
    include('../../vendor/invoker/invoke.php');
    class GMWithdrawCashController{
        public static function index(){
            Route::view("gm.withdrawcash");
        }
        public static function store(){
            //code here...
        }
        public static function show(){
            $player = new Player;
            $playerdata = DB::where($player,"codename","=",$_POST["codename"]);
            if(!count($playerdata)){
                Data::load("Message","Codename Not Found.");
            }
            Data::load("transaction",$playerdata);
            Route::index("GMWithdrawCashController");
        }
        public static function update(){
            if(Data::unload("transaction")[0]["amount"] < $_POST["amount"]){
                Data::load("Message","Account Doesn't Have Enough Balance To Withdraw.");
                Route::index("GMWithdrawCashController");
            }else{
                $player = new Player;
                $playertemp = DB::prepare($player, Data::unload("transaction")[0]["id"]);
                $playertemp->amount = Data::unload("transaction")[0]["amount"] - $_POST["amount"];
                DB::update($playertemp);
                Data::load("Message","Withdraw Success.");
                header("location: GMHomeController?show");    
            }
        }
        public static function destroy(){
            //code here...
        }
    }
?>