<?php
    include('../../vendor/invoker/invoke.php');
    class ReleaseJackpotController{
        public static function index(){
            $prize = 0;

            $wr = new WinnerRequest;
            $wrdata = DB::where($wr,"status","=","accepted");

            $gs = new GameStatus;
            $gsdata = DB::all($gs);
            
            if(count($wrdata)){
                $prize = $gsdata[0]["jackpot"] / count($wrdata);
                $player = new Player;

                foreach($wrdata as $wrd){
                    $playerdata = DB::all($player);
                    foreach($playerdata as $playerd){
                        if($wrd["pid"] == $playerd["id"]){
                            $playertemp = DB::prepare($player, $playerd["id"]);
                            $playertemp->amount = $playerd["amount"] + $prize;
                            DB::update($playertemp);
                        }
                    }
                }
                foreach($playerdata as $playerd){
                    $playertemp = DB::prepare($player, $playerd["id"]);
                    $playertemp->bet = "0";
                    DB::update($playertemp);
                }

                /**Reset bottle */
                BingoRandomizer::bottle_return_ball();

                /**Reset balls */
                $tabs = new Tabs;
                DB::wipe($tabs);

                /**Reset cards */
                $card = new Card;
                DB::wipe($card);

                /**Reset Gamestatus */
                $gs = new GameStatus;
                $gstemp = DB::prepare($gs, 1);
                $gstemp->status = "pending";
                $gstemp->tab = "...";
                $gstemp->jackpot = 0;
                DB::update($gstemp);

                /**Reset Players */
                $player = new Player;
                $playerdata = DB::all($player);

                /**Reset Winner Request */
                $wr = new WinnerRequest;
                DB::wipe($wr);

                foreach($playerdata as $p){
                    $playertemp = DB::prepare($player, $p["id"]);
                    $playertemp->amount = $p["amount"] + $p["bet"];
                    $playertemp->bet = 0;
                    $playertemp->refresh = "true";
                    DB::update($playertemp);
                }
            }
            Route::index("GMHomeController");
        }
        public static function store(){
            //code here...
        }
        public static function show(){
            //code here...
        }
        public static function update(){
            //code here...
        }
        public static function destroy(){
            //code here...
        }
    }
?>