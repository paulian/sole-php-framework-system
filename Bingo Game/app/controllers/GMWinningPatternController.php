<?php
    include('../../vendor/invoker/invoke.php');
    class GMWinningPatternController{
        public static function index(){
            //code here...
        }
        public static function store(){
            $res = json_decode(file_get_contents("../../resources/temp/json/row.json"));
            $r0 = "";
            $r1 = "";
            $r2 = "";
            $r3 = "";
            $r4 = "";
            for ($i=0; $i < count($res->r0); $i++) { 
                $r0 .= $res->r0[$i];
            }
            for ($i=0; $i < count($res->r1); $i++) { 
                $r1 .= $res->r1[$i];
            }
            for ($i=0; $i < count($res->r2); $i++) { 
                $r2 .= $res->r2[$i];
            }
            for ($i=0; $i < count($res->r3); $i++) { 
                $r3 .= $res->r3[$i];
            }
            for ($i=0; $i < count($res->r4); $i++) { 
                $r4 .= $res->r4[$i];
            }
            if($r0 == ""){
                $r0 = "null";
            }
            if($r1 == ""){
                $r1 = "null";
            }
            if($r2 == ""){
                $r2 = "null";
            }
            if($r3 == ""){
                $r3 = "null";
            }
            if($r4 == ""){
                $r4 = "null";
            }
            $wp = new WinningPattern;
            $wp->wpid = Data::generate(0,"uniqid");
            $wp->r0 = $r0;
            $wp->r1 = $r1;
            $wp->r2 = $r2;
            $wp->r3 = $r3;
            $wp->r4 = $r4;
            DB::save($wp);

            $player = new Player;
            $playerdata = DB::all($player);

            foreach($playerdata as $p){
                $playertemp = DB::prepare($player, $p["id"]);
                $playertemp->refresh = "true";
                DB::update($playertemp);
            }
            Route::index("GMHomeController");
        }
        public static function show(){
            //code here...
        }
        public static function update(){
            $wp = new WinningPattern;
            DB::wipe($wp);

            $player = new Player;
            $playerdata = DB::all($player);

            foreach($playerdata as $p){
                $playertemp = DB::prepare($player, $p["id"]);
                $playertemp->refresh = "true";
                DB::update($playertemp);
            }
            Route::index("GMHomeController");
        }
        public static function destroy(){
            $wp = new WinningPattern;
            DB::delete($wp, $_POST["id"]);

            $player = new Player;
            $playerdata = DB::all($player);

            foreach($playerdata as $p){
                $playertemp = DB::prepare($player, $p["id"]);
                $playertemp->refresh = "true";
                DB::update($playertemp);
            }
            Route::index("GMHomeController");
        }
    }
?>