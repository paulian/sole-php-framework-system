<?php
    include('../../vendor/invoker/invoke.api.php');
    class GMStartController{
        public static function index(){
            $gs = new GameStatus;
            $gstemp = DB::prepare($gs, 1);
            $gstemp->status = "ongame";
            DB::update($gstemp);
        }
        public static function store(){
            $gs = new GameStatus;
            $gstemp = DB::prepare($gs, 1);
            $gstemp->status = "pause";
            DB::update($gstemp);
        }
        public static function show(){
            //code here...
        }
        public static function update(){
            //code here...
        }
        public static function destroy(){
            //code here...
        }
    }
?>