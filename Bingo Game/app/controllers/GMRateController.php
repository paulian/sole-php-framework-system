<?php
    include('../../vendor/invoker/invoke.php');
    class GMRateController{
        public static function index(){
            $gs = new GameStatus;
            $gsdata = DB::all($gs);
            if($gsdata[0]["jackpot"] != 0){
                Data::load("Message","Reset The Game Before Changing Card Rate.");
            }else{
                $gstemp = DB::prepare($gs, 1);
                $gstemp->status = "pending";
                $gstemp->rate = $_POST["rate"];
                DB::update($gstemp);
            }
            Route::index("GMHomeController");
        }
        public static function store(){
            //code here...
        }
        public static function show(){
            //code here...
        }
        public static function update(){
            //code here...
        }
        public static function destroy(){
            //code here...
        }
    }
?>