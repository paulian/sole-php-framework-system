<?php
    include('../../vendor/invoker/invoke.api.php');
    class GMRandomizerController{
        public static function index(){
            $gs = new GameStatus;
            $gsdata = DB::all($gs);

            $ball = BingoRandomizer::bottle_shake_and_pick();
            if($ball[0] && $ball[1]){
                /**Save previous tab */
                if($gsdata[0]["tab"] != "..."){
                    $tab = new Tabs;
                    $tab->tabs = $gsdata[0]["tab"];
                    DB::save($tab);    
                }
                /**Create new tab */
                $gstemp = DB::prepare($gs, 1);
                $gstemp->status = "ongame";
                $gstemp->tab = $ball[0];
                DB::update($gstemp);
            }elseif($ball[1] <= 0){
                Data::json_response("Last Ball.");
            }else{
                Data::json_response("Pick Again.");
            }
        }
        public static function store(){
            //code here...
        }
        public static function show(){
            //code here...
        }
        public static function update(){
            //code here...
        }
        public static function destroy(){
            //code here...
        }
    }
?>