<?php
    include('../../vendor/invoker/invoke.api.php');
    class GMRowWinningPatternController{
        public static function index(){
            $res = json_decode(file_get_contents("../../resources/temp/json/row.json"));
            $res->r0 = [];
            $res->r1 = [];
            $res->r2 = [];
            $res->r3 = [];
            $res->r4 = [];
            file_put_contents("../../resources/temp/json/row.json",json_encode($res));
        }
        public static function store(){
            $res = json_decode(file_get_contents("../../resources/temp/json/row.json"));
            $row = $_POST["row"][0].$_POST["row"][1];
            $column = $_POST["row"][3];
            array_push($res->$row,$column);
            file_put_contents("../../resources/temp/json/row.json",json_encode($res));
        }
        public static function show(){
            //code here...
        }
        public static function update(){
            //code here...
        }
        public static function destroy(){
            $res = json_decode(file_get_contents("../../resources/temp/json/row.json"));
            $row = $_POST["row"][0].$_POST["row"][1];
            $column = $_POST["row"][3];
            $arr = [];
            $key = "";
            for ($i = 0; $i < count($res->$row); $i++) { 
                if($res->$row[$i] == $column){
                    $key = $i;
                }
            }
            for ($i=$key; $i < count($res->$row); $i++) { 
                array_push($arr, $res->$row[$i]);  
            }
            for ($i=0; $i < count($arr); $i++) { 
                array_pop($res->$row);
            }
            array_shift($arr);
            $res->$row = array_merge($res->$row, $arr);  
            file_put_contents("../../resources/temp/json/row.json",json_encode($res));
        }
    }
?>