<?php
    include('../../vendor/invoker/invoke.php');
    class GMHomeController{
        public static function index(){
            $player = new Player;
            $playerdata = DB::all($player);

            $wp = new WinningPattern;
            $wpdata = DB::all($wp);
            
            Route::view("gm.home",compact("playerdata","wpdata"));
        }
        public static function store(){
            //code here...
        }
        public static function show(){
            Data::load("transaction","");
            Route::view("gm.transactions");
        }
        public static function update(){
            //code here...
        }
        public static function destroy(){
            Data::unload("auth-gm");
            Route::index("HomeController");
        }
    }
?>