<?php
    include('../../vendor/invoker/invoke.php');
    class GMTransferBalanceController{
        public static function index(){
            Route::view("gm.transferbalance");
        }
        public static function store(){
            //code here...
        }
        public static function show(){
            $player = new Player;
            $playerdata = DB::where($player,"codename","=",$_POST["codename"]);
            if(!count($playerdata)){
                Data::load("Message","Codename Not Found.");
            }
            Data::load("transaction",$playerdata);
            Route::index("GMTransferBalanceController");
        }
        public static function update(){
            $player = new Player;
            $playerdata = DB::where($player,"name","=",$_POST["name"]);
            if(count($playerdata)){
                if(Data::unload("transaction")[0]["amount"] >= $_POST["amount"]){
                    $sender = DB::prepare($player, Data::unload("transaction")[0]["id"]);
                    $sender->amount = Data::unload("transaction")[0]["amount"] - $_POST["amount"];
                    DB::update($sender);

                    $reciever = DB::prepare($player, $playerdata[0]["id"]);
                    $reciever->amount = $playerdata[0]["amount"] + $_POST["amount"];
                    DB::update($reciever);
                    
                    Data::load("Message","Transfer Success.");
                    header("location: GMHomeController?show");
                }else{
                    Data::load("Message","Sender Doesn't Have Enough Balance To Transfer.");
                    Route::index("GMTransferBalanceController");  
                }
            }else{
                Data::load("Message","Reciever's Name Not Found.");
                Route::index("GMTransferBalanceController");
            }
        }
        public static function destroy(){
            //code here...
        }
    }
?>