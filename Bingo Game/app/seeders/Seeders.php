<?php
    Seed::$seeders = ["GM","GameStatus","WinningPattern","Player"];

    class GM
    {
        public static function index(){
            Seed::table("gm");
            Seed::insert([
                "gmid" => uniqid(),
                "name" => "Game Master",
                "username" => md5("gm"),
                "password" => md5("gm")
            ]);
        }
    }

    class GameStatus
    {
        public static function index(){
            Seed::table("gamestatus");
            Seed::insert([
                "status" => "pending",
                "tab" => "...",
                "jackpot" => 0,
                "rate" => 1
            ]);
        }
    }

    class WinningPattern
    {
        public static function index(){
            $r0 = ['0','4','01234','null','null','null','null','0','1','2','3','4','2','null','null','2','04','2','04','2','0','4','null','null','null','024','02134','null','null','null','null','01','34','null','null','12','23','null','null','null','null','null','null','3','null','null','1'];
            $r1 = ['1','3','null','01234','null','null','null','0','1','2','3','4','1','null','null','3','null','null','null','null','null','null','2','13','123','31','43210','null','null','12','23','10','34','null','null','12','23','34','null','null','null','null','01','2','0','4','2'];
            $r2 = ['null','null','null','null','0143','null','null','0','1','null','3','4','0','0','4','4','null','04','null','null','4','0','13','null','31','04','0134','3','1','1','3','null','null','null','null','null','null','43','34','null','null','10','10','1','1','3','3'];
            $r3 = ['3','1','null','null','null','01243','null','0','1','2','3','4','null','1','3','null','null','null','null','null','null','null','2','31','321','13','43210','23','12','null','null','null','null','34','10','null','null','null','43','32','21','01','null','0','2','2','4'];
            $r4 = ['4','0','null','null','null','null','01234','0','1','2','3','4','null','2','2','null','40','2','2','04','0','4','null','null','null','024','43210','null','null','null','null','null','null','34','10','null','null','null','null','23','12','null','null','null','3','1','null'];

            for ($i = 0; $i < 47; $i++) { 
                Seed::table("winningpattern");
                Seed::insert([
                    "wpid" => uniqid(),
                    "r0" => $r0[$i],
                    "r1" => $r1[$i],
                    "r2" => $r2[$i],
                    "r3" => $r3[$i],
                    "r4" => $r4[$i],
                ]);    
            }
        }
    }

    class Player
    {
        public static function index(){
            Seed::table("player");
            Seed::insert([
                "pid" => uniqid(),
                "name" => "Game Master",
                "email" => "gm@sole.ph",
                "codename" => "gm",
                "refresh" => "false",
                "amount" => "1000000000",
                "bet" => "0"
            ]);
        }
    }
?>