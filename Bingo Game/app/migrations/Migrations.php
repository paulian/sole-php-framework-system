<?php
    Migrate::$migration = ["PlayerMigration","CardMigration","GMMigration","GameStatusMigration","TabMigration","WinnerRequestMigration","WinningPatternMigration","MessageMigration"];


    class PlayerMigration
    {
        public static function index(){
            Migrate::attrib_table("player");
            Migrate::attrib_string(255);
            Migrate::string("pid");
            Migrate::string("name");
            Migrate::string("email");
            Migrate::string("codename");
            Migrate::string("refresh");
            Migrate::string("amount");
            Migrate::string("bet");
        }
    }

    class CardMigration
    {
        public static function index(){
            Migrate::attrib_table("card");
            Migrate::attrib_string(255);
            Migrate::string("cid");
            Migrate::string("pid");
            Migrate::string("ra");
            Migrate::string("rb");
            Migrate::string("rc");
            Migrate::string("rd");
            Migrate::string("re");
        }
    }

    class GMMigration
    {
        public static function index(){
            Migrate::attrib_table("gm");
            Migrate::attrib_string(255);
            Migrate::string("gmid");
            Migrate::string("name");
            Migrate::string("username");
            Migrate::string("password");
        }
    }

    class GameStatusMigration
    {
        public static function index(){
            Migrate::attrib_table("gamestatus");
            Migrate::attrib_string(255);
            Migrate::string("status");
            Migrate::string("tab");
            Migrate::string("jackpot");
            Migrate::string("rate");
        }
    }

    class TabMigration
    {
        public static function index(){
            Migrate::attrib_table("tabs");
            Migrate::attrib_string(255);
            Migrate::string("tabs");
        }
    }

    class WinnerRequestMigration
    {
        public static function index(){
            Migrate::attrib_table("winnerrequest");
            Migrate::attrib_string(255);
            Migrate::string("wid");
            Migrate::string("cid");
            Migrate::string("pid");
            Migrate::string("status");
        }
    }

    class WinningPatternMigration
    {
        public static function index(){
            Migrate::attrib_table("winningpattern");
            Migrate::attrib_string(255);
            Migrate::string("wpid");
            Migrate::string("r0");
            Migrate::string("r1");
            Migrate::string("r2");  
            Migrate::string("r3");              
            Migrate::string("r4");              
        }
    }

    class MessageMigration
    {
        public static function index(){
            Migrate::attrib_table("message");
            Migrate::attrib_string(255);
            Migrate::string("pid");              
            Migrate::string("message");              
        }
    }
?>