<?php
    /**Note: do not interchange code arrangement */
    include_once('../../vendor/database/db.php');
    include_once('../models/Models.php');
    include_once('../../vendor/data/data.php');
    include_once('../../vendor/data/log.php');

    $res = file_get_contents("../../.env");
    $conf = explode("
",$res);
    for ($i=0; $i < count($conf); $i++) { 
        if(explode('=',$conf[$i])[0]=="Extension"){
            if(file_exists('../../vendor/libs/'.explode('=',$conf[$i])[1].'/loader.php')){
                include_once('../../vendor/libs/'.explode('=',$conf[$i])[1].'/loader.php');
            }
        }
    }

    class Extend_Route{
        public static function er(){
            $func = array("store","show","update","destroy");
            $Controller = $_SERVER["PHP_SELF"];
            $arr = explode('/',$Controller);
            $arr = explode('.',end($arr));
            $Controller = $arr[0];
        
            if($_SERVER["QUERY_STRING"]){
                if(in_array($_SERVER["QUERY_STRING"],$func)){
                    $method = $_SERVER["QUERY_STRING"];
                    $Controller::$method();
                }else{
                    $method = "unknown_method_".$_SERVER["QUERY_STRING"];
                    $Controller::$method();
                }
            }else{
                $Controller::index();
            }
        }
    }
    Extend_Route::er();
?>