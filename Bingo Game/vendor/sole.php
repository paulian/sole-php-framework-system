<?php
    include_once('migration/migrate.php');
    include_once('migration/make.php');
    include_once('model/make.php');
    include_once('seeder/make.php');
    include_once('seeder/seed.php');
    include_once('controller/make.php');
    include_once('database/make.php');

    class AppLoad{
        public static function index(){
            $res = file_get_contents(".env");
            $conf = explode("
",$res);
            if($_SERVER["argc"] >= 2){
                if($_SERVER["argv"][1] == "migrate:database"){
                    if($_SERVER["argc"] > 2){
                        if($_SERVER["argv"][2] == "json"){
                            if(explode('=',$conf[7])[1] != "frameworkhost"){
                                echo "\e[1;33;40mPlease configure database host to frameworkhost.\e[0m";
                            }else{
                                echo "\e[1;33;40mThis JSON database management is work in progress, it contains errors. We suggest not to use it at the current framework version.\e[0m";
                                //Migrate::json_run();   
                            }
                        }elseif($_SERVER["argv"][2] == "sql"){
                            if(explode('=',$conf[7])[1] != "localhost"){
                                echo "\e[1;33;40mPlease configure database host to localhost.\e[0m";
                            }else{
                                if(explode('=',$conf[8])[1] != "sole_db"){
                                    Migrate::default_run();     
                                }else{
                                    echo "\e[1;33;40mPlease configure database name to your project database name.\e[0m";
                                } 
                            }
                        }else{
                            echo "\e[1;31;40mInvalid script ".$_SERVER["argv"][2]."\e[0m";   
                        }
                    }else{
                        echo "\e[1;33;40mPlease specify database type.\e[0m";   
                    }
                }elseif($_SERVER["argv"][1] == "seed:database"){
                    Seed::default_run();
                }elseif($_SERVER["argv"][1] == "make:database"){
                    MakeDatabase::make($_SERVER["argv"][2]);
                }elseif($_SERVER["argv"][1] == "make:controller"){
                    if($_SERVER["argc"] >= 4){
                        if($_SERVER["argv"][3] == "api"){
                            MakeController::make_api($_SERVER["argv"][2]);    
                        }else{
                            echo "\e[1;31;40mInvalid script ".$_SERVER["argv"][3]."\e[0m";
                        }
                    }else{
                        MakeController::make_default($_SERVER["argv"][2]);
                    }
                }elseif($_SERVER["argv"][1] == "make:migration"){
                    MakeMigration::make($_SERVER["argv"][2]);
                }elseif($_SERVER["argv"][1] == "make:model"){
                    MakeModel::make($_SERVER["argv"][2]);
                }elseif($_SERVER["argv"][1] == "make:seeder"){
                    MakeSeeder::make($_SERVER["argv"][2]);
                }elseif($_SERVER["argv"][1] == "framework"){
                    if($_SERVER["argv"][2] == "--version"){
                        echo "\e[1;32;40mSole PHP Framework Version:\e[0m  \e[1;33;40mv2.2\e[0m\n";
                        echo "\e[1;32;40mRequired PHP Version:\e[0m  \e[1;33;40mv8.0.0^\e[0m\n";
                        echo "\e[1;32;40mCurrent PHP Version:\e[0m \e[1;33;40mv".phpversion()."\e[0m";
                    }else{
                        echo "\e[1;31;40mInvalid script ".$_SERVER["argv"][2]."\e[0m";   
                    }
                }else{
                    echo "\e[1;31;40mInvalid script ".$_SERVER["argv"][1]."\e[0m";
                }
            }else{
                echo "\e[1;33;40mphp sole ... (please refer to the documentation for script commands)."."\e[0m";
            }
        }
    }
    Appload::index();
?>