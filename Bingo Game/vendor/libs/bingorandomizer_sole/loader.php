<?php
    class BingoRandomizer{
        public function __construct(){
            $put = json_decode(file_get_contents("../../vendor/libs/bingorandomizer_sole/phpbingorandomizer/bingorandomizer_data.json"));
            $put->B = array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15");
            $put->I = array("16","17","18","19","20","21","22","23","24","25","26","27","28","29","30");
            $put->N = array("31","32","33","34","35","36","37","38","39","40","41","42","43","44","45");
            $put->G = array("46","47","48","49","50","51","52","53","54","55","56","57","58","59","60");
            $put->O = array("61","62","63","64","65","66","67","68","69","70","71","72","73","74","75");
            file_put_contents("../../vendor/libs/bingorandomizer_sole/phpbingorandomizer/bingorandomizer_data.json",json_encode($put));

            $res = json_decode(file_get_contents("../../vendor/libs/bingorandomizer_sole/phpbingorandomizer/bingorandomizer.json"));
            $res->ra = BingoRandomizer::getNumbers(true);            
            $res->rb = BingoRandomizer::getNumbers(true);            
            $res->rc = BingoRandomizer::getNumbers(false);            
            $res->rd = BingoRandomizer::getNumbers(true);            
            $res->re = BingoRandomizer::getNumbers(true);
            file_put_contents("../../vendor/libs/bingorandomizer_sole/phpbingorandomizer/bingorandomizer.json",json_encode($res));
            //BingoRandomizer::bottle_shake_and_pick();
        }

        public static function getNumbers($r){
            $put = json_decode(file_get_contents("../../vendor/libs/bingorandomizer_sole/phpbingorandomizer/bingorandomizer_data.json"));
            $res = "";
            /**-------------------------------------------------------- */
            $arr = [];
            $temp = rand(0,count($put->B)-1);
            $res .= $put->B[$temp]."/";
            for ($i=$temp; $i < count($put->B); $i++) { 
                array_push($arr, $put->B[$i]);  
            }
            for ($i=0; $i < count($arr); $i++) { 
                array_pop($put->B);
            }
            array_shift($arr);
            $put->B = array_merge($put->B, $arr);
            /**-------------------------------------------------------- */
            $arr = [];
            $temp = rand(0,count($put->I)-1);
            $res .= $put->I[$temp]."/";
            for ($i=$temp; $i < count($put->I); $i++) { 
                array_push($arr, $put->I[$i]);  
            }
            for ($i=0; $i < count($arr); $i++) { 
                array_pop($put->I);
            }
            array_shift($arr);
            $put->I = array_merge($put->I, $arr);
            /**-------------------------------------------------------- */
            $arr = [];
            if($r){
                $temp = rand(0,count($put->N)-1);
                $res .= $put->N[$temp]."/";
                for ($i=$temp; $i < count($put->N); $i++) { 
                    array_push($arr, $put->N[$i]);  
                }
                for ($i=0; $i < count($arr); $i++) { 
                    array_pop($put->N);
                }
                array_shift($arr);
                $put->N = array_merge($put->N, $arr);
            }else{
                $res .= "sole/";
            }
            /**-------------------------------------------------------- */
            $arr = [];
            $temp = rand(0,count($put->G)-1);
            $res .= $put->G[$temp]."/";
            for ($i=$temp; $i < count($put->G); $i++) { 
                array_push($arr, $put->G[$i]);  
            }
            for ($i=0; $i < count($arr); $i++) { 
                array_pop($put->G);
            }
            array_shift($arr);
            $put->G = array_merge($put->G, $arr);
            /**-------------------------------------------------------- */
            $arr = [];
            $temp = rand(0,count($put->O)-1);
            $res .= $put->O[$temp];
            for ($i=$temp; $i < count($put->O); $i++) { 
                array_push($arr, $put->O[$i]);  
            }
            for ($i=0; $i < count($arr); $i++) { 
                array_pop($put->O);
            }
            array_shift($arr);
            $put->O = array_merge($put->O, $arr);
            file_put_contents("../../vendor/libs/bingorandomizer_sole/phpbingorandomizer/bingorandomizer_data.json",json_encode($put));
            return $res;
        }
        public static function ra(){
            $res = json_decode(file_get_contents("../../vendor/libs/bingorandomizer_sole/phpbingorandomizer/bingorandomizer.json"));
            return $res->ra;
        }
        public static function rb(){
            $res = json_decode(file_get_contents("../../vendor/libs/bingorandomizer_sole/phpbingorandomizer/bingorandomizer.json"));
            return $res->rb;
        }
        public static function rc(){
            $res = json_decode(file_get_contents("../../vendor/libs/bingorandomizer_sole/phpbingorandomizer/bingorandomizer.json"));
            return $res->rc;
        }
        public static function rd(){
            $res = json_decode(file_get_contents("../../vendor/libs/bingorandomizer_sole/phpbingorandomizer/bingorandomizer.json"));
            return $res->rd;
        }
        public static function re(){
            $res = json_decode(file_get_contents("../../vendor/libs/bingorandomizer_sole/phpbingorandomizer/bingorandomizer.json"));
            return $res->re;
        }
        public static function bottle_shake_and_pick(){
            $put = json_decode(file_get_contents("../../vendor/libs/bingorandomizer_sole/phpbingorandomizer/bingorandomizer_bottle.json"));
            $res = ["",""];
            $letter = array("B","I","N","G","O");
            $templetter = $letter[rand(0,count($letter)-1)];
            
            if($templetter == "B"){
                $res[0] .= $templetter;
                $arr = [];
                if(count($put->B) != 0){
                    $temp = rand(0,count($put->B)-1);
                    $res[0] .= $put->B[$temp];
                    for ($i=$temp; $i < count($put->B); $i++) { 
                        array_push($arr, $put->B[$i]);  
                    }
                    for ($i=0; $i < count($arr); $i++) { 
                        array_pop($put->B);
                    }
                    array_shift($arr);
                    $put->B = array_merge($put->B, $arr);    
                    $put->balls--;
                }else{
                    $res[0] = null;
                    $templetter = "I";
                }
            }
            if($templetter == "I"){
                $res[0] .= $templetter;
                $arr = [];
                if(count($put->I) != 0){
                    $temp = rand(0,count($put->I)-1);
                    $res[0] .= $put->I[$temp];
                    for ($i=$temp; $i < count($put->I); $i++) { 
                        array_push($arr, $put->I[$i]);  
                    }
                    for ($i=0; $i < count($arr); $i++) { 
                        array_pop($put->I);
                    }
                    array_shift($arr);
                    $put->I = array_merge($put->I, $arr);    
                    $put->balls--;
                }else{
                    $res[0] = null;
                    $templetter = "N";
                }
            }
            if($templetter == "N"){
                $res[0] .= $templetter;
                $arr = [];
                if(count($put->N) != 0){
                    $temp = rand(0,count($put->N)-1);
                    $res[0] .= $put->N[$temp];
                    for ($i=$temp; $i < count($put->N); $i++) { 
                        array_push($arr, $put->N[$i]);  
                    }
                    for ($i=0; $i < count($arr); $i++) { 
                        array_pop($put->N);
                    }
                    array_shift($arr);
                    $put->N = array_merge($put->N, $arr);
                    $put->balls--;
                }else{
                    $res[0] = null;
                    $templetter = "G";
                }
            }
            if($templetter == "G"){
                $res[0] .= $templetter;
                $arr = [];
                if(count($put->G) != 0){
                    $temp = rand(0,count($put->G)-1);
                    $res[0] .= $put->G[$temp];
                    for ($i=$temp; $i < count($put->G); $i++) { 
                        array_push($arr, $put->G[$i]);  
                    }
                    for ($i=0; $i < count($arr); $i++) { 
                        array_pop($put->G);
                    }
                    array_shift($arr);
                    $put->G = array_merge($put->G, $arr);    
                    $put->balls--;
                }else{
                    $res[0] = null;
                    $templetter = "O";
                }
            }
            if($templetter == "O"){
                $res[0] .= $templetter;
                $arr = [];
                if(count($put->O) != 0){
                    $temp = rand(0,count($put->O)-1);
                    $res[0] .= $put->O[$temp];
                    for ($i=$temp; $i < count($put->O); $i++) { 
                        array_push($arr, $put->O[$i]);  
                    }
                    for ($i=0; $i < count($arr); $i++) { 
                        array_pop($put->O);
                    }
                    array_shift($arr);
                    $put->O = array_merge($put->O, $arr);
                    $put->balls--;
                }else{
                    $res[0] = null;
                }
            }
            file_put_contents("../../vendor/libs/bingorandomizer_sole/phpbingorandomizer/bingorandomizer_bottle.json",json_encode($put));
            $res[1] = $put->balls;
            return $res;
        }
        public static function bottle_return_ball(){
            $put = json_decode(file_get_contents("../../vendor/libs/bingorandomizer_sole/phpbingorandomizer/bingorandomizer_bottle.json"));
            $put->B = array("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15");
            $put->I = array("16","17","18","19","20","21","22","23","24","25","26","27","28","29","30");
            $put->N = array("31","32","33","34","35","36","37","38","39","40","41","42","43","44","45");
            $put->G = array("46","47","48","49","50","51","52","53","54","55","56","57","58","59","60");
            $put->O = array("61","62","63","64","65","66","67","68","69","70","71","72","73","74","75");
            $put->balls = 75;
            file_put_contents("../../vendor/libs/bingorandomizer_sole/phpbingorandomizer/bingorandomizer_bottle.json",json_encode($put));
        }

    }
?>