<?php
    class MakeDatabase{
        public static function make($database){
            try{
                $res = file_get_contents(".env");
                $conf = explode("
",$res);
                $DB_HOST = explode('=',$conf[7])[1];
                $DB_USERNAME= explode('=',$conf[9])[1];
                $DB_PASSWORD = explode('=',$conf[10])[1];
                
                $DB_CONN = new PDO( 'mysql:host='.$DB_HOST.';', $DB_USERNAME, $DB_PASSWORD);
                $DB_CONN->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                $SEARCH = false;
                $SQL = $DB_CONN->prepare("SHOW DATABASES");
                $SQL->execute();
                $fillable = $SQL->fetchAll(PDO::FETCH_ASSOC);
                foreach($fillable as $a){
                    if($database == $a["Database"]){
                        $SEARCH = TRUE;
                    }
                }
                if(!$SEARCH){
                    $DB_CONN = new PDO( 'mysql:host='.$DB_HOST.';', $DB_USERNAME, $DB_PASSWORD);
                    $DB_CONN->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                    $SQL = $DB_CONN->prepare("CREATE DATABASE `$database`");
                    $SQL->execute();
                    echo("\e[1;32;40mDatabase ".$database." created successfully.\e[0m\n");
                    echo("\e[1;33;41mNote:\e[0m \e[1;33;40mthis only applies to sql database.\e[0m");
                }else{
                    echo("\e[1;33;40mDatabase ".$database." already exist.\e[0m\n");
                    echo("\e[1;33;41mNote:\e[0m \e[1;33;40mthis only applies to sql database.\e[0m");
                }
                
            }catch(PDOException $e){
                echo("\e[1;33;41mDatabase Connection Failed:\e[0m \e[1;33;40m".$e->getMessage()."\e[0m");
            }
        }
    }
?>