<link rel="stylesheet" href="../../public/addons/css/bootstrap.min.css">
<link rel="stylesheet" href="../../public/addons/css/font-awesome-4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="../../public/addons/css/toastr.css">
<link rel="stylesheet" href="../../public/assets/css/sole.css">
<link rel="stylesheet" href="../../public/assets/css/sole-bootstrap.css">
<script src="../../public/addons/js/toastr.min.js"></script>
<script src="../../public/addons/js/jquery-3.3.1.min.js"></script>
<script src="../../public/addons/js/bootstrap.bundle.min.js"></script>
<script src="../../public/addons/js/popper.min.js"></script>
<script src="../../public/addons/js/toastMessage.js"></script>
<script src="../../public/assets/js/sole.js"></script>
