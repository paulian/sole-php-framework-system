console.log("Framework Developer: Paul Ian Dumdum");
function pick(id){
    var btn = document.getElementById(id);
    console.log(id)
    if(btn.classList[1] == "btn-primary"){
        btn.classList.remove("btn-primary");
        btn.classList.add("btn-success");
    }else{
        btn.classList.remove("btn-success");
        btn.classList.add("btn-primary");
    }
}
function setwp(id){
    var btn = document.getElementById(id);

    if(btn.classList[1] == "btn-primary"){
        btn.classList.remove("btn-primary");
        btn.classList.add("btn-warning");
        fetch("GMRowWinningPatternController?store",  {
            method: 'POST',
            body: new URLSearchParams('row=' + id)
        })
        .then(res => res.text())
        .then(res => console.log(res))
    }else{
        btn.classList.remove("btn-warning");
        btn.classList.add("btn-primary");
        fetch("GMRowWinningPatternController?destroy",  {
            method: 'POST',
            body: new URLSearchParams('row=' + id)
        })
        .then(res => res.text())
        .then(res => console.log(res))
    }
}
function resetwp(){
    fetch("GMRowWinningPatternController")
}
function messageService(pid){
    fetch("MessageController?store",{
        method: 'POST',
        body: new URLSearchParams('pid=' + pid)
    })
    .then(res => res.json())
    .then(res => popmessage(res))
}
function popmessage(data){
    if(data != 0){
        popMessage(data);    
    }
}
function sendMessage(id,pid){
    var message = document.getElementById(id);
    fetch("MessageController",{
        method: 'POST',
        body: new URLSearchParams('pid=' + pid + '& message=' + message.value)
    })
    message.value = "";
}