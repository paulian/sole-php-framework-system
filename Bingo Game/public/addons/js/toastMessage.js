function warningMessage(data){
    toastr.options = {
        "closeButton": true,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
    }
    toastr.warning(data);
}
function successMessage(data){
    toastr.options = {
        "closeButton": true,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
    }
    toastr.success(data);
}
function infoMessage(data){
    toastr.options = {
        "closeButton": true,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
    }
    toastr.info(data);
}
function errorMessage(data){
    toastr.options = {
        "closeButton": true,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
    }
    toastr.error(data);
}
function welcomeMessage(data){
    toastr.options = {
        "closeButton": true,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-bottom-right",
    }
    toastr.info(data);
}
function popMessage(data){
    toastr.options = {
        "closeButton": true,
        "newestOnTop": true,
        "progressBar": false,
        "hideDuration": 20000,
        "positionClass": "toast-top-left",
    }
    toastr.info(data);
}