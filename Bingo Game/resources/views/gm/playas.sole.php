<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
    <body>
        <div class="modal-body">
            <div class="modal-header alert-success pb-1">
                <h6 class="mt-2"><span class="fa fa-id-card"></span> Playing As: <?php echo(Data::unload("playas-player")[0]["name"]);?></h6>
                <div class="alert-primary btn mt-1"><a href="GMPlayAsController">Refresh</a></div>
                <div class="col-md-3" id="cardnum" style="display: ">
                    <form action="GMPlayAsController?store" method="post">
                        <div class="btn-group">
                            <input type="hidden" name="pid" value="<?php echo(Data::unload("playas-player")[0]["pid"]);?>">
                            <select required name="cardnumber" id="cardNumber" class="form-control col-md-10">
                                <option value="" disabled selected>Buy Cards</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                            <button type="submit" class="btn btn-success"><span class="fa fa-credit-card"></span></button>
                        </div>    
                    </form>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col" style="height: 470px; overflow-y: scroll">
                        <div class="row">
                            <?php $arr = []; ?>
                            <?php foreach(Data::unload("playas-cards") as $card){ ?>
                            <div class="col-md-6 mb-2 mt-2">
                                <div class="card-con">
                                    <div class="modal-header p-1 card-head">
                                        <h5>B</h5>
                                        <h5>I</h5>
                                        <h5>N</h5>
                                        <h5>G</h5>
                                        <img class="card-icon" src="../../public/assets/icons/favicon.ico" alt="">
                                    </div>
                                    <div class="modal-header p-1 mh-custom-playas">
                                        <?php
                                            $ra = explode("/",$card["ra"]);
                                            for ($i=0; $i < count($ra); $i++) { 
                                                $id = Data::generate(20,"alpha");
                                                array_push($arr,$id);
                                                echo('<button onClick="pick(\''.$id.'\')" id="'.$id.'" class="btn btn-primary">'.$ra[$i].'</button>');
                                            }
                                        ?>
                                    </div>
                                    <div class="modal-header p-1 mh-custom-playas">
                                        <?php
                                            $rb = explode("/",$card["rb"]);
                                            for ($i=0; $i < count($rb); $i++) { 
                                                $id = Data::generate(20,"alpha");
                                                array_push($arr,$id);
                                                echo('<button onClick="pick(\''.$id.'\')" id="'.$id.'" class="btn btn-primary">'.$rb[$i].'</button>');
                                            }
                                        ?>
                                    </div>
                                    <div class="modal-header p-1 mh-custom-playas">
                                        <?php
                                            $rc = explode("/",$card["rc"]);
                                            for ($i=0; $i < count($rc); $i++) { 
                                                if($rc[$i] == "sole"){
                                                    echo('<button class="btn"><img class="card-icon" src="../../public/assets/icons/favicon.ico" alt=""></button>');
                                                }else{
                                                    $id = Data::generate(20,"alpha");
                                                    array_push($arr,$id);
                                                    echo('<button onClick="pick(\''.$id.'\')" id="'.$id.'" class="btn btn-primary">'.$rc[$i].'</button>');    
                                                }
                                            }
                                        ?>
                                    </div>
                                    <div class="modal-header p-1 mh-custom-playas">
                                        <?php
                                            $rd = explode("/",$card["rd"]);
                                            for ($i=0; $i < count($rd); $i++) { 
                                                $id = Data::generate(20,"alpha");
                                                array_push($arr,$id);
                                                echo('<button onClick="pick(\''.$id.'\')" id="'.$id.'" class="btn btn-primary">'.$rd[$i].'</button>');
                                            }
                                        ?>
                                    </div>
                                    <div class="modal-header p-1 mh-custom-playas">
                                        <?php
                                            $re = explode("/",$card["re"]);
                                            for ($i=0; $i < count($re); $i++) { 
                                                $id = Data::generate(20,"alpha");
                                                array_push($arr,$id);
                                                echo('<button onClick="pick(\''.$id.'\')" id="'.$id.'" class="btn btn-primary">'.$re[$i].'</button>');
                                            }
                                        ?>
                                    </div>
                                    <hr>
                                    <div style="display: flex; justify-content: space-between;" class="p-1">
                                        <h6>Card: <?php echo($card["id"]); ?></h6>
                                        <button id="<?php echo($card['cid']); ?>" onclick="winnerrequest('<?php echo($card['cid']); ?>')" class="btn btn-secondary btn-sm">Bingo</button>
                                    </div>
                                </div>      
                            </div>
                            <?php }?>
                            <?php Data::load("balls-playas",$arr); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>