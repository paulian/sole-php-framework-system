<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
    <body>
        <div class="bg-img">
            <div class="bg-trnsp">
                <div class="card-cred">
                    <?php if(!Data::unload("auth-gm")){ Route::return("HomeController"); }?>
                    <div class="modal-header alert-success">
                        <div style="display: flex;">
                            <img class="card-icon mr-1" src="../../public/assets/icons/favicon.ico" alt="">
                            <h5 class="m-1">SoloLeveler BINGO Game</h5>    
                        </div>
                    </div>
                    <?php
                        if(Data::unload("Message")){
                    ?>
                        <div class="modal-header alert alert-warning mt-1" id="alertcard">
                            <h5><?php echo Data::unload("Message"); ?></h5>
                        </div>
                    <?php
                        }
                        Data::trash("Message");
                    ?>
                    <div class="modal-body">
                        <div class="modal-header">
                            <h6><b>Withdraw Cash</b></h6>
                        </div>
                        <div class="modal-body">
                            <form action="GMWithdrawCashController?show" method="post">
                                <div class="form-group">
                                    <h6>Code Name</h6>
                                    <input required type="text" name="codename" id="" class="form-control">
                                </div>
                                <div class="modal-footer">
                                    <a href="GMHomeController?show" class="btn btn-secondary btn-sm"><span class="fa fa-remove"></span> Cancel</a>
                                    <button class="btn btn-primary btn-sm"><span class="fa fa-primary"></span> Proceed</button>
                                </div>
                            </form>
                            <?php if(Data::unload("transaction")) { ?>
                                <form action="GMWithdrawCashController?update" method="post">
                                    <div class="form-group">
                                        <h6><b>Name:</b> <?php echo(Data::unload("transaction")[0]["name"]); ?></h6>
                                        <h6><b>Balance:</b> <?php echo(Data::unload("transaction")[0]["amount"]); ?></h6>
                                    </div>
                                    <div class="form-group">
                                        <h6>Enter Amount</h6>
                                        <input required type="number" name="amount" id="" class="form-control">
                                    </div>
                                    <div class="modal-footer">
                                        <a href="GMHomeController?show" class="btn btn-secondary btn-sm"><span class="fa fa-remove"></span> Cancel</a>
                                        <button class="btn btn-success btn-sm"><span class="fa fa-money"></span> Withdraw</button>
                                    </div>
                                </form>
                            <?php } ?>
                        </div>
                    </div>    
                </div>        
            </div>
        </div>
    </body>
</html>