<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
    <body onload="resetwp()">
        <?php if(!Data::unload("auth-gm")){ Route::return("HomeController"); }?>
        <div class="modal-body">
            <div class="modal-header alert-success">
                <h5><b>Game Master Dashboard</b></h5>
                <div>
                    <button onclick="startgame()" id="btnstart" class="btn btn-success btn-sm" style="display: "><span class="fa fa-play"></span> Play Game</button>
                    <button onclick="pausegame()" id="btnpause" class="btn btn-success btn-sm" style="display: none"><span class="fa fa-pause"></span> Pause Game</button>
                    <button onclick="resetgame()" class="btn btn-dark btn-sm" id="btnreset"><span class="fa fa-refresh"></span> Reset Game</button>
                    <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#rate" id="btnrate"><span class="fa fa-line-chart"></span> Rate</button>
                    <button onclick="randomize()" id="btnrandom" class="btn btn-primary btn-sm" style="display: none"><span class="fa fa-random"></span> Randomize</button>
                    <a href="GMHomeController?show" class="btn btn-success btn-sm" target="_blank"><span class="fa fa-list"></span> Transactions</a>
                    <a href="GMHomeController?destroy" class="btn btn-dark btn-sm" style="display: "><span class="fa fa-sign-out"></span> Logout</a>
                </div>
            </div>
            <?php
                if(Data::unload("Message")){
            ?>
                <div class="modal-header alert alert-warning" id="alertcard">
                    <h5><?php echo Data::unload("Message"); ?></h5>
                </div>
            <?php
                }
                Data::trash("Message");
            ?>
            <div class="modal fade" id="rate">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="GMRateController" method="post">
                            <div class="modal-header">
                                <h6>Card Rate</h6>
                                <button data-dismiss="modal" class="btn">&times;</button>
                            </div>
                            <div class="modal-body">
                                <input required type="number" class="form-control" name="rate">
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-primary"><span class="fa fa-save"></span> Save</button>
                            </div>    
                        </form>
                        
                    </div>
                </div>
            </div>
            <div class="modal fade" id="playas">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form action="GMPlayAsController?show" method="post">
                            <div class="modal-header">
                                <h6><b>Play As</b></h6>
                                <button data-dismiss="modal" class="btn">&times;</button>
                            </div>
                            <div class="modal-body">
                                <select name="id" id="" class="form-control">
                                    <?php foreach($dd["playerdata"] as $player){ ?>
                                        <option value="<?php echo $player["id"]; ?>"><?php echo $player["name"]; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm"><span class="fa fa-play"></span> Play</button>
                            </div>    
                        </form>
                        
                    </div>
                </div>
            </div>
            <div class="modal fade" id="sendmessage">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h6><b>Send Message</b></h6>
                            <button data-dismiss="modal" class="btn">&times;</button>
                        </div>
                        <div class="modal-body">
                            <textarea id="message" rows="5" class="form-control" placeholder="Aa"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button onclick="sendMessage('message','<?php echo(Data::unload('playas-player')[0]['name']);?>')" class="btn btn-primary btn-sm"><span class="fa fa-send"></span> Send</button>
                            <button data-dismiss="modal" class="btn btn-secondary btn-sm"><span class="fa fa-remove"></span> Cancel</button>
                        </div>    
                    </div>
                </div>
            </div>
            <div class="modal fade" id="addpattern">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h6><b>Add Winning Pattern</b></h6>
                            <button data-dismiss="modal" class="btn">&times;</button>
                        </div>
                        <div class="modal-body pl-5 pr-5">
                            <div class="card-con p-4">
                                <div class="modal-header p-1 card-head">
                                    <h5>B</h5>
                                    <h5>I</h5>
                                    <h5>N</h5>
                                    <h5>G</h5>
                                    <img class="card-icon" src="../../public/assets/icons/favicon.ico" alt="">
                                </div>
                                <div class="modal-header p-1 mh-custom mb-4 mt-3">
                                    <?php
                                        for ($i=0; $i < 5; $i++) { 
                                            echo('<button onClick="setwp(\''.'r0.'.$i.'\')" id="'.'r0.'.$i.'" class="btn btn-primary">'.'</button>');
                                        }
                                    ?>
                                </div>
                                <div class="modal-header p-1 mh-custom mb-4">
                                    <?php
                                        for ($i=0; $i < 5; $i++) { 
                                            echo('<button onClick="setwp(\''.'r1.'.$i.'\')" id="'.'r1.'.$i.'" class="btn btn-primary">'.'</button>');
                                        }
                                    ?>
                                </div>
                                <div class="modal-header p-1 mh-custom mb-4">
                                    <?php
                                        for ($i=0; $i < 5; $i++) {
                                            if($i == 2){
                                                echo('<button class="btn"><img class="card-icon" src="../../public/assets/icons/favicon.ico" alt=""></button>');
                                            }else{
                                                echo('<button onClick="setwp(\''.'r2.'.$i.'\')" id="'.'r2.'.$i.'" class="btn btn-primary">'.'</button>');   
                                            }
                                        }
                                    ?>
                                </div>
                                <div class="modal-header p-1 mh-custom mb-4">
                                    <?php
                                        for ($i=0; $i < 5; $i++) { 
                                            echo('<button onClick="setwp(\''.'r3.'.$i.'\')" id="'.'r3.'.$i.'" class="btn btn-primary">'.'</button>');
                                        }
                                    ?>
                                </div>
                                <div class="modal-header p-1 mh-custom mb-4">
                                    <?php
                                        for ($i=0; $i < 5; $i++) { 
                                            echo('<button onClick="setwp(\''.'r4.'.$i.'\')" id="'.'r4.'.$i.'" class="btn btn-primary">'.'</button>');
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <form action="GMWinningPatternController?store" method="post">
                            <div class="modal-footer">
                                <button class="btn btn-success btn-sm"><span class="fa fa-plus"></span> Add</button>
                            </div>
                        </form>    
                    </div>
                </div>
            </div>
            <div class="modal fade" id="winningpattern">
                <div class="modal-dialog" style="margin: 0px; padding: 0px;">
                    <div class="modal-content" style="width: 100vw; height: 100vh;">
                        <div class="modal-header">
                            <h6><b>Winning Patterns</b></h6>
                            <button data-dismiss="modal" class="btn">&times;</button>
                        </div>
                        <div class="modal-body row" style="overflow-y: scroll;">
                            <?php foreach($dd["wpdata"] as $wp){ ?>
                                <div class="col-md-3 mb-4">
                                    <div class="card-con">
                                        <div class="modal-header p-1 card-head">
                                            <h5>B</h5>
                                            <h5>I</h5>
                                            <h5>N</h5>
                                            <h5>G</h5>
                                            <img class="card-icon" src="../../public/assets/icons/favicon.ico" alt="">
                                        </div>
                                        <div class="modal-header p-1 mh-custom-playas">
                                            <?php
                                                if($wp["r0"] != "null"){
                                                    $r0 = str_split($wp["r0"]);
                                                    for ($i=0; $i < 5; $i++) {
                                                        if(in_array($i,$r0)){
                                                            echo('<button class="btn btn-warning"></button>');

                                                        }else{
                                                            echo('<button class="btn btn-primary"></button>');
                                                        }
                                                    }
                                                }else{
                                                    for ($i=0; $i < 5; $i++) { 
                                                        echo('<button class="btn btn-primary"></button>');
                                                    }
                                                }
                                            ?>
                                        </div>
                                        <div class="modal-header p-1 mh-custom-playas">
                                            <?php
                                                if($wp["r1"] != "null"){
                                                    $r1 = str_split($wp["r1"]);
                                                    for ($i=0; $i < 5; $i++) {
                                                        if(in_array($i,$r1)){
                                                            echo('<button class="btn btn-warning"></button>');

                                                        }else{
                                                            echo('<button class="btn btn-primary"></button>');
                                                        }
                                                    }
                                                }else{
                                                    for ($i=0; $i < 5; $i++) { 
                                                        echo('<button class="btn btn-primary"></button>');
                                                    }
                                                }
                                            ?>
                                        </div>
                                        <div class="modal-header p-1 mh-custom-playas">
                                            <?php
                                                if($wp["r2"] != "null"){
                                                    $r2 = str_split($wp["r2"]);
                                                    for ($i=0; $i < 5; $i++) {
                                                        if($i == 2){
                                                            echo('<button class="btn"><img class="card-icon" src="../../public/assets/icons/favicon.ico" alt=""></button>');
                                                        }else{
                                                            if(in_array($i,$r2)){
                                                                echo('<button class="btn btn-warning"></button>');

                                                            }else{
                                                                echo('<button class="btn btn-primary"></button>');
                                                            }    
                                                        }
                                                        
                                                    }
                                                }else{
                                                    for ($i=0; $i < 5; $i++) {
                                                        if($i == 2){
                                                            echo('<button class="btn"><img class="card-icon" src="../../public/assets/icons/favicon.ico" alt=""></button>');
                                                        }else{
                                                            echo('<button class="btn btn-primary"></button>');
                                                        }
                                                    }
                                                }
                                            ?>
                                        </div>
                                        <div class="modal-header p-1 mh-custom-playas">
                                            <?php
                                                if($wp["r3"] != "null"){
                                                    $r3 = str_split($wp["r3"]);
                                                    for ($i=0; $i < 5; $i++) {
                                                        if(in_array($i,$r3)){
                                                            echo('<button class="btn btn-warning"></button>');

                                                        }else{
                                                            echo('<button class="btn btn-primary"></button>');
                                                        }
                                                    }
                                                }else{
                                                    for ($i=0; $i < 5; $i++) { 
                                                        echo('<button class="btn btn-primary"></button>');
                                                    }
                                                }
                                            ?>
                                        </div>
                                        <div class="modal-header p-1 mh-custom-playas">
                                            <?php
                                                if($wp["r4"] != "null"){
                                                    $r4 = str_split($wp["r4"]);
                                                    for ($i=0; $i < 5; $i++) {
                                                        if(in_array($i,$r4)){
                                                            echo('<button class="btn btn-warning"></button>');

                                                        }else{
                                                            echo('<button class="btn btn-primary"></button>');
                                                        }
                                                    }
                                                }else{
                                                    for ($i=0; $i < 5; $i++) { 
                                                        echo('<button class="btn btn-primary"></button>');
                                                    }
                                                }
                                            ?>
                                        </div>
                                        <form action="GMWinningPatternController?destroy" method="post">
                                            <input type="hidden" name="id" value="<?php echo($wp["id"]); ?>">
                                            <div class="modal-footer" style="padding: 0px;">
                                                <button class="btn btn-danger btn-sm"><span class="fa fa-remove"></span> Remove</button>
                                            </div>    
                                        </form>
                                    </div>    
                                </div>
                            <?php } ?>
                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-primary btn-sm"><span class="fa fa-check"></span> OK</button>
                            <a href="GMWinningPatternController?update" class="btn btn-danger btn-sm"><span class="fa fa-remove"></span> Remove All</a>
                        </div>    
                    </div>
                </div>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7" style="display: <?php if(!Data::unload("playas")){ echo("none"); } ?>;">
                        <?php Route::view("gm.playas"); ?>
                    </div>
                    <div class="col-md-7" style="display: <?php if(Data::unload("playas")){ echo("none"); } ?>;">
                        <h6><b>Players</b></h6>
                        <div class="modal-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>Player ID</td>
                                        <td>Name</td>
                                        <td>Balance</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody id="playerdisplay">
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="modal-header">
                            <h6 id="playernum"><span class="fa fa-users"></span> Number of Players: </h6>
                            <h6 id="cardrate"><span class="fa fa-line-chart"></span> Card Rate: </h6>
                            <h6 id="jackpot"><span class="fa fa-money"></span> Jackpot: </h6>
                        </div>
                        <div class="modal-header">
                            <h6><span class="fa fa-bell"></span> Winner Request: <span id="wr">0</span></h6>
                            <a href="ReviewWinnerRequestController" class="btn btn-sm btn-secondary" target="_blank">Review</a>
                        </div>
                        <button data-toggle="modal" data-target="#sendmessage" class="btn alert-warning col p-2 mb-2"><b><span class="fa fa-envelope"></span> Send Message</b></button>
                        <button data-toggle="modal" data-target="#playas" class="btn alert-warning col p-2 mb-2"  style="display: <?php if(Data::unload("playas")){ echo("none"); } ?>;"><b><span class="fa fa-user"></span> Play As</b></button>
                        <a href="GMPlayAsController?destroy" class="btn alert-warning col p-2 mb-2" style="display: <?php if(!Data::unload("playas")){ echo("none"); } ?>;"><b><span class="fa fa-list"></span> Player List</b></a>
                        <a href="#" data-toggle="modal" data-target="#addpattern" class="btn alert-primary col p-2 mb-2" ><b><span class="fa fa-plus"></span> Add Winning Pattern</b></a>
                        <a href="#" data-toggle="modal" data-target="#winningpattern" class="btn alert-success col p-2 mb-2" ><b><span class="fa fa-list"></span> Winning Patterns</b></a>
                        <div class="btn alert-success col p-2"><b>Current Draw</b></div>
                        <h2 class="m-4" style="text-align: center;" id="tab">...</h2>
                        <div class="btn alert-primary col p-2"><b>Previous Draw</b></div>
                        <hr>
                        <div id="tabCon">
                            <div class="row" id="tabs">
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            
            setInterval(gmboard, 1000);
            function gmboard(){
                fetch("GMPlayerController")
                .then(res => res.json())
                .then(res => updateplayerdisplay(res))

                messageService(<?php echo(Data::unload("playas-player")[0]["id"]);?>);

                fetch("AutoClickBallController")
                .then(res => res.json())
                .then(res => autoClickBall(res,<?php Data::json_response(Data::unload("balls-playas"));?>))
            }
            function updateplayerdisplay(data){
                var playerdisplay = document.getElementById("playerdisplay");
                playerdisplay.innerHTML = "";

                for (let index = 0; index < data.length; index++) {
                    playerdisplay.insertAdjacentHTML("afterbegin",'<tr><td>'+ data[index]["pid"] +'</td><td>'+ data[index]["name"] +'</td><td>'+ data[index]["amount"] +'</td><td><button onclick="removeplayer('+ data[index]["id"] +')" class="btn btn-sm btn-danger"><span class="fa fa-trash"></span> Remove</button></td></tr>');
                    
                }
                var playernum = document.getElementById("playernum");
                playernum.innerHTML = '<span class="fa fa-users"></span> Number of Players: ' + data.length;
                gameStandBy();
            }
            function gameStandBy(){
                fetch("GameStatusController")
                .then(res => res.json())
                .then(res => display(res))
            }
            function display(data){
                var tab = document.getElementById("tab");
                tab.innerHTML = data[0]["tab"]
                var jackpot = document.getElementById("jackpot");
                jackpot.innerHTML = '<span class="fa fa-money"></span> Jackpot: '  + data[0]["jackpot"]
                var rate = document.getElementById("cardrate");
                rate.innerHTML = '<span class="fa fa-line-chart"></span> Card Rate: ' + data[0]["rate"];

                //button display
                if(data[0]["status"] == "ongame"){
                    var btnstart = document.getElementById("btnstart");
                    btnstart.style.display = "none";
                    var btnpause = document.getElementById("btnpause");
                    btnpause.style.display = "";
                    var btnrandom = document.getElementById("btnrandom");
                    btnrandom.style.display = "";
                    var btnreset = document.getElementById("btnreset");
                    btnreset.style.display = "none";
                    var btnrate = document.getElementById("btnrate");
                    btnrate.style.display = "none";
                    var cardnum = document.getElementById("cardnum");
                    cardnum.style.display = "none";
                }
                if(data[0]["status"] == "pause"){
                    var btnstart = document.getElementById("btnstart");
                    btnstart.style.display = "";
                    var btnpause = document.getElementById("btnpause");
                    btnpause.style.display = "none";
                    var btnreset = document.getElementById("btnreset");
                    btnreset.style.display = "";
                    var btnrandom = document.getElementById("btnrandom");
                    btnrandom.style.display = "none";
                    var btnrate = document.getElementById("btnrate");
                    btnrate.style.display = "none";
                    var cardnum = document.getElementById("cardnum");
                    cardnum.style.display = "none";
                }
                if(data[0]["status"] == "pending"){
                    var btnstart = document.getElementById("btnstart");
                    btnstart.style.display = "";
                    var btnpause = document.getElementById("btnpause");
                    btnpause.style.display = "none";
                    var btnrandom = document.getElementById("btnrandom");
                    btnrandom.style.display = "none";
                    var btnrate = document.getElementById("btnrate");
                    btnrate.style.display = "";
                    var cardnum = document.getElementById("cardnum");
                    cardnum.style.display = "";
                }
                tabdisplay();
            }
            function tabdisplay(){
                fetch("TabController")
                .then(res => res.json())
                .then(res => displays(res))
            }
            function displays(data){
                var tabs = document.getElementById("tabs");
                tabs.remove();
                var tabcons = document.getElementById("tabCon");
                tabcons.insertAdjacentHTML("afterbegin",'<div class="row" id="tabs"></div>');
                var tabs = document.getElementById("tabs");
                for (let index = 0; index < data.length; index++) {
                    tabs.insertAdjacentHTML("afterbegin",'<div class="col-md-2 mt-2"><h5>'+ data[index]["tabs"] +'</h5></div>');
                }
                fetch("WinnerRequestController?show")
                .then(res => res.json())
                .then(res => wrdisplay(res))
            }
            function wrdisplay(data){
                var wr = document.getElementById("wr");
                var pidwr = document.getElementById("pidwr");

                if(data.length != wr.innerText){
                    wr.innerText = data.length;
                    var pid = 0;
                    for (let index = 0; index < data.length; index++) {
                        pid = data[index]["pid"];
                    }
                    fetch("WinnerRequestController?store",  {
                        method: 'POST',
                        body: new URLSearchParams('pid=' + pid)
                    })
                    .then(res => res.json())
                    .then(res => wrnotif(res))
                }else{
                    wr.innerText = data.length;
                }
            }
            function wrnotif(data){
                successMessage("Player " + data[0]["name"] + " submitted a winner request");
            }
            function removeplayer(id){
                fetch("GMPlayerController?destroy", {
                    method: 'POST',
                    body: new URLSearchParams('id=' + id)
                })
                .then(res => res.json())
                .then(res => infoMessage(res))
            }
            function randomize(){
                fetch("GMRandomizerController")
                .then(res => res.json())
                .then(res => randomizenotif(res))
            }
            function randomizenotif(data){
                if(data){
                    welcomeMessage(data);
                }
            }
            function startgame(){
                fetch("GMStartController")
                .then(res => res.text())
            }
            function pausegame(){
                fetch("GMStartController?store")
            }
            function resetgame(){
                fetch("GMResetController")
                infoMessage("Resetting Game...");
            }
            function winnerrequest(cid){
                fetch("WinnerRequestController", {
                    method: 'POST',
                    body: new URLSearchParams('pid=' + <?php echo(Data::unload("playas-player")[0]["id"]);?> + '& cid=' + cid)
                })

                var btnwr = document.getElementById(cid);
                btnwr.style.display = "none";
            }
            function autoClickBall(gm,data){
                if(data.length && gm){
                    for (let index = 0; index < data.length; index++) {
                        var button = document.getElementById(data[index])
                        if(button.innerText == gm){
                            if(button.classList.length > 1){
                                button.classList.remove("btn-primary")
                                button.classList.add("btn-success")
                            }
                        }
                    }
                }
            }
        </script>
    </body>
</html>