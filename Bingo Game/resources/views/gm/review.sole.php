<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
    <body>
        <?php if(!Data::unload("auth-gm")){ Route::return("HomeController"); }?>
        <div class="modal-body">
            <div class="modal-header alert-success">
                <h5><b>Player's Winner Request</b></h5>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-7">
                        <div class="modal-header">
                            <h6><b>Players</b></h6>
                            <div>
                                <a href="GMHomeController" class="btn btn-primary btn-sm"><span class="fa fa-home"></span> Return Home</a>   
                                <button data-toggle="modal" data-target="#releasejackpot" class="btn btn-success btn-sm"><span class="fa fa-credit-card"></span> Release Jackpot</button>       
                            </div>    
                        </div>
                        <div class="modal fade" id="releasejackpot">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h6><b>Release Jackpot Confirmation</b></h6>
                                        <button data-dismiss="modal" class="btn">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <h6>Do you wish to proceed? this can't be undone.</h6>
                                    </div>
                                    <div class="modal-footer">
                                        <a href="ReleaseJackpotController" class="btn btn-success btn-sm"><span class="fa fa-save"></span> Proceed</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-body">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>Player ID</td>
                                        <td>Name</td>
                                        <td>Card ID</td>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        foreach($dd["wrdata"] as $wr){
                                            foreach($dd["playerdata"] as $player){
                                                if($wr["pid"] == $player["id"]){
                                    ?>
                                        <tr>
                                            <td><?php echo($player["id"]); ?></td>
                                            <td><?php echo($player["name"]); ?></td>
                                            <td><?php echo($wr["cid"]); ?></td>
                                            <td><button data-toggle="modal" data-target="#card<?php echo($wr["id"]); ?>" class="btn btn-sm btn-success"><span class="fa fa-book"></span> Review</button></td>
                                        </tr>
                                        <?php
                                            foreach($dd["carddata"] as $card){
                                                if($card["cid"] == $wr["cid"]){
                                        ?>
                                            <div class="modal fade" id="card<?php echo($wr["id"]); ?>">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h6><b>Card Owner:</b> <?php echo($player["name"]); ?></h6>
                                                            <button data-dismiss="modal" class="btn">&times;</button>
                                                        </div>
                                                        <div class="modal-body pl-5 pr-5">
                                                            <div class="card-con">
                                                                <div class="modal-header card-head">
                                                                    <h5>B</h5>
                                                                    <h5>I</h5>
                                                                    <h5>N</h5>
                                                                    <h5>G</h5>
                                                                    <img class="card-icon" src="../../public/assets/icons/favicon.ico" alt="">
                                                                </div>
                                                                <div class="modal-header mh-custom">
                                                                    <?php
                                                                        $ra = explode("/",$card["ra"]);
                                                                        for ($i=0; $i < count($ra); $i++) { 
                                                                            if(in_array($ra[$i], $dd["tabdata"])){
                                                                                echo('<button class="btn btn-success">'.$ra[$i].'</button>');
                                                                            }else{
                                                                                echo('<button class="btn btn-primary">'.$ra[$i].'</button>');
                                                                            }
                                                                        }
                                                                    ?>
                                                                </div>
                                                                <div class="modal-header mh-custom">
                                                                        <?php
                                                                        $rb = explode("/",$card["rb"]);
                                                                        for ($i=0; $i < count($rb); $i++) { 
                                                                            if(in_array($rb[$i], $dd["tabdata"])){
                                                                                echo('<button class="btn btn-success">'.$rb[$i].'</button>');
                                                                            }else{
                                                                                echo('<button class="btn btn-primary">'.$rb[$i].'</button>');
                                                                            }
                                                                        }
                                                                    ?>
                                                                </div>
                                                                <div class="modal-header mh-custom">
                                                                    <?php
                                                                        $rc = explode("/",$card["rc"]);
                                                                        for ($i=0; $i < count($rc); $i++) { 
                                                                            if($rc[$i] == "sole"){
                                                                                echo('<button class="btn"><img class="card-icon" src="../../public/assets/icons/favicon.ico" alt=""></button>');
                                                                            }else{
                                                                                if(in_array($rc[$i], $dd["tabdata"])){
                                                                                    echo('<button class="btn btn-success">'.$rc[$i].'</button>');
                                                                                }else{
                                                                                    echo('<button class="btn btn-primary">'.$rc[$i].'</button>');
                                                                                }
                                                                            }
                                                                        }
                                                                    ?>
                                                                </div>
                                                                <div class="modal-header mh-custom">
                                                                    <?php
                                                                        $rd = explode("/",$card["rd"]);
                                                                        for ($i=0; $i < count($rd); $i++) { 
                                                                            if(in_array($rd[$i], $dd["tabdata"])){
                                                                                echo('<button class="btn btn-success">'.$rd[$i].'</button>');
                                                                            }else{
                                                                                echo('<button class="btn btn-primary">'.$rd[$i].'</button>');
                                                                            }
                                                                        }
                                                                    ?>
                                                                </div>
                                                                <div class="modal-header mh-custom">
                                                                    <?php
                                                                        $re = explode("/",$card["re"]);
                                                                        for ($i=0; $i < count($re); $i++) { 
                                                                            if(in_array($re[$i], $dd["tabdata"])){
                                                                                echo('<button class="btn btn-success">'.$re[$i].'</button>');
                                                                            }else{
                                                                                echo('<button class="btn btn-primary">'.$re[$i].'</button>');
                                                                            }
                                                                        }
                                                                    ?>
                                                                </div>
                                                                <hr>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <h6 id="option<?php echo($wr['id']); ?>"></h6>
                                                            <div id="cardoption<?php echo($wr['id']); ?>">
                                                                <button onclick="winnerconfirm('<?php echo($wr['id']); ?>')" class="btn btn-success btn-sm"><span class="fa fa-check"></span> Confirm</button>
                                                                <button onclick="winnerdecline('<?php echo($wr['id']); ?>')" class="btn btn-danger btn-sm"><span class="fa fa-remove"></span> Decline</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php 
                                                }
                                            }
                                        ?>
                                        
                                    <?php
                                                }
                                            }
                                        }
                                    ?>
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="btn alert-success col p-2"><b>Current Draw</b></div>
                        <h2 class="m-4" style="text-align: center;" id="tab"></h2>
                        <div class="btn alert-primary col p-2"><b>Previous Draw</b></div>
                        <hr>
                        <div id="tabCon">
                            <div class="row" id="tabs">
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            setInterval(gameStandBy, 1000);
            // function gmboard(){
            //     fetch("ReviewWinnerRequestController?show")
            //     .then(res => res.json())
            //     .then(res => console.log(res))
            // }
            // function updateplayerdisplay(data){
            //     var playerdisplay = document.getElementById("playerdisplay");
            //     playerdisplay.innerHTML = "";

            //     for (let index = 0; index < data.length; index++) {
            //         playerdisplay.insertAdjacentHTML("afterbegin",'<tr><td>'+ data[index]["pid"] +'</td><td>'+ data[index]["name"] +'</td><td>'+ data[index]["amount"] +'</td><td><button onclick="removeplayer('+ data[index]["id"] +')" class="btn btn-sm btn-danger"><span class="fa fa-trash"></span> Remove</button></td></tr>');
                    
            //     }
            //     gameStandBy();
            // }
            function gameStandBy(){
                fetch("GameStatusController")
                .then(res => res.json())
                .then(res => display(res))
            }
            function display(data){
                var tab = document.getElementById("tab");
                tab.innerText = data[0]["tab"];
                tabdisplay();
            }
            function tabdisplay(){
                fetch("TabController")
                .then(res => res.json())
                .then(res => displays(res))
            }
            function displays(data){
                var tabs = document.getElementById("tabs");
                tabs.remove();
                var tabcons = document.getElementById("tabCon");
                tabcons.insertAdjacentHTML("afterbegin",'<div class="row" id="tabs"></div>');
                var tabs = document.getElementById("tabs");
                for (let index = 0; index < data.length; index++) {
                    tabs.insertAdjacentHTML("afterbegin",'<div class="col-md-2 mt-2"><h5>'+ data[index]["tabs"] +'</h5></div>');
                }
            }
            function winnerconfirm(id){
                fetch("WinnerRequestController?update", {
                    method: 'POST',
                    body: new URLSearchParams('id=' + id +'&status=accepted')
                })
                hidecardoption(id,"Accepted")
            }
            function winnerdecline(id){
                fetch("WinnerRequestController?update", {
                    method: 'POST',
                    body: new URLSearchParams('id=' + id +'&status=declined')
                })
                hidecardoption(id,"Declined")
            }
            function hidecardoption(id, message){
                var cardoption = document.getElementById("cardoption"+id);
                cardoption.style.display = "none";

                var option = document.getElementById("option"+id);
                option.innerHTML = "<b>"+ message +"</b>";
            }
        </script>
    </body>
</html>