<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
    <body>
        <div class="bg-img">
            <div class="bg-trnsp">
                <?php if(!Data::unload("auth-gm")){ Route::return("HomeController"); }?>
                <div class="card-cred">
                    <div class="modal-header alert-primary">
                        <div style="display: flex;">
                            <img class="card-icon mr-1" src="../../public/assets/icons/favicon.ico" alt="">
                            <h5 class="m-1">SoloLeveler BINGO Game</h5>    
                        </div>
                    </div>
                    <?php
                        if(Data::unload("Message")){
                    ?>
                        <div class="modal-header alert alert-warning mt-1" id="alertcard">
                            <h5><?php echo Data::unload("Message"); ?></h5>
                        </div>
                    <?php
                        }
                        Data::trash("Message");
                    ?>
                    <div class="modal-body">
                        <form action="GMPlayerRegistrationController?store" method="post">
                            <div class="modal-header">
                                <h6><b>Player Registration</b></h6>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <h6>Full Name</h6>
                                    <input required class="form-control" type="text" name="name">     
                                </div>
                                <div class="form-group">
                                    <h6>Email Address</h6>
                                    <input required class="form-control" type="email" name="email">     
                                </div>
                                <div class="form-group">
                                    <h6>Code Name</h6>
                                    <input required class="form-control" type="text" name="codename">     
                                </div>
                                <div class="form-group">
                                    <h6>Amount</h6>
                                    <input required class="form-control" type="number" name="amount">     
                                </div>
                            </div>
                            <div class="modal-footer">
                                <a href="GMHomeController?show" class="btn btn-secondary btn-sm"><span class="fa fa-remove"></span> Cancel</a>
                                <button class="btn btn-primary btn-sm"><span class="fa fa-save"></span> Submit</button>
                            </div>
                        </form>
                    </div>    
                </div>        
            </div>
        </div>
    </body>
</html>