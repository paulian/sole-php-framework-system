<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
    <body>
        <div class="bg-img">
            <div class="bg-trnsp">
                <?php if(!Data::unload("auth-gm")){ Route::return("HomeController"); }?>
                <div class="card-cred">
                    <div class="modal-header alert-dark">
                        <div style="display: flex;">
                            <img class="card-icon mr-1" src="../../public/assets/icons/favicon.ico" alt="">
                            <h5 class="m-1">SoloLeveler BINGO Game</h5>    
                        </div>
                    </div>
                    <?php
                        if(Data::unload("Message")){
                    ?>
                        <div class="modal-header alert alert-success mt-1" id="alertcard">
                            <h5><?php echo Data::unload("Message"); ?></h5>
                        </div>
                    <?php
                        }
                        Data::trash("Message");
                    ?>
                    <div class="modal-body">
                        <div class="modal-header">
                            <h6><b><span class="fa fa-list"></span> Transaction Menu</b></h6>
                        </div>
                        <div class="modal-body">
                            <a href="GMHomeController" class="form-group col btn p-3 alert-dark"><b><span class="fa fa-home"></span> Return Home</b></a>
                            <a href="GMPlayerRegistrationController" class="form-group col btn p-3 alert-primary"><b><span class="fa fa-user"></span> Create Player Account</b></a>
                            <a href="GMAccountBalanceController" class="form-group col btn p-3 alert-primary"><b><span class="fa fa-credit-card"></span> View Player Balance</b></a>
                            <a href="GMTransferBalanceController" class="form-group col btn p-3 alert-primary"><b><span class="fa fa-credit-card"></span> Transfer Balance</b></a>
                            <a href="GMDepositCashController" class="form-group col btn p-3 alert-success"><b><span class="fa fa-money"></span> Deposit Cash</b></a>
                            <a href="GMWithdrawCashController" class="form-group col btn p-3 alert-success"><b><span class="fa fa-money"></span> Withdraw Cash</b></a>
                        </div>
                    </div>    
                </div>        
            </div>
        </div>
    </body>
</html>