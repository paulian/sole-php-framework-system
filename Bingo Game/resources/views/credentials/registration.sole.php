<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
    <body>
        <div class="bg-img">
            <div class="bg-trnsp">
                <div class="card-cred">
                    <div class="modal-header alert-primary">
                        <div style="display: flex;">
                            <img class="card-icon mr-1" src="../../public/assets/icons/favicon.ico" alt="">
                            <h5 class="m-1">SoloLeveler BINGO Game</h5>    
                        </div>
                    </div>
                    <div class="modal-header">
                        <h6><b>Player Registration</b></h6>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <h6><i><b>Note:</b> Please seek for game master's assistance.</i></h6>    
                        </div>
                    </div>
                    <div class="modal-footer">
                        <a href="HomeController" class="btn btn-primary btn-sm"><span class="fa fa-check"></span> OK</a>
                    </div>   
                </div>        
            </div>
        </div>
    </body>
</html>