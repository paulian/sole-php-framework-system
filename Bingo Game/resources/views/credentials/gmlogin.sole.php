<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
    <body>
        <div class="bg-img">
            <div class="bg-trnsp">
                <div class="card-cred">
                    <div class="modal-header alert-dark">
                        <div style="display: flex;">
                            <img class="card-icon mr-1" src="../../public/assets/icons/favicon.ico" alt="">
                            <h5 class="m-1">SoloLeveler BINGO Game</h5>    
                        </div>
                    </div>
                    <?php
                        if(Data::unload("Message")){
                    ?>
                        <div class="modal-header alert alert-danger mt-1" id="alertcard">
                            <h5><?php echo Data::unload("Message"); ?></h5>
                        </div>
                    <?php
                        }
                        Data::trash("Message");
                    ?>
                    <div class="modal-body">
                        <form action="GMController?show" method="post">
                            <div class="modal-header">
                                <h6><b>Game Master Login</b></h6>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <h6>Username</h6>
                                    <input required class="form-control" type="password" name="username">     
                                </div>
                                <div class="form-group">
                                    <h6>Password</h6>
                                    <input required class="form-control" type="password" name="password">     
                                </div>
                            </div>
                            <div class="modal-footer">
                                <a href="HomeController" class="btn btn-secondary btn-sm"><span class="fa fa-remove"></span> Cancel</a>
                                <button class="btn btn-primary btn-sm"><span class="fa fa-sign-in"></span> Login</button>
                            </div>
                        </form>
                    </div>    
                </div>        
            </div>
        </div>
    </body>
</html>