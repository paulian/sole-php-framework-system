<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
    <body>
        <div class="bg-img">
            <div class="bg-trnsp">
                <div class="modal-header alert-secondary">
                    <div style="display: flex;">
                            <img class="card-icon mr-1" src="../../public/assets/icons/favicon.ico" alt="">
                            <h5 class="m-1">SoloLeveler BINGO Game</h5>    
                        </div>
                </div>
                <div class="modal-body">
                    <div class="modal-body">
                        <div class="row">
                            <a class="col-md-4 btn" href="RegistrationController">
                                <div class="modal-header btn btn-primary">
                                    <span class="fa fa-book"> Register</span>
                                </div>
                                <div class="modal-body alert-primary mt-1">
                                    <h6><i><b>Note: </b> Player should register only with the presence of the game master, for the purpose that the player should deposit the money to the game master upon registration. All player is monitored and reviewed by the game master.</i></h6>
                                </div>
                            </a>
                            <a class="col-md-4 btn" href="LoginController">
                                <div class="modal-header btn btn-success">
                                    <span class="fa fa-play"> Play Game</span>
                                </div>
                                <div class="modal-body alert-success mt-1">
                                    <h6><i><b>Note: </b> Use the codename as a login credential to enter the game. If a player have a problem logging into his/her account, it might be because the player register into the game without the game master and he/she did not deposit any money. Otherwise the player forgotten his/her codename and should seek for game master's assistance.</i></h6>
                                </div>
                            </a>
                            <a class="col-md-4 btn" href="GMController">
                                <div class="modal-header btn btn-dark">
                                    <span class="fa fa-user"> Game Master</span>
                                </div>
                                <div class="modal-body alert-dark mt-1">
                                    <h6><i><b>Note: </b> Only game master knows the rule.</i></h6>
                                </div>
                            </a>   
                        </div>
                    </div>
                </div>        
            </div>
        </div>
        
    </body>
</html>