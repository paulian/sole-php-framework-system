<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
    <body>
        <?php if(!Data::unload("auth-player")){ Route::return("HomeController"); }?>
        <div id="pausecard" style="display: none;" class="cfp alert-dark">
                <div class="ic alert-success">
                    <div class="row">
                        <img src="../../public/assets/icons/favicon.ico" alt="">
                        <h5 class="mt-3 ml-3" >SoloLeveler BINGO Game is Paused</h5>
                    </div>
                    <hr>
                    <h6 id="pausetitleplsub" class="ml-3" style="display: none;">Note: Player/s submitted a winner request, confirmation from the game master is needed.</h6>
                    <h6 id="pausetitlegmps" class="ml-3" style="display: none;">Note: Game master is busy, wait for further instruction.</h6>
                    <div id="pausenote" class="m-3 ml-5" style="">
                        <h6></h6>
                    </div>
                    <button data-toggle="modal" data-target="#sendmessage" class="btn alert-primary col p-2 mb-2"><b><span class="fa fa-envelope"></span> Send Message</b></button>
                </div>
            </div>
        <div class="modal-body">
            <div class="modal-header alert-success">
                <h6 class="mt-2"><span class="fa fa-id-card"></span> Player Name: <?php echo($dd["player"][0]["name"]);?></h6>
                <h6 class="mt-2"><span class="fa fa-money"></span> Balance: <?php echo($dd["player"][0]["amount"]);?></h6>
                <h6 class="mt-2" id="rate"><span class="fa fa-line-chart"></span> Card Rate: </h6>
                <h6 class="mt-2"><span class="fa fa-money"></span> Bet: <?php echo($dd["player"][0]["bet"]);?></h6>
                <h6 class="mt-2" id="jackpot"><span class="fa fa-money"></span> Jackpot: </h6>
                <div class="col-md-3" id="cardnum" style="display: ">
                    <form action="PlayerBoardController?store" method="post">
                        <div class="btn-group">
                            <input type="hidden" name="pid" value="<?php echo($dd["player"][0]["pid"]);?>">
                            <select required name="cardnumber" id="cardNumber" class="form-control col-md-10">
                                <option value="" disabled selected>Buy Cards</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                            </select>
                            <button type="submit" class="btn btn-success"><span class="fa fa-credit-card"></span></button>
                        </div>    
                    </form>
                </div>
            </div>
            <?php
                if(Data::unload("Message")){
            ?>
                <div class="modal-header alert alert-warning mt-1" id="alertcard">
                    <h5><?php echo Data::unload("Message"); ?></h5>
                </div>
            <?php
                }
                Data::trash("Message");
            ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8" style="height: 470px; overflow-y: scroll">
                        <div class="row">
                            <?php foreach($dd["cards"] as $card){ ?>
                            <div class="col-md-6  mb-2 mt-2">
                                <div class="card-con">
                                    <div class="modal-header card-head">
                                        <h5>B</h5>
                                        <h5>I</h5>
                                        <h5>N</h5>
                                        <h5>G</h5>
                                        <img class="card-icon" src="../../public/assets/icons/favicon.ico" alt="">
                                    </div>
                                    <div class="modal-header mh-custom">
                                        <?php
                                            $ra = explode("/",$card["ra"]);
                                            for ($i=0; $i < count($ra); $i++) { 
                                                $id = Data::generate(20,"alpha");
                                                echo('<button onClick="pick(\''.$id.'\')" id="'.$id.'" class="btn btn-primary">'.$ra[$i].'</button>');
                                            }
                                        ?>
                                    </div>
                                    <div class="modal-header mh-custom">
                                            <?php
                                            $rb = explode("/",$card["rb"]);
                                            for ($i=0; $i < count($rb); $i++) { 
                                                $id = Data::generate(20,"alpha");
                                                echo('<button onClick="pick(\''.$id.'\')" id="'.$id.'" class="btn btn-primary">'.$rb[$i].'</button>');
                                            }
                                        ?>
                                    </div>
                                    <div class="modal-header mh-custom">
                                        <?php
                                            $rc = explode("/",$card["rc"]);
                                            for ($i=0; $i < count($rc); $i++) { 
                                                if($rc[$i] == "sole"){
                                                    echo('<button class="btn"><img class="card-icon" src="../../public/assets/icons/favicon.ico" alt=""></button>');
                                                }else{
                                                    $id = Data::generate(20,"alpha");
                                                    echo('<button onClick="pick(\''.$id.'\')" id="'.$id.'" class="btn btn-primary">'.$rc[$i].'</button>');    
                                                }
                                            }
                                        ?>
                                    </div>
                                    <div class="modal-header mh-custom">
                                        <?php
                                            $rd = explode("/",$card["rd"]);
                                            for ($i=0; $i < count($rd); $i++) { 
                                                $id = Data::generate(20,"alpha");
                                                echo('<button onClick="pick(\''.$id.'\')" id="'.$id.'" class="btn btn-primary">'.$rd[$i].'</button>');
                                            }
                                        ?>
                                    </div>
                                    <div class="modal-header mh-custom">
                                        <?php
                                            $re = explode("/",$card["re"]);
                                            for ($i=0; $i < count($re); $i++) { 
                                                $id = Data::generate(20,"alpha");
                                                echo('<button onClick="pick(\''.$id.'\')" id="'.$id.'" class="btn btn-primary">'.$re[$i].'</button>');
                                            }
                                        ?>
                                    </div>
                                    <hr>
                                    <div style="display: flex; justify-content: space-between;">
                                        <h6>Card ID: <?php echo($card["cid"]); ?></h6>
                                        <button id="<?php echo($card['cid']); ?>" onclick="winnerrequest('<?php echo($card['cid']); ?>')" class="btn btn-secondary btn-sm">Bingo</button>
                                    </div>
                                </div>
                                      
                            </div>
                            <?php }?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <button data-toggle="modal" data-target="#sendmessage" class="btn alert-warning col p-2 mb-2"><b><span class="fa fa-envelope"></span> Send Message</b></button>
                        <a href="#" data-toggle="modal" data-target="#winningpattern" class="btn alert-success col p-2 mb-2" ><b><span class="fa fa-list"></span> Winning Patterns</b></a>
                        <a href="PlayerBoardController?destroy" class="btn alert-danger col p-2"><b><span class="fa fa-sign-out"></span> Logout</b></a>
                        <hr>
                        <div class="btn alert-success col p-2"><b>Current Draw</b></div>
                        <div style="text-align: center;">
                            <h1 id="tab">...</h1>
                        </div>
                        <div class="btn alert-primary col p-2"><b>Previous Draw</b></div>
                        <hr>
                        <div id="tabCon">
                            <div class="row" id="tabs">
                            </div>    
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="sendmessage">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6><b>Send Message</b></h6>
                        <button data-dismiss="modal" class="btn">&times;</button>
                    </div>
                    <div class="modal-body">
                        <textarea id="message" rows="5" class="form-control" placeholder="Aa"></textarea>
                    </div>
                    <div class="modal-footer">
                        <button onclick="sendMessage('message','<?php echo($dd['player'][0]['name']);?>')" class="btn btn-primary btn-sm"><span class="fa fa-send"></span> Send</button>
                        <button data-dismiss="modal" class="btn btn-secondary btn-sm"><span class="fa fa-remove"></span> Cancel</button>
                    </div>    
                </div>
            </div>
        </div>
        <div class="modal fade" id="winningpattern">
            <div class="modal-dialog" style="margin: 0px; padding: 0px;">
                <div class="modal-content" style="width: 100vw; height: 100vh;">
                    <div class="modal-header">
                        <h6><b>Winning Patterns</b></h6>
                        <button data-dismiss="modal" class="btn">&times;</button>
                    </div>
                    <div class="modal-body row" style="overflow-y: scroll;">
                        <?php foreach($dd["wpdata"] as $wp){ ?>
                            <div class="col-md-3 mb-4">
                                <div class="card-con">
                                    <div class="modal-header p-1 card-head">
                                        <h5>B</h5>
                                        <h5>I</h5>
                                        <h5>N</h5>
                                        <h5>G</h5>
                                        <img class="card-icon" src="../../public/assets/icons/favicon.ico" alt="">
                                    </div>
                                    <div class="modal-header p-1 mh-custom-playas">
                                        <?php
                                            if($wp["r0"] != "null"){
                                                $r0 = str_split($wp["r0"]);
                                                for ($i=0; $i < 5; $i++) {
                                                    if(in_array($i,$r0)){
                                                        echo('<button class="btn btn-warning"></button>');

                                                    }else{
                                                        echo('<button class="btn btn-primary"></button>');
                                                    }
                                                }
                                            }else{
                                                for ($i=0; $i < 5; $i++) { 
                                                    echo('<button class="btn btn-primary"></button>');
                                                }
                                            }
                                        ?>
                                    </div>
                                    <div class="modal-header p-1 mh-custom-playas">
                                        <?php
                                            if($wp["r1"] != "null"){
                                                $r1 = str_split($wp["r1"]);
                                                for ($i=0; $i < 5; $i++) {
                                                    if(in_array($i,$r1)){
                                                        echo('<button class="btn btn-warning"></button>');

                                                    }else{
                                                        echo('<button class="btn btn-primary"></button>');
                                                    }
                                                }
                                            }else{
                                                for ($i=0; $i < 5; $i++) { 
                                                    echo('<button class="btn btn-primary"></button>');
                                                }
                                            }
                                        ?>
                                    </div>
                                    <div class="modal-header p-1 mh-custom-playas">
                                        <?php
                                            if($wp["r2"] != "null"){
                                                $r2 = str_split($wp["r2"]);
                                                for ($i=0; $i < 5; $i++) {
                                                    if($i == 2){
                                                        echo('<button class="btn"><img class="card-icon" src="../../public/assets/icons/favicon.ico" alt=""></button>');
                                                    }else{
                                                        if(in_array($i,$r2)){
                                                            echo('<button class="btn btn-warning"></button>');

                                                        }else{
                                                            echo('<button class="btn btn-primary"></button>');
                                                        }    
                                                    }
                                                    
                                                }
                                            }else{
                                                for ($i=0; $i < 5; $i++) {
                                                    if($i == 2){
                                                        echo('<button class="btn"><img class="card-icon" src="../../public/assets/icons/favicon.ico" alt=""></button>');
                                                    }else{
                                                        echo('<button class="btn btn-primary"></button>');
                                                    }
                                                }
                                            }
                                        ?>
                                    </div>
                                    <div class="modal-header p-1 mh-custom-playas">
                                        <?php
                                            if($wp["r3"] != "null"){
                                                $r3 = str_split($wp["r3"]);
                                                for ($i=0; $i < 5; $i++) {
                                                    if(in_array($i,$r3)){
                                                        echo('<button class="btn btn-warning"></button>');

                                                    }else{
                                                        echo('<button class="btn btn-primary"></button>');
                                                    }
                                                }
                                            }else{
                                                for ($i=0; $i < 5; $i++) { 
                                                    echo('<button class="btn btn-primary"></button>');
                                                }
                                            }
                                        ?>
                                    </div>
                                    <div class="modal-header p-1 mh-custom-playas">
                                        <?php
                                            if($wp["r4"] != "null"){
                                                $r4 = str_split($wp["r4"]);
                                                for ($i=0; $i < 5; $i++) {
                                                    if(in_array($i,$r4)){
                                                        echo('<button class="btn btn-warning"></button>');

                                                    }else{
                                                        echo('<button class="btn btn-primary"></button>');
                                                    }
                                                }
                                            }else{
                                                for ($i=0; $i < 5; $i++) { 
                                                    echo('<button class="btn btn-primary"></button>');
                                                }
                                            }
                                        ?>
                                    </div>
                                </div>    
                            </div>
                        <?php } ?>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-primary btn-sm"><span class="fa fa-check"></span> OK</button>
                    </div>    
                </div>
            </div>
        </div>                                    
        <script>
            setInterval(gameStandBy, 1000);
            function gameStandBy(){
                fetch("GameStatusController")
                .then(res => res.json())
                .then(res => display(res))

                fetch("GameStatusController?show", {
                    method: 'POST',
                    body: new URLSearchParams('id=' + <?php echo($dd["player"][0]["id"]); ?>)
                })
                .then(res => res.json())
                .then(res => refreshpage(res))

                messageService(<?php echo($dd["player"][0]["id"]); ?>);

                // fetch("WinnerRequestController?show")
                // .then(res => res.json())
                // .then(res => pausecarddisplay(res))
            }
            function refreshpage(data){
                if(data[0]["refresh"] == "true"){
                    window.location.href = "";
                }
            }
            function display(data){
                var tab = document.getElementById("tab");
                tab.innerText = data[0]["tab"];
                var jackpot = document.getElementById("jackpot");
                jackpot.innerHTML = '<span class="fa fa-money"></span> Jackpot: ' + data[0]["jackpot"]
                var rate = document.getElementById("rate");
                rate.innerHTML = '<span class="fa fa-line-chart"></span> Card Rate: ' + data[0]["rate"]

                if(data[0]["status"] != "pending"){
                    var cardnum = document.getElementById("cardnum");
                    cardnum.style.display = "none";
                }else{
                    var cardnum = document.getElementById("cardnum");
                    cardnum.style.display = "";
                }
                if(data[0]["status"] == "pause"){
                    var pausecard = document.getElementById("pausecard");
                    pausecard.style.display = "";
                }else{
                    var pausecard = document.getElementById("pausecard");
                    pausecard.style.display = "none";
                }
                tabdisplay();
                pauseCard();
            }
            function tabdisplay(){
                fetch("TabController")
                .then(res => res.json())
                .then(res => displays(res))
            }
            function displays(data){
                var tabs = document.getElementById("tabs");
                tabs.remove();
                var tabcons = document.getElementById("tabCon");
                tabcons.insertAdjacentHTML("afterbegin",'<div class="row" id="tabs"></div>');
                var tabs = document.getElementById("tabs");
                for (let index = 0; index < data.length; index++) {
                    tabs.insertAdjacentHTML("afterbegin",'<div class="col-md-2"><h5>'+ data[index]["tabs"] +'</h5></div>');
                }
            }
            function winnerrequest(cid){
                fetch("WinnerRequestController", {
                    method: 'POST',
                    body: new URLSearchParams('pid=' + <?php echo($dd["player"][0]["id"]); ?> + '& cid=' + cid)
                })

                var btnwr = document.getElementById(cid);
                btnwr.style.display = "none";
            }
            function pauseCard(){
                fetch("PauseNoteController")
                .then(res => res.json())
                .then(res => pauseCarDdisplay(res))
            }
            function pauseCarDdisplay(data){
                if(data[0].length){
                    var pausetitleplsub = document.getElementById("pausetitleplsub");
                    pausetitleplsub.style.display = "";
                    var pausetitlegmps = document.getElementById("pausetitlegmps");
                    pausetitlegmps.style.display = "none";

                    var pausenote = document.getElementById("pausenote");
                    pausenote.innerHTML = "";

                    for (let index = 0; index < data[0].length; index++) {
                        for (let indexa = 0; indexa < data[1].length; indexa++) {
                            if(data[0][index]["pid"] == data[1][indexa]["id"]){
                                pausenote.insertAdjacentHTML("beforeend", '<h6>Request from player '+ data[1][indexa]["name"] +' with card ID <i>'+ data[0][index]["cid"] +'</i> is '+ data[0][index]["status"] +'.</h6>');
                            }
                        }
                    }

                }else{
                    var pausetitleplsub = document.getElementById("pausetitleplsub");
                    pausetitleplsub.style.display = "none";
                    var pausetitlegmps = document.getElementById("pausetitlegmps");
                    pausetitlegmps.style.display = "";
                }
            }
            // function pausecarddisplay(data){
            //     for (let index = 0; index < data.length; index++) {
            //         fetch("WinnerRequestController?store", {
            //             method: 'POST',
            //             body: new URLSearchParams('pid=' + data[index]["id"])
            //         })
            //         .then(res => res.json())
            //         .then(res => displaypausenote(res))
            //     }
            // }
            // function displaypausenote(data){
            //     console.log(data[0]["name"])
            // }
        </script>
    </body>
</html>