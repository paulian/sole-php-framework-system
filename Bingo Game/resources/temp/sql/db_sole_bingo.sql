-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 10, 2021 at 04:07 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_sole_bingo`
--

-- --------------------------------------------------------

--
-- Table structure for table `card`
--

CREATE TABLE `card` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `cid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ra` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rb` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rd` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `re` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `gamestatus`
--

CREATE TABLE `gamestatus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tab` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jackpot` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gamestatus`
--

INSERT INTO `gamestatus` (`id`, `status`, `tab`, `jackpot`, `rate`, `created_at`, `updated_at`) VALUES
(1, 'pending', '...', '0', '1', '2021-10-10 13:54:55', '2021-10-10 13:54:55');

-- --------------------------------------------------------

--
-- Table structure for table `gm`
--

CREATE TABLE `gm` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `gmid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gm`
--

INSERT INTO `gm` (`id`, `gmid`, `name`, `username`, `password`, `created_at`, `updated_at`) VALUES
(1, '6162f0afa11d4', 'Game Master', '92073d2fe26e543ce222cc0fb0b7d7a0', '92073d2fe26e543ce222cc0fb0b7d7a0', '2021-10-10 13:54:55', '2021-10-10 13:54:55');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`) VALUES
(1, '2021_10_10_251747_player'),
(2, '2021_10_10_318575_card'),
(3, '2021_10_10_384862_gm'),
(4, '2021_10_10_466499_gamestatus'),
(5, '2021_10_10_531914_tabs'),
(6, '2021_10_10_568026_winnerrequest'),
(7, '2021_10_10_615021_winningpattern');

-- --------------------------------------------------------

--
-- Table structure for table `player`
--

CREATE TABLE `player` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `pid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `codename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `refresh` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bet` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `player`
--

INSERT INTO `player` (`id`, `pid`, `name`, `email`, `codename`, `refresh`, `amount`, `bet`, `created_at`, `updated_at`) VALUES
(1, '6162f0b075d24', 'Game Master', 'gm@sole.ph', 'gm', 'false', '1000000000', '0', '2021-10-10 13:54:56', '2021-10-10 13:54:56');

-- --------------------------------------------------------

--
-- Table structure for table `tabs`
--

CREATE TABLE `tabs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tabs` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `winnerrequest`
--

CREATE TABLE `winnerrequest` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `wid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `winningpattern`
--

CREATE TABLE `winningpattern` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `wpid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r0` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r3` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `r4` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `winningpattern`
--

INSERT INTO `winningpattern` (`id`, `wpid`, `r0`, `r1`, `r2`, `r3`, `r4`, `created_at`, `updated_at`) VALUES
(1, '6162f0afafe8c', '0', '1', 'null', '3', '4', '2021-10-10 13:54:55', '2021-10-10 13:54:55'),
(2, '6162f0afb359e', '4', '3', 'null', '1', '0', '2021-10-10 13:54:55', '2021-10-10 13:54:55'),
(3, '6162f0afb5949', '01234', 'null', 'null', 'null', 'null', '2021-10-10 13:54:55', '2021-10-10 13:54:55'),
(4, '6162f0afb77de', 'null', '01234', 'null', 'null', 'null', '2021-10-10 13:54:55', '2021-10-10 13:54:55'),
(5, '6162f0afb9dd0', 'null', 'null', '0143', 'null', 'null', '2021-10-10 13:54:55', '2021-10-10 13:54:55'),
(6, '6162f0afc0195', 'null', 'null', 'null', '01243', 'null', '2021-10-10 13:54:55', '2021-10-10 13:54:55'),
(7, '6162f0afc1bc6', 'null', 'null', 'null', 'null', '01234', '2021-10-10 13:54:55', '2021-10-10 13:54:55'),
(8, '6162f0afc5ba5', '0', '0', '0', '0', '0', '2021-10-10 13:54:55', '2021-10-10 13:54:55'),
(9, '6162f0afc9e79', '1', '1', '1', '1', '1', '2021-10-10 13:54:55', '2021-10-10 13:54:55'),
(10, '6162f0afce25b', '2', '2', 'null', '2', '2', '2021-10-10 13:54:55', '2021-10-10 13:54:55'),
(11, '6162f0afd4008', '3', '3', '3', '3', '3', '2021-10-10 13:54:55', '2021-10-10 13:54:55'),
(12, '6162f0afd82f2', '4', '4', '4', '4', '4', '2021-10-10 13:54:55', '2021-10-10 13:54:55'),
(13, '6162f0afda318', '2', '1', '0', 'null', 'null', '2021-10-10 13:54:55', '2021-10-10 13:54:55'),
(14, '6162f0afde78e', 'null', 'null', '0', '1', '2', '2021-10-10 13:54:55', '2021-10-10 13:54:55'),
(15, '6162f0afe3bbd', 'null', 'null', '4', '3', '2', '2021-10-10 13:54:55', '2021-10-10 13:54:55'),
(16, '6162f0afe7b58', '2', '3', '4', 'null', 'null', '2021-10-10 13:54:55', '2021-10-10 13:54:55'),
(17, '6162f0afebfb1', '04', 'null', 'null', 'null', '40', '2021-10-10 13:54:55', '2021-10-10 13:54:55'),
(18, '6162f0aff03fb', '2', 'null', '04', 'null', '2', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(19, '6162f0b0025b1', '04', 'null', 'null', 'null', '2', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(20, '6162f0b00a360', '2', 'null', 'null', 'null', '04', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(21, '6162f0b0104e5', '0', 'null', '4', 'null', '0', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(22, '6162f0b014f76', '4', 'null', '0', 'null', '4', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(23, '6162f0b01d46b', 'null', '2', '13', '2', 'null', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(24, '6162f0b021d73', 'null', '13', 'null', '31', 'null', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(25, '6162f0b02678b', 'null', '123', '31', '321', 'null', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(26, '6162f0b02b189', '024', '31', '04', '13', '024', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(27, '6162f0b031906', '02134', '43210', '0134', '43210', '43210', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(28, '6162f0b0361c0', 'null', 'null', '3', '23', 'null', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(29, '6162f0b03d4d8', 'null', 'null', '1', '12', 'null', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(30, '6162f0b041b85', 'null', '12', '1', 'null', 'null', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(31, '6162f0b046385', 'null', '23', '3', 'null', 'null', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(32, '6162f0b04abbf', '01', '10', 'null', 'null', 'null', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(33, '6162f0b0510a3', '34', '34', 'null', 'null', 'null', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(34, '6162f0b0569ee', 'null', 'null', 'null', '34', '34', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(35, '6162f0b05b1b4', 'null', 'null', 'null', '10', '10', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(36, '6162f0b05f923', '12', '12', 'null', 'null', 'null', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(37, '6162f0b063ea7', '23', '23', 'null', 'null', 'null', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(38, '6162f0b0660ee', 'null', '34', '43', 'null', 'null', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(39, '6162f0b068b65', 'null', 'null', '34', '43', 'null', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(40, '6162f0b06aaa2', 'null', 'null', 'null', '32', '23', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(41, '6162f0b06ce7a', 'null', 'null', 'null', '21', '12', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(42, '6162f0b06ef94', 'null', 'null', '10', '01', 'null', '2021-10-10 13:54:56', '2021-10-10 13:54:56'),
(43, '6162f0b072120', 'null', '01', '10', 'null', 'null', '2021-10-10 13:54:56', '2021-10-10 13:54:56');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `card`
--
ALTER TABLE `card`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gamestatus`
--
ALTER TABLE `gamestatus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gm`
--
ALTER TABLE `gm`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabs`
--
ALTER TABLE `tabs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `winnerrequest`
--
ALTER TABLE `winnerrequest`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `winningpattern`
--
ALTER TABLE `winningpattern`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `card`
--
ALTER TABLE `card`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gamestatus`
--
ALTER TABLE `gamestatus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `gm`
--
ALTER TABLE `gm`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `player`
--
ALTER TABLE `player`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tabs`
--
ALTER TABLE `tabs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `winnerrequest`
--
ALTER TABLE `winnerrequest`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `winningpattern`
--
ALTER TABLE `winningpattern`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
