Sole PHP Framework
----------------------------------------------------------------------------------------------------------------------------------
This is a PHP Framework that works similarly as Laravel Framework

The purpose of this Framework is to make it easier to develop a web application as if you are using a Laravel Framework.
There are instance that instructors doesn't require any PHP Framework when they give student a web project,
specialy for those beginners in PHP.

And for those who wan't to learn basic PHP, I don't suggest using this Framework,
because this Framework have shortened the code for database management (SQL queries), file handling, file redirection, and etc.

Although it have quite much functions which it can manage, this is not close enough to be called a full
package framework. So use it at your own risk.
----------------------------------------------------------------------------------------------------------------------------------
To start using the framework please read first the documentation and refer to the Sample CRUD System.
If you have queries please inbox at "paulian.dumdum@gmail.com".
Thank you, enjoy and God bless.
----------------------------------------------------------------------------------------------------------------------------------

Sole PHP Framework
----------------------------------------------------------------------------------------------------------------------------------
Copyright © 2021 PID SoloLeveler.
Everyone is permitted to copy and distribute verbatim copies
of this license document, but changing it is not allowed.