<?php
    Migrate::$migration = ["DirFolderMigration","DirFileMigration"];

    class DirFolderMigration
    {
        public static function index(){
            Migrate::attrib_table("dirfolder");
            Migrate::attrib_string(255);
            Migrate::string("fid");
            Migrate::string("name");
            Migrate::string("color");
        }
    }

    class DirFileMigration
    {
        public static function index(){
            Migrate::attrib_table("dirfile");
            Migrate::attrib_string(255);
            Migrate::string("fid");
            Migrate::string("name");
            Migrate::string("file");
            Migrate::string("location");
            Migrate::string("type");
            Migrate::string("size");
        }
    }
?>