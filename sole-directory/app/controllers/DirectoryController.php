<?php
    include('../../vendor/invoker/invoke.php');
    class DirectoryController{
        public static function index(){
            $folder = new DirFolder;
            $folderdata = DB::where($folder,"fid","=",array_reverse(Data::unload("path"))[0][0],"name","asc");
            $file = new DirFile;
            $filedata = DB::where($file,"fid","=",array_reverse(Data::unload("path"))[0][0],"name","asc");
            $icon = [
                "VIDEO" => "fa-file-video-o",
                "DOCX" => "fa-file-word-o",
                "IMAGE" => "fa-file-image-o",
                "SOUND" => "fa-file-audio-o",
                "PDF" => "fa-file-pdf-o",
                "PPTX" => "fa-file-powerpoint-o",
                "ARCHIVE" => "fa-file-archive-o",
                "XLSX" => "fa-file-excel-o",
                "FILE" => "fa-file-o"
            ];
            $type = [
                "VIDEO" => "Video File",
                "DOCX" => "Microsoft Word Document",
                "IMAGE" => "Image File",
                "SOUND" => "Audio File",
                "PDF" => "PDF Document File",
                "PPTX" => "Microsoft Powerpoint Presentation",
                "ARCHIVE" => "Archive File",
                "XLSX" => "Microsoft Excel Worksheet",
                "FILE" => "File"
            ];
            $preview = ["FILE","VIDEO","SOUND","PDF","IMAGE"];

            $res = json_decode(file_get_contents("../../resources/temp/json/cm.json"));
            $res->path = "";
            $res->files = [];
            $res->action = "";
            file_put_contents("../../resources/temp/json/cm.json",json_encode($res));

            Route::view("directory", compact("folderdata","filedata","icon","type","preview"));
        }
        public static function store(){
            //code here...
        }
        public static function show(){
            //code here...
        }
        public static function update(){
            //code here...
        }
        public static function destroy(){
            //code here...
        }
        public static function handler(Request $request){
            //code here...
        }
    }
?>