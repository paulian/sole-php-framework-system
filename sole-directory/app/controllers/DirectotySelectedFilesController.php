<?php
    include('../../vendor/invoker/invoke.api.php');
    class DirectotySelectedFilesController{
        public static function index(){
            Data::json_response(Data::unload("path"));
        }
        public static function store(Request $request){
            if($request->path == 0 || $request->path == null){
                $res = json_decode(file_get_contents("../../resources/temp/json/cm.json"));
                $res->path = 0;
                $res->files = explode(',',$request->files);
                $res->action = $request->action;
                file_put_contents("../../resources/temp/json/cm.json",json_encode($res));
                Data::json_response($res);   
            }else{
                $res = json_decode(file_get_contents("../../resources/temp/json/cm.json"));
                $res->path = $request->path;
                $res->files = explode(',',$request->files);
                $res->action = $request->action;
                file_put_contents("../../resources/temp/json/cm.json",json_encode($res));
                Data::json_response($res); 
            }
        }
        public static function show(){
            //code here...
        }
        public static function update(Request $request){
            $res = json_decode(file_get_contents("../../resources/temp/json/cm.json"));
                $res->path = null;
                $res->files = explode(',',$request->files);
                $res->action = "delete";
                file_put_contents("../../resources/temp/json/cm.json",json_encode($res));
                Data::json_response($res); 
        }
        public static function destroy(){
            //code here...
        }
        public static function handler(Request $request){
            $folder = new DirFolder;
            Data::json_response(DB::where($folder,"fid","=",$request->id,"name","asc"));
        }
    }
?>