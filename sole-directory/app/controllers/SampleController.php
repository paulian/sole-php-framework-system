<?php
    include('../../vendor/invoker/invoke.php');
    class SampleController{
        public static function index(){
            QR::wipe();
            $faker = new Faker;
            $res = QR::logo_create("I am ".$faker->name()." from ".$faker->address().".");
            $qr = $res["path"].$res["file"];
            Route::view("home",compact("qr"));
        }
        public static function store(){
            //code here...
        }
        public static function show(){
            //code here...
        }
        public static function update(){
            //code here...
        }
        public static function destroy(){
            //code here...
        }
        public static function handler(Request $request){
            //code here...
        }
    }
?>