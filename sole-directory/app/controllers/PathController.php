<?php
    include('../../vendor/invoker/invoke.php');
    class PathController{
        public static function index(){
            //code here...
        }
        public static function store(){
            //code here...
        }
        public static function show(){
            //code here...
        }
        public static function update(){
            //code here...
        }
        public static function destroy(){
            //code here...
        }
        public static function handler(Request $request){
            $index = 0;
            for ($i=0; $i < count(Data::unload("path")); $i++) { 
                if($request->id == Data::unload("path")[$i][0]){
                    $index = $i++;
                }
            }
            for ($i=count(Data::unload("path"))-1; $i > $index; $i--) {
                $temp =  Data::unload("path");
                array_pop($temp);
                Data::load("path",$temp);
            }
            Route::index("DirectoryController");
        }
    }
?>