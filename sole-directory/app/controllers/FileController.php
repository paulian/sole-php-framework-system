<?php
    include('../../vendor/invoker/invoke.php');
    class FileController{
        public static function index(){
            $file = new DirFile;
            $filedata = DB::where($file,"fid","=",array_reverse(Data::unload("path"))[0][0]);
            foreach($filedata as $data){
                File::delete($data["file"],$data["location"]);
                DB::delete($file,$data["id"]);
            }
            Data::load("message",["All files inside this folder has been deleted.","success"]);
            Route::index("DirectoryController"); 
        }
        public static function store(){
            for ($i=0; $i < count($_FILES["filelist"]["name"]); $i++) { 
                $res = File::upload_file_multiple($_FILES,"filelist",$i,array_reverse(Data::unload("path"))[0][1]);
                $file = new DirFile;
                $file->fid = array_reverse(Data::unload("path"))[0][0];
                $file->name = $res->name;
                $file->file = $res->file;
                $file->location = $res->location;
                $file->type = $res->type;
                $file->size = $res->size;
                DB::save($file);
            }
            Data::load("message",["File/s uploaded.","success"]);
            Route::index("DirectoryController"); 
        }
        public static function show(){
            $file = new DirFile;
            $filedata = DB::find($file,Data::unload("file-id"));
            File::download_file($filedata[0]["file"],$filedata[0]["name"],$filedata[0]["location"]);
        }
        public static function update(Request $request){
            $file = new DirFile;
            $ext = DB::find($file,$request->id);
            $ext = array_reverse(explode(".",$ext[0]["name"]))[0];
            $filetemp = DB::prepare($file,$request->id);
            $filetemp->name = $request->name.".".$ext;
            DB::update($filetemp);
            Data::load("message",["File has been renamed to ".$request->name.".".$ext,"success"]);
            Route::index("DirectoryController"); 
        }
        public static function destroy(){
            $file = new DirFile;
            $filedata = DB::find($file,Data::unload("file-id"));
            File::delete($filedata[0]["file"],$filedata[0]["location"]);
            DB::delete($file,Data::unload("file-id"));
            Data::load("message",[$filedata[0]["name"]." has been deleted.","success"]);
            Route::index("DirectoryController"); 
        }
        public static function handler(Request $request){
            if($request->type == "download"){
                Data::load("file-id",$request->id);
                FileController::show();  
            }
            if($request->type == "delete"){
                Data::load("file-id",$request->id);
                FileController::destroy();
            }
            if($request->type == "deleteall"){
                Route::index("FileController"); 
            }
        }
    }
?>