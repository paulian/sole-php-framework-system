<?php
    include('../../vendor/invoker/invoke.php');
    class CopyOrMoveFilesController{
        public static function index(){
            //code here...
        }
        public static function store(){
            //code here...
        }
        public static function show(){
            //code here...
        }
        public static function update(){
            $res = json_decode(file_get_contents("../../resources/temp/json/cm.json"));
            if($res->action == "move"){
                $folder = new DirFolder;
                if($res->path == 0 || $res->path == null || $res->path == ""){
                    $folderid = 0;
                    $foldername = "Root";
                }else{
                    $folderid = $res->path;
                    $foldername = DB::find($folder,$res->path)[0]["name"];
                }

                $file = new DirFile;
                for ($i=0; $i < count($res->files); $i++) {
                    $filedata = DB::find($file,$res->files[$i]);
                    $filepath = $filedata[0]["location"].$filedata[0]["file"];
                    $temp = explode('/',$filedata[0]["location"]);
                    array_pop($temp);
                    array_pop($temp);
                    $tempfinal = "";
                    for ($j=0; $j < count($temp); $j++) { 
                        $tempfinal .= $temp[$j]."/";
                    }
                    $destpath = $tempfinal.$foldername."/".$filedata[0]["file"];

                    if(rename($filepath,$destpath)){
                        $filetemp = DB::prepare($file,$res->files[$i]);
                        $filetemp->fid = $folderid;
                        $filetemp->location = $tempfinal.$foldername."/";
                        DB::update($filetemp);
                        Data::load("message",["Files has been moved.","success"]); 
                    }else{
                        Data::load("message",["Files cannot be move.","warning"]); 
                    }
                }    
            }
            if($res->action == "copy"){
                $folder = new DirFolder;
                if($res->path == 0 || $res->path == null || $res->path == ""){
                    $folderid = 0;
                    $foldername = "Root";
                }else{
                    $folderid = $res->path;
                    $foldername = DB::find($folder,$res->path)[0]["name"];
                }

                $file = new DirFile;
                for ($i=0; $i < count($res->files); $i++) {
                    $filedata = DB::find($file,$res->files[$i]);
                    $filepath = $filedata[0]["location"].$filedata[0]["file"];

                    $ext = explode('.',$filedata[0]["file"]);

                    $temp = explode('/',$filedata[0]["location"]);
                    array_pop($temp);
                    array_pop($temp);
                    $tempfinal = "";
                    for ($j=0; $j < count($temp); $j++) { 
                        $tempfinal .= $temp[$j]."/";
                    }

                    $filename = uniqid('', true).".".$ext[2];
                    $destpath = $tempfinal.$foldername."/".$filename;

                    if(copy($filepath,$destpath)){
                        $file = new DirFile;
                        $file->fid = $folderid;
                        $file->name = $filedata[0]["name"];
                        $file->file = $filename;
                        $file->location = $tempfinal.$foldername."/";
                        $file->type = $filedata[0]["type"];
                        $file->size = $filedata[0]["size"];
                        DB::save($file);
                        Data::load("message",["Files copied.","success"]); 
                    }else{
                        Data::load("message",["Files cannot be copy.","warning"]); 
                    }
                }
            }
            $res->path = "";
            $res->files = [];
            $res->action = "";
            file_put_contents("../../resources/temp/json/cm.json",json_encode($res));
            Route::index("DirectoryController");
        }
        public static function destroy(){
            $res = json_decode(file_get_contents("../../resources/temp/json/cm.json"));
            if(count($res->files)){
                $file = new DirFile;
                for ($i=0; $i < count($res->files); $i++) { 
                    $filedata = DB::find($file,$res->files[$i]);
                    File::delete($filedata[0]["file"],$filedata[0]["location"]);
                    DB::delete($file,$filedata[0]["id"]);
                }
                Data::load("message",["Files has been deleted.","success"]);
            }
            else{
                Data::load("message",["You haven't selected any file.","warning"]); 
            }
            $res->path = "";
            $res->files = [];
            $res->action = "";
            file_put_contents("../../resources/temp/json/cm.json",json_encode($res));
            Route::index("DirectoryController");
        }
        public static function handler(Request $request){
            //code here...
        }
    }
?>