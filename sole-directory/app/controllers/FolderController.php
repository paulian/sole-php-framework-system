<?php
    include('../../vendor/invoker/invoke.php');
    class FolderController{
        public static function index(){
            //code here...
        }
        public static function store(Request $request){
            
            if(is_dir("../../public/root/file/".$request->foldername)){
                Data::load("message",["Folder ".$request->foldername." already existed.","warning"]);
            }else{
                $folder = new DirFolder;
                $folder->fid = array_reverse(Data::unload("path"))[0][0];
                $folder->name = $request->foldername;
                $folder->color = "folder-gray";
                DB::save($folder);
                mkdir("../../public/root/file/".$request->foldername);   
                Data::load("message",["Folder ".$request->foldername." has been created.","success"]);
            }
            Route::index("DirectoryController"); 
        }
        public static function show(){
            //code here...
        }
        public static function update(Request $request){
            if(file_exists("../../public/root/file/".$request->namenew)){
                Data::load("message",["Foldername already existed.","warning"]); 
            }else{
                rename("../../public/root/file/".$request->nameold,"../../public/root/file/".$request->namenew);
                $folder = new DirFolder;
                $foldertemp = DB::prepare($folder,$request->id);
                $foldertemp->name = $request->namenew;
                DB::update($foldertemp);

                $file = new DirFile;
                $filedata = DB::where($file,"fid","=",$request->id);
                foreach($filedata as $data){
                    $filetemp = DB::prepare($file,$data["id"]);
                    $filetemp->location = "../../public/root/file/".$request->namenew."/";
                    DB::update($filetemp);
                }
                Data::load("message",["Folder has been renamed to ".$request->namenew.".","success"]);    
            }
            Route::index("DirectoryController");
        }
        public static function destroy(){
            $folder = new DirFolder;
            $folderdata = DB::where($folder,"fid","=",Data::unload("folder-id"));
            if(count($folderdata) > 0){
                Data::load("message",["Folder is not empty.","warning"]); 
            }else{
                $file = new DirFile;
                $filedata = DB::where($file,"fid","=",Data::unload("folder-id"));
                if(count($filedata) > 0){
                    Data::load("message",["Folder is not empty.","warning"]);
                }else{
                    $folder = new DirFolder;
                    $foldertemp = DB::find($folder,Data::unload("folder-id"));
                    if(file_exists("../../public/root/file/".$foldertemp[0]["name"])){
                        rmdir("../../public/root/file/".$foldertemp[0]["name"]);
                    }
                    DB::delete($folder,Data::unload("folder-id"));
                    Data::load("message",["Folder has been deleted.","success"]);
                }
            }
            Route::index("DirectoryController");
        }
        public static function handler(Request $request){
            if($request->type == "select"){
                $temp = Data::unload("path");
                array_push($temp,[$request->id,$request->name]);
                Data::load("path",$temp);
                Route::index("DirectoryController");   
            }
            if($request->type == "delete"){
                Data::load("folder-id",$request->id);
                FolderController::destroy(); 
            }
        }
    }
?>