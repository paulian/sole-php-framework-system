<?php
    include('../../vendor/invoker/invoke.php');
    class RootController{
        public static function index(){
            $path = [];
            array_push($path,[0,"Root"]);
            Data::load("path",$path);
            Data::load("message",["",""]);
            Route::index("DirectoryController");
        }
        public static function store(){
            //code here...
        }
        public static function show(){
            //code here...
        }
        public static function update(){
            //code here...
        }
        public static function destroy(){
            //code here...
        }
        public static function handler(Request $request){
            //code here...
        }
    }
?>