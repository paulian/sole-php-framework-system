<?php
    $models = ["DirFile","DirFolder"];

    class DirFile
    {
        public $table = "dirfile";
        public $fillable = [
            "fid",
            "name",
            "file",
            "location",
            "type",
            "size"
        ];
    }

    class DirFolder
    {
        public $table = "dirfolder";
        public $fillable = [
            "fid",
            "name",
            "color"
        ];
    }
?>