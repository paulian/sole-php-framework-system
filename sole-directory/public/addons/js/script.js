function displayname(input,_this) {
    if(input.files["length"] > 1){
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    if(input.files[0]['size'] > 40000000){
                        _this.siblings('label').html("File size too large.")
                        toastr.warning("File size too large, max 40mb.");
                    }
                    else{
                        _this.siblings('label').html(input.files["length"] + " files")
                    }
                }
                reader.readAsDataURL(input.files[0]);
            }
    }else{
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                if(input.files[0]['size'] > 40000000){
                    _this.siblings('label').html("File size too large.")
                    toastr.warning("File size too large, max 40mb.");
                }
                else{
                    _this.siblings('label').html(input.files[0]['name'])
                }
            }
            reader.readAsDataURL(input.files[0]);
        }    
    }
    
}

var files = [];
var actiontype = "";
var currentdir = [];
var path = "";

fetch("DirectotySelectedFilesController")
.then(res => res.json())
.then(res => currentdir = res)

function selectFiles(){
    var selectcolumn = document.getElementById("selectcolumn");
    selectcolumn.style = "display: ";

    var rowid = document.getElementById("rowid");
    for (let index = 0; index < rowid.value; index++) {
        var selectcolumndata = document.getElementById(index);
        selectcolumndata.style = "display: ";    
    }

    var btnselect = document.getElementById("btnselect");
    btnselect.style = "display: none";
    var btndeleteall = document.getElementById("btndeleteall");
    btndeleteall.style = "display: none";

    var btnselectcancel = document.getElementById("btnselectcancel");
    btnselectcancel.style = "display: ";
    var btncopy = document.getElementById("btncopy");
    btncopy.style = "display: ";
    var btnmove = document.getElementById("btnmove");
    btnmove.style = "display: ";
    var btndelete = document.getElementById("btndelete");
    btndelete.style = "display: ";
}
function cancelSelectFiles(){
    var selectcolumn = document.getElementById("selectcolumn");
    selectcolumn.style = "display: none";

    var rowid = document.getElementById("rowid");
    for (let index = 0; index < rowid.value; index++) {
        var selectcolumndata = document.getElementById(index);
        selectcolumndata.style = "display: none";    
    }

    var btnselect = document.getElementById("btnselect");
    btnselect.style = "display: ";
    var btndeleteall = document.getElementById("btndeleteall");
    btndeleteall.style = "display: ";

    var btnselectcancel = document.getElementById("btnselectcancel");
    btnselectcancel.style = "display: none";
    var btncopy = document.getElementById("btncopy");
    btncopy.style = "display: none";
    var btnmove = document.getElementById("btnmove");
    btnmove.style = "display: none";
    var btndelete = document.getElementById("btndelete");
    btndelete.style = "display: none";

    var checkboxmaster = document.getElementById("checkbox");
    checkboxmaster.setAttribute("checked",null);
    selectAll();
}
function selectedFiles(id){
    if(files.indexOf(id) >= 0 ){
        files.splice(files.indexOf(id),1);
    }else{
        files.push(id)
    }
    fetch("DirectotySelectedFilesController?update", {
        method: 'POST',
        body: new URLSearchParams('&files=' + files)
    })
    .then(res => res.json())
    .then(res => console.log(res))
}
function selectAll(){
    var checkboxmaster = document.getElementById("checkbox");
    if(checkboxmaster.hasAttribute("checked")){
        checkboxmaster.removeAttribute("checked");

        var rowid = document.getElementById("rowid");
        for (let index = 0; index < rowid.value; index++) {
            var checkbox = document.getElementById("checkbox" + index);
            if(checkbox.hasAttribute("checked")){
                checkbox.removeAttribute("checked")      
            }     
        }
        window.location.href = "";
    }else{
        files = [];
        checkboxmaster.setAttribute("checked",null);
        var rowid = document.getElementById("rowid");
        for (let index = 0; index < rowid.value; index++) {
            var checkbox = document.getElementById("checkbox" + index);
            if(!checkbox.hasAttribute("checked")){
                checkbox.setAttribute("checked",null)      
            }
            if(!files.indexOf(checkbox.value) >= 0 ){
                files.push(checkbox.value)
            }     
        }
    }
    fetch("DirectotySelectedFilesController?update", {
        method: 'POST',
        body: new URLSearchParams('&files=' + files)
    })
    .then(res => res.json())
    .then(res => console.log(res))
}
function clickCopy(){
    actiontype = "copy";
    var copyormovehead = document.getElementById("copyormovehead");
    copyormovehead.innerText = "Copy Files To: ";
    if(files.length){
        listDerictory()    
    }
}
function clickMove(){
    actiontype = "move";
    var copyormovehead = document.getElementById("copyormovehead");
    copyormovehead.innerText = "Move Files To: ";
    if(files.length){
        listDerictory()    
    }
}
function clickDelete(){
    actiontype = "delete";
    console.log(actiontype)
}

function listDerictory(){
    if(actiontype == "copy"){
        var copyormovebtn= document.getElementById("copyormovebtn");
        copyormovebtn.innerHTML = '<span class="fa fa-copy"></span> Copy Here';
    }
    if (actiontype == "move"){
        var copyormovebtn= document.getElementById("copyormovebtn");
        copyormovebtn.innerHTML = '<span class="fa fa-arrow-right"></span> Move Here';
    }
    //display path
    var pathlist = document.getElementById("pathlist");
    pathlist.innerHTML = "";

    pathlist.insertAdjacentHTML('beforeend','<button class="btn btn-sm btn-success"><span id="pathicon" class="fa fa-laptop"></span></button>');
    pathlist.insertAdjacentHTML('beforeend','<button class="btn btn-sm btn-dark">:</button>');
    pathlist.insertAdjacentHTML('beforeend','<button onclick="pathUpdate(0)" class="btn btn-sm btn-success">Root</button>');
    pathlist.insertAdjacentHTML('beforeend','<button class="btn btn-sm btn-dark">/</button>');

    path = currentdir[currentdir.length-1][0]

    if(path == 0 || path == null){
        var pathicon = document.getElementById("pathicon");
        pathicon.classList.remove("fa-folder")    
        pathicon.classList.add("fa-laptop")    
    }else{
        var pathicon = document.getElementById("pathicon");
        pathicon.classList.remove("fa-laptop")    
        pathicon.classList.add("fa-folder") 
    }
    
    console.log(path)

    for (let index = 1; index < currentdir.length; index++) {
        pathlist.insertAdjacentHTML('beforeend','<button style="overflow-x: hidden; overflow-y: hidden; height: 31.5px;" onclick="pathUpdate('+ index +')" class="btn btn-sm btn-success">'+ currentdir[index][1] +'</button>');
        pathlist.insertAdjacentHTML('beforeend','<button class="btn btn-sm btn-dark">/</button>');
    }

    //id use to fetch directory list
    dirid = currentdir[currentdir.length - 1][0];

    //fetch directory list inside this currect directory
    fetch("DirectotySelectedFilesController?handler&id="+dirid)
    .then(res => res.json())
    .then(res =>dirList(res))
}
function dirList(data){
    var dirlist = document.getElementById("dirlist");
    dirlist.innerHTML = "";
    for (let index = 0; index < data.length; index++) {
        dirlist.insertAdjacentHTML('beforeend','<div class="col-md-3 mb-2"><button style="height: 31.5px; overflow-y: hidden;" onclick="addPath('+ data[index]["id"] +',\''+ data[index]["name"] +'\')" class="btn btn-sm btn-primary col"><span class="fa fa-folder"></span> '+ data[index]["name"] +'</button></div>');
    }
    fetch("DirectotySelectedFilesController?store", {
        method: 'POST',
        body: new URLSearchParams('path=' + path + '&files=' + files + '&action=' + actiontype)
    })
    .then(res => res.json())
    .then(res => console.log(res))
}
function pathUpdate(id){
    for (let index = currentdir.length - 1; index > id; index--) {
        currentdir.pop()
    }
    listDerictory()
}
function addPath(id,name){
    currentdir.push([id,name])
    listDerictory()
}
function about(){
    speakIt = new SpeechSynthesisUtterance();
    speakIt.volume= 1;
    speakIt.rate = 1.3;
    speakIt.pitch = 1;

    speakIt.text = 
    "Description, The main purpose of this system was to create directory, which users can send files throughout the network. So that the users don't need anymore to use external drive to transfer files between computers. There is also no need to use applications like ShareIt, and Bluetooth to send files. Advantages, Everyone who are connected in the same network can access the file, Fast and easy to use, Doesn't delete folder easily, even though the user click the delete button by mistake, unless the folder is empty, Some of the file can be preview. Disadvantages, Anyone can access the files (we assume that you trust everyone who are connected in your network). Share Directory Link @ Server's IP Address/Projects/Library Project/sole-directory/";
    window.speechSynthesis.speak(speakIt);
}