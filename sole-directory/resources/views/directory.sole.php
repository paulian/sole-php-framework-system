<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
    <body>
        <div class="modal-header">
            <h5>Sole Directory</h5>
            <button onclick="about()" data-toggle="modal" data-target="#about" class="btn btn-sm btn-success"><span class="fa fa-info"></span> About</button>
        </div>
        <div class="modal fade" id="about">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6><b>Sole Directory</b></h6>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <h6><b>Description</b></h6>
                            <hr>
                            <h6 class="pl-3">The main purpose of this system was to create directory, which users can send files throughout the network (specifically LAN). So that the users don't need anymore to use external drive to transfer files between computers. There is also no need to use applications like <i>ShareIt and Bluetooth</i> to send files.</h6>
                            <hr>
                            <h6><b>Advantages</b></h6>
                            <hr>
                            <h6 class="pl-3">Everyone who are connected in the same network can access the file.</h6>
                            <h6 class="pl-3">Fast and easy to use.</h6>
                            <h6 class="pl-3">Doesn't delete folder easily, even though the user click the delete button by mistake, unless the folder is empty.</h6>
                            <h6 class="pl-3">Some of the file can be preview.</h6>
                            <hr>
                            <h6><b>Disadvantages</b></h6>
                            <hr>
                            <h6 class="pl-3">Anyone can access the files (we assume that you trust everyone who are connected in your network).</h6>
                            <hr>
                            <h6><b>Share Directory Link</b></h6>
                            <hr>
                            <?php
                                $linktemp = explode("/",$_SERVER["SCRIPT_NAME"]);
                                $link = "";
                                for ($i=0; $i < count($linktemp)-3; $i++) { 
                                    $link .= $linktemp[$i]."/";
                                }
                            ?>
                            <h6 class="pl-3"><a href="#">Server's IP Address<?php echo($link); ?></a></h6>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-sm btn-primary col">I Understand</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-body">
            <?php if(Data::unload("message")[0]){ ?>
            <div class="alert alert-<?php echo(Data::unload("message")[1]); ?>">
                <h5><?php echo(Data::unload("message")[0]); ?></h5>
            </div>
            <?php } Data::load("message",["",""]); ?>
            <div class="modal-header">
                <div class="col-md-10" style="display: flexbox;">
                    <div class="btn-group">
                        <button class="btn btn-sm btn-primary"><span class="fa fa-<?php if(array_reverse(Data::unload("path"))[0][0] == 0){echo("laptop");}else{echo("folder");}?>"></span></button>
                        <button class="btn btn-sm btn-dark">:</button>
                        <a href="PathController?handler&id=0" class="btn btn-sm btn-primary">Root</a>
                        <button class="btn btn-sm btn-dark">/</button>
                        <?php 
                            for ($i=0; $i < count(Data::unload("path")); $i++) { 
                                if(Data::unload("path")[$i][1] != "Root"){
                                    echo('<a style="overflow-x: hidden; overflow-y: hidden; height: 31.5px;" href="PathController?handler&id='.Data::unload("path")[$i][0].'" class="btn btn-sm btn-primary">'.Data::unload("path")[$i][1].'</a>');
                                    echo('<button class="btn btn-sm btn-dark">/</button>');
                                }
                            }
                        ?>    
                    </div>
                </div>
                <div class="btn-group col-md-2">
                    <button data-toggle="modal" data-target="#addfolder" class="btn btn-sm btn-dark"><span class="fa fa-plus"></span> Folder</button>
                    <button data-toggle="modal" data-target="#uploadfile" class="btn btn-sm btn-dark"><span class="fa fa-upload"></span> File</button>    
                </div>
            </div>
            <div class="modal-body">
                <div class="modal-header">
                    <h6>Folders</h6>
                </div>
                <div class="mb-3 p-3">
                    <div class="row">
                        <?php foreach($dd["folderdata"] as $folder){ ?>
                            <div class="col-md-4">
                                <div class="modal-header mb-3 <?php echo($folder["color"]); ?>">
                                    <h6 class="pr-1 folder-link"><span class="fa fa-folder"></span> <a href="FolderController?handler&id=<?php echo($folder["id"]); ?>&name=<?php echo($folder["name"]); ?>&type=select"><?php echo($folder["name"]); ?></a></h6>
                                    <div class="btn-group">
                                        <button data-toggle="modal" data-target="#foldercolor<?php echo($folder["id"]); ?>" class="btn btn-sm btn-dark"><span class="fa fa-folder"></span></button>
                                        <button data-toggle="modal" data-target="#renamefolder<?php echo($folder["id"]); ?>" class="btn btn-sm btn-dark"><span class="fa fa-edit"></span></button>
                                        <a href="FolderController?handler&id=<?php echo($folder["id"]); ?>&type=delete" class="btn btn-sm btn-danger"><span class="fa fa-trash"></span></a>
                                    </div>      
                                </div>
                            </div>
                            <div class="modal" id="foldercolor<?php echo($folder["id"]); ?>">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h6>Change Folder Color</h6>
                                        </div>
                                        <form action="FolderColorController?update" method="post" class="btn-group col">
                                            <div class="modal-body">
                                                <select required name="color" class="form-control">
                                                    <option value="folder-gray">Gray <i>(default)</i></option>
                                                    <option value="folder-blue">Blue</option>
                                                    <option value="folder-green">Green</option>
                                                    <option value="folder-red">Red</option>
                                                    <option value="folder-yellow">Yellow</option>
                                                    <option value="folder-orange">Orange</option>
                                                    <option value="folder-violet">Violet</option>
                                                </select>
                                                <input required class="form-control" type="hidden" name="id" value="<?php echo($folder["id"]); ?>">
                                            </div>
                                            <div class="modal-footer">
                                                <button class="btn btn-dark"><span class="fa fa-save"></span> Save</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                            <div class="modal" id="renamefolder<?php echo($folder["id"]); ?>">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h6>Rename Folder</h6>
                                        </div>
                                        <div class="modal-body">
                                            <form action="FolderController?update" method="post">
                                                <div class="btn-group col">
                                                    <input type="hidden" name="id" value="<?php echo($folder["id"]); ?>">
                                                    <input type="hidden" name="nameold" value="<?php echo($folder["name"]); ?>">
                                                    <input required type="text" class="form-control" name="namenew" value="<?php echo($folder["name"]); ?>" placeholder="Folder Name">
                                                    <button class="btn btn-dark"><span class="fa fa-edit"></span></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>    
                </div>
                <div class="modal-header">
                    <h6>Files</h6>
                    <div class="btn-group">
                        <a href="FileController?handler&type=deleteall" id="btndeleteall" style="display: ;" class="btn btn-sm btn-danger"><span class="fa fa-warning"></span> Empty Folder</a>
                        <button href="#" id="btnselect" style="display: ;" onclick="selectFiles()" class="btn btn-sm btn-dark"><span class="fa fa-check-square"></span> Select Files</button>
                        <button href="#" id="btnselectcancel" style="display: none;" onclick="cancelSelectFiles()" class="btn btn-sm btn-dark"><span class="fa fa-remove"></span></button>
                        <button href="#" id="btncopy" style="display: none;" onclick="clickCopy()" data-toggle="modal" data-target="#copyormove" class="btn btn-sm btn-success"><span class="fa fa-copy"></span></button>
                        <button href="#" id="btnmove" style="display: none;" onclick="clickMove()" data-toggle="modal" data-target="#copyormove" class="btn btn-sm btn-success"><span class="fa fa-arrow-right"></span></button>
                        <a href="CopyOrMoveFilesController?destroy" id="btndelete" style="display: none;" onclick="clickDelete()" class="btn btn-sm btn-danger"><span class="fa fa-trash-o"></span></a>
                    </div>
                    
                </div>
                
                <div class="mb-3">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <td>Name</td>
                                <td>Type</td>
                                <td style="width: 150px; text-align: right;">Size</td>
                                <td>Date Modified</td>
                                <td>Action</td>
                                <td id="selectcolumn" style="display: none;"; class="alert alert-dark">
                                    <input id="checkbox" onclick="selectAll()" class="form-control mt-1" style="height: 18px; width: 18px;  margin: auto;" type="checkbox" name="" id="">
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $rowid = 0;?>
                            <?php foreach($dd["filedata"] as $file){ ?>
                            <tr>
                                <td><span class="fa <?php echo($dd["icon"][$file["type"]]); ?>"></span> <?php echo($file["name"]); ?></td>
                                <td style="width: 300px;"><?php echo($dd["type"][$file["type"]]); ?></td>
                                <td style="width: 150px; text-align: right;">
                                    <?php
                                        $temp = array_reverse(str_split(strrev($file["size"]),3));
                                        $size = "";
                                        for ($i=0; $i < count($temp)-1; $i++) {
                                            if($i == count($temp)-2){
                                                $size .= strrev($temp[$i]);
                                            }else{
                                                $size .= strrev($temp[$i]).",";
                                            }
                                        }
                                        if($size){
                                            echo($size." KB"); 
                                        }else{
                                            echo("0 KB");
                                        }
                                        
                                    ?>
                                </td>
                                <td style="width: 200px;"><?php echo(date("d/m/y h:i A",strtotime($file["updated_at"]))); ?></td>
                                <td style="width: 100px;">
                                    <div class="btn-group">
                                        <a 
                                            <?php
                                                if(in_array($file["type"],$dd["preview"])){
                                                    echo('href="'.$file["location"].$file["file"].'"');
                                                    echo('target="_blank"');
                                                    echo('class="btn btn-sm btn-success"');
                                                }else{
                                                    echo('data-toggle="modal"');
                                                    echo('data-target="#previewmessage"');
                                                    echo('href="#"');
                                                    echo('class="btn btn-sm btn-dark"');
                                                }
                                            ?>><span class="fa fa-book"></span></a>
                                        <a href="FileController?handler&id=<?php echo($file["id"]); ?>&type=download" class="btn btn-sm btn-dark"><span class="fa fa-download"></span></a>
                                        <button data-toggle="modal" data-target="#renamefile<?php echo($file["id"]); ?>" class="btn btn-sm btn-dark"><span class="fa fa-edit"></span></button>
                                        <a href="FileController?handler&id=<?php echo($file["id"]); ?>&type=delete" class="btn btn-sm btn-danger"><span class="fa fa-trash"></span></a>
                                    </div>
                                </td>
                                <td id="<?php echo($rowid);?>" style="display: none;"; class="alert alert-dark">
                                    <input id="checkbox<?php echo($rowid);?>" class="form-control mt-1" style="height: 18px; width: 18px; margin: auto;" type="checkbox" name="" onclick="selectedFiles('<?php echo($file['id']); ?>')" value="<?php echo($file['id']); ?>">
                                </td>
                            </tr>
                            <div class="modal" id="renamefile<?php echo($file["id"]); ?>">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h6>Rename File</h6>
                                        </div>
                                        <div class="modal-body">
                                            <form action="FileController?update" method="post">
                                                <div class="btn-group col">
                                                    <input type="hidden" name="id" value="<?php echo($file["id"]); ?>">
                                                    <input required type="text" class="form-control" name="name" value="<?php 
                                                        $s = explode('.',$file["name"]);
                                                        $name = "";
                                                        for ($i=0; $i < count($s)-1; $i++) { 
                                                            if($i == count($s)-2){
                                                                $name .= $s[$i];
                                                            }else{
                                                                $name .= $s[$i].".";
                                                            }
                                                        }
                                                        echo($name);
                                                    ?>" placeholder="File Name">
                                                    <button class="btn btn-dark"><span class="fa fa-edit"></span></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php $rowid++; ?>
                            <?php } ?>
                            <input id="rowid" type="hidden" value="<?php echo($rowid);?>">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal" id="previewmessage">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="mt-1">Preview Unavailable</h6>
                        <button data-dismiss="modal" class="btn btn-sm btn-primary">OK</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="addfolder">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6>Create New Folder</h6>
                    </div>
                    <div class="modal-body">
                        <form action="FolderController?store" method="post" class="btn-group col">
                            <input required class="form-control" type="text" name="foldername" placeholder="Folder Name">
                            <button class="btn btn-dark"><span class="fa fa-plus"></span></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="uploadfile">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6>Upload File</h6>
                    </div>
                    <div class="modal-body">
                        <form action="FileController?store" method="post"  enctype="multipart/form-data" class="btn-group col">
                            <div class="input-group mb-3">
                                <div class="custom-file">
                                    <input required type="file" name="filelist[]" id="upload" multiple>
                                </div>
                                <div class="input-group-prepend">
                                    <button class="input-group-text btn-dark"><span class="fa fa-upload"></span></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" id="copyormove">
            <div class="modal-dialog">
                <div class="modal-content" style="width: 700px; margin-left: -95px;">
                    <div class="modal-header">
                        <h6 id="copyormovehead"></h6>
                    </div>
                    <div class="modal-body" style="overflow-x: scroll;">
                        <div class="btn-group" id="pathlist">
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="row col" style="margin: 0px; padding: 0px;" id="dirlist">
                            <div class="alert alert-warning col"><h6>Please select files first.</h6></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <form action="CopyOrMoveFilesController?update" method="post">
                            <div class="btn-group">
                                <a class="btn btn-sm btn-dark" href="#" data-dismiss="modal"><span class="fa fa-remove"></span> Cancel</a>
                                <button id="copyormovebtn" class="btn btn-sm btn-success" type="submit">Unavailable</button>      
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>