<?php
    /**Note: do not interchange code arrangement */
    include_once('../../vendor/database/dc.php');
    include_once('../../vendor/database/db.php');
    include_once('../models/Models.php');
    include_once('../../vendor/data/data.php');
    include_once('../../vendor/data/log.php');
    include_once('../../vendor/file handler/file.php');
    include_once('../../public/load.php');
    include_once('../../vendor/assets loader/auto.php');
    include_once('../../vendor/route/route.php');
    
    $res = file_get_contents("../../.env");
    $conf = explode("
",$res);
    for ($i=0; $i < count($conf); $i++) { 
        if(explode('=',$conf[$i])[0]=="Extension"){
            if(file_exists('../../vendor/libs/'.explode('=',$conf[$i])[1].'/loader.php')){
                include_once('../../vendor/libs/'.explode('=',$conf[$i])[1].'/loader.php');
            }else{
                echo "<b>Library Inclusion Failed: </b>library <b>".explode('=',$conf[$i])[1]."</b> or <b>loader.php </b> file can't be found in ./vendor/libs/<br>";
            }
        }
    }

    class Extend_Route{
        public static function er(){
            $func = array("store","show","update","destroy","handler");
            $Controller = $_SERVER["PHP_SELF"];
            $arr = explode('/',$Controller);
            $arr = explode('.',end($arr));
            $Controller = $arr[0];

            if(count($_GET)){
                $request_data = new Request;
                $request_keys = array_keys($_GET);

                for ($i=0; $i < count($request_keys); $i++) { 
                    $temp = $request_keys[$i];
                    $request_data->$temp = $_GET[$temp];
                }

                $request_keys = array_keys($_POST);
                
                for ($i=0; $i < count($request_keys); $i++) { 
                    $temp = $request_keys[$i];
                    $request_data->$temp = $_POST[$temp];
                }

                if(in_array(array_keys($_GET)[0],$func)){
                    $method = array_keys($_GET)[0];
                    $Controller::$method($request_data);
                }else{
                    $method = "unknown_method->".array_keys($_GET)[0];
                    $Controller::$method($request_data);
                }
            }else{
                $Controller::index();
            }
        }
    }
    class Request{
        public static $request = "";
    }
    Extend_Route::er();
?>