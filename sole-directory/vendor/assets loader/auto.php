<?php
    class Auto{
        public static $file = "../../vendor/assets loader/link.php";
        public static function index(){
            $document = file_get_contents(Auto::$file);
            $document = "";
            for ($i=0; $i < count(Load::$CSS); $i++) { 
                $document .= '<link rel="stylesheet" href="../../public/'.Load::$CSS[$i].'">'.'
';
            }
            $document .= '<link rel="stylesheet" href="../../public/assets/css/sole.css">'.'
';
            $document .= '<link rel="stylesheet" href="../../public/assets/css/sole-bootstrap.css">'.'
';
            for ($i=0; $i < count(Load::$JS); $i++) { 
                $document .= '<script src="../../public/'.Load::$JS[$i].'"></script>'.'
';
            }
            $document .= '<script src="../../public/assets/js/sole.js"></script>'.'
';
            file_put_contents(Auto::$file,$document); 
            include(Auto::$file);
        }
    }
    Auto::index();
?>
<?php if($APP_TITLE){?>
    <title><?php echo $APP_NAME;?></title>
<?php }?>
<?php if($APP_ICON){?>
    <link rel="shortcut icon" href="../../public/assets/icons/favicon.ico" type="image/x-icon">
<?php }?>
<?php if($APP_WATER_MARK){?>
<div class="wm">
    <h6><span>SOLO</span>Leveler_v<?php echo Data::unload("APP_FRAMEWORK_VERSION"); ?> </h6>
</div>
<style>
    .wm{
        position: fixed;
        bottom: 0;
        right: 0;
        padding: 8px;
        padding-bottom: 2px;
        border: solid 1px white;
        border-radius: 5px;
        z-index: 1050;
        overflow: hidden;
        outline: 0;
        background-color: #07052bf3;
    }
    .wm h6{
        color: white;
    }
    .wm span{
        color: red;
        font-weight: bolder;
    }
</style>
<?php }?>

