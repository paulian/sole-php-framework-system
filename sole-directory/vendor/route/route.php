
<?php
    class Route{
        public static function load($name){
            header('location: app/controllers/'.$name);
        }
        public static function view($name, $dd = null){
            $arr = explode('.',$name);
            $location = "";
            for ($i=0; $i <= count($arr)-1; $i++) { 
                $location .= '/'.$arr[$i];
            }
            if(file_exists("../../resources/views".$location.".sole.php") === true){
                require_once("../../resources/views".$location.".sole.php");
                Auto::index();
            }else{
                echo "<b>View Error: </b>The file ".end($arr).".sole.php doesn't exist, make sure to name the file according to filename format 'filename.sole.php'.";
                Auto::index();
            }
        }
        public static function index($name){
            header('location: ../controllers/'.$name);
        }
        public static function return($name){
            $_SESSION['Route_Location'] = $name;
            header('location: ../controllers/'.$name);
        }
    }
?>