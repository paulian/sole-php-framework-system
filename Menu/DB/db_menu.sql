-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 18, 2021 at 05:13 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_menu`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `folder` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `language` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `name`, `folder`, `language`, `description`, `thumbnail`, `created_at`, `updated_at`) VALUES
(1, 'Analogue Clock', 'Analogue Clock', 'JavaScript', 'Analogue Clock', '6145f61f22a756.89086197.png', '2021-09-18 14:22:23', '2021-09-18 14:24:05'),
(2, 'Calculator', 'calculator', 'JavaScript', 'Calculator', '6145f6c0cd63e1.60616326.png', '2021-09-18 14:25:04', '2021-09-18 14:25:04'),
(3, 'Crush and Love Meter', 'crush_and_love_meter', 'PHP', 'Crush and Love Meter', '6145f8c6dd97b3.08695518.png', '2021-09-18 14:33:42', '2021-09-18 14:39:31'),
(4, 'Crush and Love Meter', 'clm.v1.1', 'PHP', 'Crush and Love Meter', '6145f90a1d9661.23905126.png', '2021-09-18 14:34:50', '2021-09-18 14:39:17'),
(5, 'Event Attendance Management System', 'Event Attendance Management System', 'PHP', 'Event Attendance Management System', '6145f94a8786b1.79443795.png', '2021-09-18 14:35:54', '2021-09-18 14:37:27'),
(6, 'Event Management System', 'Event Management System', 'PHP', 'Event Management System', '6145f9844bbc99.28542814.png', '2021-09-18 14:36:52', '2021-09-18 14:37:08'),
(7, 'Event Facility Reservation System', 'event_facility_reservation_system', 'PHP', 'Event Facility Reservation System', '6145faab442156.86308139.png', '2021-09-18 14:41:47', '2021-09-18 14:41:47'),
(8, 'Module Directory and Monitoring System', 'File Management System', 'PHP', 'Module Directory and Monitoring System', '6145faf540f196.56455097.png', '2021-09-18 14:43:01', '2021-09-18 14:43:01'),
(9, 'Game', 'Game', 'JavaScript', 'Game', '6145fb28666bd6.02873975.png', '2021-09-18 14:43:52', '2021-09-18 14:43:52'),
(10, 'Insta-Chat', 'insta-chat', 'PHP', 'Instant chat box', '6145fba0a0cc36.26512529.png', '2021-09-18 14:45:52', '2021-09-18 14:45:52'),
(11, 'PHP CRUD (Sample PHP Program for Demostration)', 'PHP CRUD (Sample PHP Program for Demostration)', 'PHP', 'PHP CRUD (Sample PHP Program for Demostration)', '6145fc3dbb6bc1.24709155.png', '2021-09-18 14:48:29', '2021-09-18 14:48:29'),
(12, 'Sport Profiling', 'Sport Profiling', 'Sport Profiling', 'Sport Profiling', '6145fd3fa99f95.35175868.png', '2021-09-18 14:52:47', '2021-09-18 14:52:47'),
(13, 'Main Menu', 'Menu', 'PHP', 'Main Menu', '6145fdc316ed13.52933159.png', '2021-09-18 14:54:59', '2021-09-18 14:54:59'),
(14, 'Animal Shelter', 'Templates/Animal Shelter', 'N/A', 'Animal Shelter', '6145fe1f944d53.25833825.png', '2021-09-18 14:56:31', '2021-09-18 14:56:31'),
(15, 'Bahia', 'Templates/Bahia', 'N/A', 'Bahia', '6145fe44634120.58048943.png', '2021-09-18 14:57:08', '2021-09-18 14:57:08'),
(16, 'Black Forest', 'Templates/Black Forest', 'N/A', 'Black Forest', '6145fe6283c114.72047292.png', '2021-09-18 14:57:38', '2021-09-18 14:57:38'),
(17, 'Flux', 'Templates/Flux', 'N/A', 'Flux', '6145fe7d8d1ee5.85409823.png', '2021-09-18 14:58:05', '2021-09-18 14:58:05'),
(18, 'Lady Bugg', 'Templates/Lady Bugg', 'N/A', 'Lady Bugg', '6145fe99b1a750.40958323.png', '2021-09-18 14:58:33', '2021-09-18 14:58:33'),
(19, 'The Coast', 'Templates/The Coast', 'N/A', 'The Coast', '6145fec30156b8.13012867.png', '2021-09-18 14:59:15', '2021-09-18 15:05:51'),
(20, 'Visions', 'Templates/Visions', 'N/A', 'Visions', '6145fedede17a5.24285548.png', '2021-09-18 14:59:42', '2021-09-18 15:05:39'),
(21, 'Rocket', 'Templates/Rocket', 'N/A', 'Rocket', '6145fefceb55b9.37221352.png', '2021-09-18 15:00:12', '2021-09-18 15:00:12'),
(22, 'Scroll Portfolio', 'Templates/Scroll Portfolio', 'N/A', 'Scroll Portfolio', '6145ff16b62cd6.29935729.png', '2021-09-18 15:00:38', '2021-09-18 15:05:20');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`) VALUES
(1, '2021_18_09_251365_menu');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
