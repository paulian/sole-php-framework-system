<?php
    Migrate::$migration = ["MenuMigration"];

    class MenuMigration
    {
        public static function index(){
            Migrate::attrib_table("menu");
            Migrate::attrib_string(255);
            Migrate::string("name");
            Migrate::string("folder");
            Migrate::string("language");
            Migrate::string("description");
            Migrate::string("thumbnail");
        }
    }
?>