<?php
    $models = ["Menu"];

    class Menu
    {
        public $table = "menu";
        public $fillable = [
            "name",
            "folder",
            "language",
            "description",
            "thumbnail",
        ];
    }
?>