<?php
    include('../../Vendor/Invoker/invoke.php');
    class MenuController{
        public static function index(){
            $menu = new Menu;
            Data::load("menu",Data::reverse(DB::all($menu))); 
            Route::view("home");
        }
        public static function store(){
            $file = File::upload_img($_FILES,"thumbnail","thumbnail");
            if($file->type != "INVALID"){
                $menu = new Menu;
                $menu->name = $_POST["name"];   
                $menu->folder = $_POST["location"];    
                $menu->language = $_POST["language"];   
                $menu->description = $_POST["description"];   
                $menu->thumbnail = $file->file;
                DB::save($menu);   
            }
            Route::index("MenuController");
        }
        public static function show(){
            $menu = new Menu;
            Data::load("menu",Data::reverse(DB::where($menu,"name","like",$_POST["name"]))); 
            Route::view("home");
        }
        public static function update(){
            $menu = new Menu;
            $update = DB::prepare($menu, $_POST["id"]);
            $update->name = $_POST["name"];   
            $update->folder = $_POST["location"];    
            $update->language = $_POST["language"];   
            $update->description = $_POST["description"]; 
            DB::update($menu);
            Route::index("MenuController");
        }
        public static function destroy(){
            $menu = new Menu;
            DB::delete($menu, $_POST["id"]);
            Route::index("MenuController");
        }
    }
?>