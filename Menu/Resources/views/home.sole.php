<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
    <body onclick="clean()">
        <div class="modal-header sticky-top">
            <div class="title">
                <h5>Main Menu</h5>
                <h6>System Main Menu</h6>    
            </div>
                <div  class="btn-group mr-5 mt-3">
                        <form action="MenuController?show" method="post" style="padding: 0px; margin: 0px;">
                            <div class="btn-group">
                                <input oninput="search(this.value)" type="text" name="name" id="searchname" class="form-control" placeholder="Search Name">
                                <button class="btn btn-success"><span class="fa fa-search"></span></button>
                                <div id="searchSuggest">
                                    <div id="suggest">
                                    
                                    </div>
                                </div>  
                            </div>
                        </form>
                    <button class="btn btn-primary" data-toggle="modal" data-target="#add">Add</button>
                    <button class="btn btn-primary" data-toggle="modal" data-target="#add"><span class="fa fa-plus"></span></button>
                    <button type="button" class="btn btn-dark">Action</button>
                    <button type="button" class="btn btn-dark dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu">
                        <a class="dropdown-item btn-success" href="http://localhost/phpmyadmin/" target="_blank"><span class="fa fa-server"></span> phpMyAdmin</a>
                    <div class="dropdown-divider"></div>
                        <a class="dropdown-item btn-success" href="http://localhost/phpmyadmin/sql.php?db=db_menu&table=menu&pos=0" target="_blank"><span class="fa fa-database"></span> Database</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-body">
            <div class="row">
                <?php foreach(Data::unload("menu") as $menu){ ?>
                <div class="col-md-3 mb-3">
                    <div class="item-display">
                        <a href="../../Public/Root/Img/thumbnail/<?php echo $menu["thumbnail"];?>" target="_blank">
                            <img src="../../Public/Root/Img/thumbnail/<?php echo $menu["thumbnail"];?>" alt="" class="item-img">
                        </a>
                        
                        <div class="item-description">
                            <h6><b>System Name: </b> <i><?php echo $menu["name"];?></i></h6>
                            <h6><b>Language: </b> <i><?php echo $menu["language"];?></i></h6>
                            <h6 style="height: 40px; overflow-y: hidden;"><b>Description: </b> <i><?php echo $menu["description"];?></i></h6>
                        </div>
                        <div class="modal-footer">
                            <button data-toggle="modal" data-target="#delete<?php echo $menu["id"];?>" class="btn btn-sm btn-danger"><span class="fa fa-trash"></span></button>
                            <button data-toggle="modal" data-target="#info<?php echo $menu["id"];?>" class="btn btn-sm btn-primary"><span class="fa fa-info"></span></button>                            
                            <a href="../../../<?php echo $menu["folder"];?>" target="_blank" class="btn btn-sm btn-success"><span class="fa fa-play"></span></a>
                        </div>
                    </div>
                </div>
                <div class="modal" id="info<?php echo $menu["id"];?>">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h6 class="mt-2"><span class="fa fa-info"></span> System Info</h6>
                            </div>
                            <form action="MenuController?update" method="post">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <input type="hidden" value="<?php echo $menu["id"];?>" name="id" id="">
                                        <label>System Name</label>
                                        <input required type="text" name="name" id="" value="<?php echo $menu["name"];?>" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Language</label>
                                        <input required type="text" name="language" value="<?php echo $menu["language"];?>" id="" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea required name="description" class="form-control" rows="5"><?php echo $menu["description"];?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Location (Folder Name)</label>
                                        <input required type="text" name="location" value="<?php echo $menu["folder"];?>" id="" class="form-control">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-sm btn-primary"><span class="fa fa-save"></span> Save Changes</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="modal" id="delete<?php echo $menu["id"];?>">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h6 class="mt-2"><span class="fa fa-warning"></span> Delete Menu</h6>
                            </div>
                            <form action="MenuController?destroy" method="post">
                                <div class="modal-body">
                                    <div class="form-group">
                                        <input type="hidden" value="<?php echo $menu["id"];?>" name="id" id="">
                                        <h6>Do you really want to delete <?php echo $menu["name"];?>? This can't be undone.</h6>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button class="btn btn-sm btn-danger"><span class="fa fa-trash"></span> Delete</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>

        <div class="modal" id="add">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="mt-2"><span class="fa fa-plus"></span> Add Menu</h6>
                    </div>
                    <form action="MenuController?store" method="post" enctype="multipart/form-data">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>System Name</label>
                                <input required type="text" name="name" id="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Language</label>
                                <input required type="text" name="language" id="" class="form-control">
                            </div>
                            <div class="form-group">
                                <label>Description</label>
                                <textarea required name="description" class="form-control" rows="5"></textarea>
                            </div>
                            <div class="form-group">
                                <label>Location (Folder Name)</label>
                                <input required type="text" name="location" id="" class="form-control">
                            </div>
                            <hr>
                            <div class="form-group">
                                <label><i>Note: Please use the screenshot of your system for thumbnail.</i></label>
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">Thumbnail</span>
                                    </div>
                                    <div class="custom-file">
                                        <input required type="file" class="custom-file-input" name="thumbnail" id="upload" onchange="displayname(this,$(this))">
                                        <label class="custom-file-label" for="upload">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-sm btn-primary"><span class="fa fa-save"></span> Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>