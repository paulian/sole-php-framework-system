<?php
    session_start();
    $_SESSION["DB_ERR_INDEX"] = 0;
    $_SESSION["DB_ERR_DETAILS"] = [];
    $_SESSION["MIGRATION_INDEX"] = 0;
    $_SESSION["MIGRATION_INFO"] = [];
    class Data{
        public static function load($name, $data){
            $_SESSION[$name] = $data;
            return $data;
        }
        public static function unload($name){
            $data = $_SESSION[$name];
            return $data;
        }
        public static function trash($name){
            $_SESSION[$name] = "";
        }
        public static function json_response($data){
            print_r(json_encode($data));
        }
        public static function reverse($arr){
            return array_reverse($arr);
        }
        public static function shuffle($str){
            return str_shuffle($str);
        }
        public static function pick($arr){
            return $arr[rand(0,count($arr)-1)];
        }
        public static function generate($n,$type){
            $key = "";
            $case = 0;
            $letter = [
                "a","b","c","d","e","f","g","h","i","j","k","l","m",
                "n","o","p","q","r","s","t","u","v","w","x","y","z"
            ];
            $number = [
                "1","2","3","4","5","6","7","8","9"
            ];
            $symbol = [
                "`","~","!","@","#","$","%","^","&","*","(",")","-","_","=","+","[","]","{","}",";",":","'",'"',",",".","<",">","/","|","?",
            ];
            if($type == "alpha"){
                for ($i=0; $i < $n; $i++) { 
                    $case = rand(0,1);
                    if($case){
                        $key .= strtoupper($letter[rand(0,count($letter)-1)]);
                    }else{
                        $key .= strtolower($letter[rand(0,count($letter)-1)]);
                    }
                }
            }elseif($type == "numeric"){
                for ($i=0; $i < $n; $i++) { 
                    $key .= $number[rand(0,count($number)-1)];
                }
            }elseif($type == "symbol"){
                for ($i=0; $i < $n; $i++) { 
                    $key .= $symbol[rand(0,count($symbol)-1)];
                }
            }elseif($type == "alphanumeric"){
                for ($i=0; $i < $n; $i++) { 
                    $key_type = rand(0,1);
                    if($key_type){
                        $case = rand(0,1);
                        if($case){
                            $key .= strtoupper($letter[rand(0,count($letter)-1)]);
                        }else{
                            $key .= strtolower($letter[rand(0,count($letter)-1)]);
                        }
                    }else{
                        $key .= $number[rand(0,count($number)-1)];
                    }    
                }
            }elseif($type == "alphasymbol"){
                for ($i=0; $i < $n; $i++) { 
                    $key_type = rand(0,1);
                    if($key_type){
                        $case = rand(0,1);
                        if($case){
                            $key .= strtoupper($letter[rand(0,count($letter)-1)]);
                        }else{
                            $key .= strtolower($letter[rand(0,count($letter)-1)]);
                        }
                    }else{
                        $key .= $symbol[rand(0,count($symbol)-1)];
                    }    
                }
            }
            elseif($type == "numericsymbol"){
                for ($i=0; $i < $n; $i++) { 
                    $key_type = rand(0,1);
                    if($key_type){
                        $key .= $number[rand(0,count($number)-1)];
                    }else{
                        $key .= $symbol[rand(0,count($symbol)-1)];
                    }    
                }
            }
            elseif($type == "alphanumericsymbol"){
                for ($i=0; $i < $n; $i++) { 
                    $key_type = rand(0,2);
                    if($key_type == 0){
                        $case = rand(0,1);
                        if($case){
                            $key .= strtoupper($letter[rand(0,count($letter)-1)]);
                        }else{
                            $key .= strtolower($letter[rand(0,count($letter)-1)]);
                        }
                    }elseif($key_type == 1){
                        $key .= $number[rand(0,count($number)-1)];
                    }
                    elseif($key_type == 2){
                        $key .= $symbol[rand(0,count($symbol)-1)];
                    }    
                }
            }
            else{
                $key = uniqid();
            }
            return $key;
        }
    }
?>