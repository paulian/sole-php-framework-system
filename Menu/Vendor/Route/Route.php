
<?php
    class Route{
        public static function load($name){
            header('location: App/Controllers/'.$name);
        }
        public static function view($name){
            if($_SESSION['DB_ERR_DETAILS']){
                header('location: ../DB/ERR/ERR.php');
            }else{
                $arr = explode('.',$name);
                $location = "";
                for ($i=0; $i <= count($arr)-1; $i++) { 
                    $location .= '/'.$arr[$i];
                } 
                require_once("../../Resources/views".$location.".sole.php");
                Auto::index();
            }
        }
        public static function index($name){
            if($_SESSION['DB_ERR_DETAILS']){
                header('location: ../DB/ERR/ERR.php');
            }
            else{
                $_SESSION['DB_ERR_DETAILS'] = "";
                header('location: ../Controllers/'.$name);
            }
        }
        public static function return($name){
            if($_SESSION['DB_ERR_DETAILS']){
                header('location: ../DB/ERR/ERR.php');
            }
            else{
                $_SESSION['DB_ERR_DETAILS'] = "";
                $_SESSION['Route_Location'] = $name;
                header('location: ../Controllers/'.$name);
            }
        }
    }
?>