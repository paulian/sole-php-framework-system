<?php
    /**Do not interchange code arrangement!! */
    include_once('../../Vendor/Database/DC.php');
    include_once('../../Vendor/Database/DB.php');
    include_once('../Models/Models.php');
    include_once('../../Vendor/Data/Data.php');
    include_once('../../Vendor/Data/Log.php');
    include_once('../../Vendor/File Handler/File.php');
    include_once('../../Public/Load.php');
    include_once('../../Vendor/Assets Loader/Auto.php');
    include_once('../../Vendor/Route/Route.php');

    class Extend_Route{
        public static function er(){
            $func = array("store","show","update","destroy");
            $Controller = $_SERVER["PHP_SELF"];
            $arr = explode('/',$Controller);
            $arr = explode('.',end($arr));
            $Controller = $arr[0];
        
            if($_SERVER["QUERY_STRING"]){
                if(in_array($_SERVER["QUERY_STRING"],$func)){
                    $method = $_SERVER["QUERY_STRING"];
                    $Controller::$method();
                }else{
                    $method = "unknown_method_".$_SERVER["QUERY_STRING"];
                    $Controller::$method();
                }
            }else{
                $Controller::index();
            }
        }
    }
    Extend_Route::er();
?>