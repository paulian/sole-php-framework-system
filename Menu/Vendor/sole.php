<?php
    include_once('Migration/Migrate.php');
    include_once('Migration/Make.php');
    include_once('Model/Make.php');
    include_once('Controller/Make.php');
    class AppLoad{
        public static function index(){
            include('Database/DC_CLI.php');
            if($_SERVER["argc"] >= 3){
                if($_SERVER["argv"][1] == "migrate:table"){
                    if($_SERVER["argv"][2] == "json"){
                        if($DB_HOST != "frameworkhost"){
                            echo "**please configure database host to frameworkhost"."**";
                        }else{
                            echo "**This JSON database management is work in progress, it contains errors. I suggest not to use it at the current framework version.**";
                            //Migrate::json_run();    
                        }
                    }elseif($_SERVER["argv"][2] == "sql"){
                        if($DB_HOST != "localhost"){
                            echo "**please configure database host to localhost"."**";
                        }else{
                            Migrate::default_run();   
                        }
                    }else{
                        echo "**Invalid script ".$_SERVER["argv"][2]."**";   
                    }
                }elseif($_SERVER["argv"][1] == "make:controller"){
                    if($_SERVER["argc"] >= 4){
                        if($_SERVER["argv"][3] == "api"){
                            MakeController::make_api($_SERVER["argv"][2]);    
                        }else{
                            echo "**Invalid script ".$_SERVER["argv"][3]."**";
                        }
                    }else{
                        MakeController::make_default($_SERVER["argv"][2]);
                    }
                }elseif($_SERVER["argv"][1] == "make:migration"){
                    MakeMigration::make($_SERVER["argv"][2]);
                }elseif($_SERVER["argv"][1] == "make:model"){
                    MakeModel::make($_SERVER["argv"][2]);
                }elseif($_SERVER["argv"][1] == "framework"){
                    if($_SERVER["argv"][2] == "--version"){
                        echo "**Sole Framework: v$APP_FRAMEWORK_VERSION"."**";
                    }else{
                        echo "**Invalid script ".$_SERVER["argv"][2]."**";   
                    }
                }else{
                    echo "**Invalid script ".$_SERVER["argv"][1]."**";
                }
            }else{
                echo "**php sole ... (please refer to the documentation for script commands)"."**";
            }
        }
    }
    Appload::index();
?>