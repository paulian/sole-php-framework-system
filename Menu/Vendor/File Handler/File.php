<?php
    class File{
        /**
         * ---------------------------------------------------------------------
         * Upload All File Types
         * ---------------------------------------------------------------------
         */
        public static function upload_file($data,$index,$folder){
            $response = new Response;
            $response->file = "";
            $response->name = "";
            $response->size = "";
            $response->type = "";
            $response->location = "";
            $response->status = false;
            if(is_dir("../../Public/Root/File/".$folder) === false){
                mkdir("../../Public/Root/File/".$folder);
            }
            $destination = "../../Public/Root/File/".$folder."/";
            $file = $data[$index];

            $name_primary = $_FILES[$index]['name'];
            $size = $_FILES[$index]['size'];
            $tmp = $_FILES[$index]['tmp_name'];

            $ext_primary = explode('.', $name_primary);
            $ext_secondary = strtolower(end($ext_primary));
        
            $name_secondary = uniqid('', true);
            $name_secondary = $name_secondary.".".$ext_secondary;

            $imageFile = array('png','jpg','jpeg','img','ico');
            $musicFile = array('mp3','wma','ape','ogg','fla','aac','m4a','ac3','wav','wv');
            $videoFile = array('mp4','avi','mkv','mov','mpeg','webm','3gp');
            $xlsxfile = array('xls','xlsx');
            $docxFile = array('doc','docx');
            $pptxFile = array('ppt','pptx');
            $pdfFile = array('pdf');
            $compressFile = array('zip','rar','bin','iso');

            if(in_array($ext_secondary, $imageFile)){
                $type = "IMAGE";
            }else if(in_array($ext_secondary, $musicFile)){
                $type = "SOUND";
            }else if(in_array($ext_secondary, $videoFile)){
                $type = "VIDEO";
            }else if(in_array($ext_secondary, $xlsxfile)){
                $type = "XLSX";
            }else if(in_array($ext_secondary, $docxFile)){
                $type = "DOCX";
            }else if(in_array($ext_secondary, $pptxFile)){
                $type = "PPTX";
            }else if(in_array($ext_secondary, $pdfFile)){
                $type = "PDF";
            }else if(in_array($ext_secondary, $compressFile)){
                $type = "ARCHIVE";
            }else{
                $type = "FILE";
            }

            $arraycount = count($ext_primary);
            $nametemp = "";
            if($arraycount > 2){
                $i = 0;
                while($i < $arraycount-1){
                    if($nametemp == ""){
                        $nametemp = $ext_primary[$i];
                    }else{
                        $nametemp = $nametemp.'.'.$ext_primary[$i];
                    }
                    $i++;
                }
            }else{
                $nametemp = $ext_primary[0];
            }

            if($size < 40000000 /**to 40000000 */){
                $dest = $destination.$name_secondary;
                move_uploaded_file($tmp, $dest);
                $response->file = $name_secondary;
                $response->name = $nametemp.'.'.$ext_secondary;
                $response->size = $size;
                $response->type = $type;
                $response->location = $destination;
                $response->status = true;
            }
            return $response;
        }
        /**
         * ---------------------------------------------------------------------
         * Upload Image File Type
         * ---------------------------------------------------------------------
         */
        public static function upload_img($data,$index,$folder){
            $response = new Response;
            $response->file = "";
            $response->name = "";
            $response->size = "";
            $response->type = "";
            $response->location = "";
            $response->status = false;
            if(is_dir("../../Public/Root/Img/".$folder) === false){
                mkdir("../../Public/Root/Img/".$folder);
            }
            $destination = "../../Public/Root/Img/".$folder."/";
            $file = $data[$index];

            $name_primary = $_FILES[$index]['name'];
            $size = $_FILES[$index]['size'];
            $tmp = $_FILES[$index]['tmp_name'];

            $ext_primary = explode('.', $name_primary);
            $ext_secondary = strtolower(end($ext_primary));
        
            $name_secondary = uniqid('', true);
            $name_secondary = $name_secondary.".".$ext_secondary;

            $arraycount = count($ext_primary);
            $nametemp = "";
            if($arraycount > 2){
                $i = 0;
                while($i < $arraycount-1){
                    if($nametemp == ""){
                        $nametemp = $ext_primary[$i];
                    }else{
                        $nametemp = $nametemp.'.'.$ext_primary[$i];
                    }
                    $i++;
                }
            }else{
                $nametemp = $ext_primary[0];
            }

            $imageFile = array('png','jpg','jpeg','img','ico');

            if(in_array($ext_secondary, $imageFile)){
                if($size < 40000000 /**to 40000000 */){
                    $dest = $destination.$name_secondary;
                    move_uploaded_file($tmp, $dest);
                    $response->file = $name_secondary;
                    $response->name = $nametemp.'.'.$ext_secondary;
                    $response->size = $size;
                    $response->type = "IMAGE";
                    $response->location = $destination;
                    $response->status = true;
                }
            }else{
                $response->file = "";
                $response->name = $nametemp.'.'.$ext_secondary;
                $response->size = $size;
                $response->type = "INVALID";
                $response->location = "";
                $response->status = false;
            }
            return $response;
        }
        /**
         * ---------------------------------------------------------------------
         * Upload Document File Type
         * ---------------------------------------------------------------------
         */
        public static function upload_document($data,$index,$folder){
            $response = new Response;
            $response->file = "";
            $response->name = "";
            $response->size = "";
            $response->type = "";
            $response->location = "";
            $response->status = false;
            if(is_dir("../../Public/Root/Document/".$folder) === false){
                mkdir("../../Public/Root/Document/".$folder);
            }
            $destination = "../../Public/Root/Document/".$folder."/";
            $file = $data[$index];

            $name_primary = $_FILES[$index]['name'];
            $size = $_FILES[$index]['size'];
            $tmp = $_FILES[$index]['tmp_name'];

            $ext_primary = explode('.', $name_primary);
            $ext_secondary = strtolower(end($ext_primary));
        
            $name_secondary = uniqid('', true);
            $name_secondary = $name_secondary.".".$ext_secondary;

            $arraycount = count($ext_primary);
            $nametemp = "";
            if($arraycount > 2){
                $i = 0;
                while($i < $arraycount-1){
                    if($nametemp == ""){
                        $nametemp = $ext_primary[$i];
                    }else{
                        $nametemp = $nametemp.'.'.$ext_primary[$i];
                    }
                    $i++;
                }
            }else{
                $nametemp = $ext_primary[0];
            }

            $xlsxfile = array('xls','xlsx');
            $docxFile = array('doc','docx');
            $pptxFile = array('ppt','pptx');
            $pdfFile = array('pdf');

            if(in_array($ext_secondary, $xlsxfile)){
                if($size < 40000000 /**to 40000000 */){
                    $dest = $destination.$name_secondary;
                    move_uploaded_file($tmp, $dest);
                    $response->file = $name_secondary;
                    $response->name = $nametemp.'.'.$ext_secondary;
                    $response->size = $size;
                    $response->type = "XLSX";
                    $response->location = $destination;
                    $response->status = true;
                }
            }else if(in_array($ext_secondary, $docxFile)){
                if($size < 40000000 /**to 40000000 */){
                    $dest = $destination.$name_secondary;
                    move_uploaded_file($tmp, $dest);
                    $response->file = $name_secondary;
                    $response->name = $nametemp.'.'.$ext_secondary;
                    $response->size = $size;
                    $response->type = "DOCX";
                    $response->location = $destination;
                    $response->status = true;
                }
            }else if(in_array($ext_secondary, $pptxFile)){
                if($size < 40000000 /**to 40000000 */){
                    $dest = $destination.$name_secondary;
                    move_uploaded_file($tmp, $dest);
                    $response->file = $name_secondary;
                    $response->name = $nametemp.'.'.$ext_secondary;
                    $response->size = $size;
                    $response->type = "PPTX";
                    $response->location = $destination;
                    $response->status = true;
                }
            }else if(in_array($ext_secondary, $pdfFile)){
                if($size < 40000000 /**to 40000000 */){
                    $dest = $destination.$name_secondary;
                    move_uploaded_file($tmp, $dest);
                    $response->file = $name_secondary;
                    $response->name = $nametemp.'.'.$ext_secondary;
                    $response->size = $size;
                    $response->type = "PDF";
                    $response->location = $destination;
                    $response->status = true;
                }
            }else{
                $response->file = "";
                $response->name = $nametemp.'.'.$ext_secondary;
                $response->size = $size;
                $response->type = "INVALID";
                $response->location = "";
                $response->status = false;
            }
            return $response;
        }
        /**
         * ---------------------------------------------------------------------
         * Upload Sound File Type
         * ---------------------------------------------------------------------
         */
        public static function upload_sound($data,$index,$folder){
            $response = new Response;
            $response->file = "";
            $response->name = "";
            $response->size = "";
            $response->type = "";
            $response->location = "";
            $response->status = false;
            if(is_dir("../../Public/Root/Sound/".$folder) === false){
                mkdir("../../Public/Root/Sound/".$folder);
            }
            $destination = "../../Public/Root/Sound/".$folder."/";
            $file = $data[$index];

            $name_primary = $_FILES[$index]['name'];
            $size = $_FILES[$index]['size'];
            $tmp = $_FILES[$index]['tmp_name'];

            $ext_primary = explode('.', $name_primary);
            $ext_secondary = strtolower(end($ext_primary));
        
            $name_secondary = uniqid('', true);
            $name_secondary = $name_secondary.".".$ext_secondary;

            $arraycount = count($ext_primary);
            $nametemp = "";
            if($arraycount > 2){
                $i = 0;
                while($i < $arraycount-1){
                    if($nametemp == ""){
                        $nametemp = $ext_primary[$i];
                    }else{
                        $nametemp = $nametemp.'.'.$ext_primary[$i];
                    }
                    $i++;
                }
            }else{
                $nametemp = $ext_primary[0];
            }

            $musicFile = array('mp3','wma','ape','ogg','fla','aac','m4a','ac3','wav','wv');

            if(in_array($ext_secondary, $musicFile)){
                if($size < 40000000 /**to 40000000 */){
                    $dest = $destination.$name_secondary;
                    move_uploaded_file($tmp, $dest);
                    $response->file = $name_secondary;
                    $response->name = $nametemp.'.'.$ext_secondary;
                    $response->size = $size;
                    $response->type = "SOUND";
                    $response->location = $destination;
                    $response->status = true;
                }
            }else{
                $response->file = "";
                $response->name = $nametemp.'.'.$ext_secondary;
                $response->size = $size;
                $response->type = "INVALID";
                $response->location = "";
                $response->status = false;
            }
            return $response;
        }
        /**
         * ---------------------------------------------------------------------
         * Upload Video File Type
         * ---------------------------------------------------------------------
         */
        public static function upload_video($data,$index,$folder){
            $response = new Response;
            $response->file = "";
            $response->name = "";
            $response->size = "";
            $response->type = "";
            $response->location = "";
            $response->status = false;
            if(is_dir("../../Public/Root/Video/".$folder) === false){
                mkdir("../../Public/Root/Video/".$folder);
            }
            $destination = "../../Public/Root/Video/".$folder."/";
            $file = $data[$index];

            $name_primary = $_FILES[$index]['name'];
            $size = $_FILES[$index]['size'];
            $tmp = $_FILES[$index]['tmp_name'];

            $ext_primary = explode('.', $name_primary);
            $ext_secondary = strtolower(end($ext_primary));
        
            $name_secondary = uniqid('', true);
            $name_secondary = $name_secondary.".".$ext_secondary;

            $arraycount = count($ext_primary);
            $nametemp = "";
            if($arraycount > 2){
                $i = 0;
                while($i < $arraycount-1){
                    if($nametemp == ""){
                        $nametemp = $ext_primary[$i];
                    }else{
                        $nametemp = $nametemp.'.'.$ext_primary[$i];
                    }
                    $i++;
                }
            }else{
                $nametemp = $ext_primary[0];
            }

            $videoFile = array('mp4','avi','mkv','mov','mpeg','webm','3gp');

            if(in_array($ext_secondary, $videoFile)){
                if($size < 40000000 /**to 40000000 */){
                    $dest = $destination.$name_secondary;
                    move_uploaded_file($tmp, $dest);
                    $response->file = $name_secondary;
                    $response->name = $nametemp.'.'.$ext_secondary;
                    $response->size = $size;
                    $response->type = "VIDEO";
                    $response->location = $destination;
                    $response->status = true;
                }
            }else{
                $response->file = "";
                $response->name = $nametemp.'.'.$ext_secondary;
                $response->size = $size;
                $response->type = "INVALID";
                $response->location = "";
                $response->status = false;
            }
            return $response;
        }
        /**
         * ---------------------------------------------------------------------
         * Upload Archive File Type
         * ---------------------------------------------------------------------
         */
        public static function upload_archive($data,$index,$folder){
            $response = new Response;
            $response->file = "";
            $response->name = "";
            $response->size = "";
            $response->type = "";
            $response->location = "";
            $response->status = false;
            if(is_dir("../../Public/Root/Archive/".$folder) === false){
                mkdir("../../Public/Root/Archive/".$folder);
            }
            $destination = "../../Public/Root/Archive/".$folder."/";
            $file = $data[$index];

            $name_primary = $_FILES[$index]['name'];
            $size = $_FILES[$index]['size'];
            $tmp = $_FILES[$index]['tmp_name'];

            $ext_primary = explode('.', $name_primary);
            $ext_secondary = strtolower(end($ext_primary));
        
            $name_secondary = uniqid('', true);
            $name_secondary = $name_secondary.".".$ext_secondary;

            $arraycount = count($ext_primary);
            $nametemp = "";
            if($arraycount > 2){
                $i = 0;
                while($i < $arraycount-1){
                    if($nametemp == ""){
                        $nametemp = $ext_primary[$i];
                    }else{
                        $nametemp = $nametemp.'.'.$ext_primary[$i];
                    }
                    $i++;
                }
            }else{
                $nametemp = $ext_primary[0];
            }

            $compressFile = array('zip','rar','bin','iso');

            if(in_array($ext_secondary, $compressFile)){
                if($size < 40000000 /**to 40000000 */){
                    $dest = $destination.$name_secondary;
                    move_uploaded_file($tmp, $dest);
                    $response->file = $name_secondary;
                    $response->name = $nametemp.'.'.$ext_secondary;
                    $response->size = $size;
                    $response->type = "ARCHIVE";
                    $response->location = $destination;
                    $response->status = true;
                }
            }else{
                $response->file = "";
                $response->name = $nametemp.'.'.$ext_secondary;
                $response->size = $size;
                $response->type = "INVALID";
                $response->location = "";
                $response->status = false;
            }
            return $response;
        }
        /**
         * ---------------------------------------------------------------------
         * Download All File Types(Work In Progress)
         * inbox me for updates @ paulian.dumdum@gmail.com
         * ---------------------------------------------------------------------
         */
        /*
        public static function download_file($file,$name,$folder){
            echo $file;
            echo $name;
            echo $folder;

            rename("../Public/Root/File/".$folder."/".$file, "../Public/Root/File/".$folder."/".$name);

            $destination = "../Public/Root/File/".$folder."/" . $name;
            file_exists($destination);
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename=' . basename($destination));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize("../Public/Root/File/".$folder."/" . $name));
            readfile("../Public/Root/File/".$folder."/". $name);

            rename("../Public/Root/File/".$folder."/".$name, "../Public/Root/File/".$folder."/".$file);
        }
         */
    }
    class Response{
        public $response = [];
    }
?>