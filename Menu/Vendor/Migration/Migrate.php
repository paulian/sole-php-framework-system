<?php
    include_once('Vendor/Database/DB.php');
    include_once('Vendor/Migration/Migrate.php');
    include_once('App/Migrations/Migration Models.php');
    class Migrate{
        public static $stat = false;
        public static function default_run(){
            for ($i=0; $i < count(Migrate::$migration); $i++) { 
                Migrate::$migration[$i]::index();
                Migrate::id();
                Migrate::timestamp();
                Migrate::commit();
            }
            if(Migrate::$stat){
                echo "**Nothing to migrate**
**Note: to migrate to remaining tables, please remove the model's class name that has already been migrated from the migration array (this only applies to sql database).**
";
            }
        }
        public static function commit(){
            try{
                include('Vendor/Database/DC_CLI.php');
                $SEARCH = false;
                $SQL = $DB_CONN->prepare("SHOW TABLES");
                $SQL->execute();
                $fillable = $SQL->fetchAll(PDO::FETCH_ASSOC);
                foreach($fillable as $a){
                    if($a["Tables_in_".$DB_DATABASE] == "migrations"){
                        $SEARCH = true;
                    }
                }
                if(!$SEARCH){
                    try{
                        $MIGRATION_TABLE = "migrations";
                        $MIGRATION_BLUEPRINT = "`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT, `migration` VARCHAR(255) COLLATE utf8mb4_unicode_ci NOT NULL";
                        $SQL = "CREATE TABLE `$DB_DATABASE`.`$MIGRATION_TABLE` ($MIGRATION_BLUEPRINT , PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci";
                        $DB_CONN->exec($SQL);
                    }catch(PDOException $e){
                        echo "**Migration failed: " . $e->getMessage()."**
";
                    }
                }
                $TABLE = Migrate::$table;
                $BLUEPRINT = Migrate::$attribute;
                $SQL = "CREATE TABLE `$DB_DATABASE`.`$TABLE` ($BLUEPRINT) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci";
                $DB_CONN->exec($SQL);
                $DATE = new DateTime();
                $VALUES = $DATE->format('Y')."_".$DATE->format('d')."_".$DATE->format('m')."_".$DATE->format('u')."_".Migrate::$table;
                $SQL = "INSERT INTO `migrations` (`migration`) VALUES ('$VALUES')";
                $DB_CONN->exec($SQL);
                Migrate::$attribute = "";
                Migrate::$table = "";
                
                echo('**Migrated::'.$TABLE."**
");
            }catch(PDOException $e){
                Migrate::$stat = true;
            }
        }
        public static function attrib_table($name){
            Migrate::$table = $name;
            Migrate::$table_json = $name;
        }
        public static function attrib_string($num){
            Migrate::$string = $num;
        }
        public static function id(){
            Migrate::$attribute = "`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT ".Migrate::$attribute;
            Migrate::$attribute_json = array_reverse(Migrate::$attribute_json);
            array_push(Migrate::$attribute_json,"id");
            Migrate::$attribute_json = array_reverse(Migrate::$attribute_json);
        }
        public static function timestamp(){
            Migrate::$attribute .= " , `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`id`)";
            array_push(Migrate::$attribute_json,"created_at");
            array_push(Migrate::$attribute_json,"updated_at");
        }
        public static function string($name){
            Migrate::$attribute .= " , `".$name."` VARCHAR(".Migrate::$string.") COLLATE utf8mb4_unicode_ci NOT NULL ";
            array_push(Migrate::$attribute_json,$name);
        }
        public static function date($name){
            Migrate::$attribute .= " , `".$name."` DATETIME NOT NULL";
            array_push(Migrate::$attribute_json,$name);
        }
        public static $migration = [];
        public static $attribute = "";
        public static $attribute_json = [];
        public static $table = "";
        public static $table_json = "";
        public static $string = 255;

        public static function json_run(){
            include('Vendor/Database/DC_CLI.php');
            if(is_dir("Vendor/Database/JSON.DB/".$DB_DATABASE) === false){
                mkdir("Vendor/Database/JSON.DB/".$DB_DATABASE);
            }
            for ($i=0; $i < count(Migrate::$migration); $i++) { 
                Migrate::$migration[$i]::index();
                Migrate::id();
                Migrate::timestamp();
                Migrate::commit_json($DB_DATABASE);
            }
        }
        public static function commit_json($DB_DATABASE){
            $json = [
                "table" => Migrate::$table_json,
                "attribute" => Migrate::$attribute_json,
                "index" => 0,
                "data" => [],
            ];
            $file = Migrate::$table_json.".json";
            if(is_file("Vendor/Database/JSON.DB/".$DB_DATABASE."/".$file) === false){
                $file_write = fopen("Vendor/Database/JSON.DB/".$DB_DATABASE."/".$file,"w");
                fwrite($file_write,json_encode($json));    
            }
            echo('**Migrated::'.Migrate::$table_json."**
");
            Migrate::$attribute_json = [];
            Migrate::$table_json = [];
        }
    }
?>