<?php
    class MakeMigration{
        public static function make($m){
            $migration = file_get_contents("App/Migrations/Migration Models.php");
            $res = explode('
',$migration);
            $a = explode(' = ',$res[1]);
            
            $b = str_replace(['["','"];','","','[','];'],['','','/','',''],$a);
            $c = explode("/",$b[1]);
            if($b[1]==""){
                $c = [];   
            }
            if(!in_array($m,$c)){
                array_push($c,$m);

                $mtemp = '    Migrate::$migration = ["';
                for ($i=0; $i < count($c); $i++) { 
                    $mtemp .= $c[$i];
                    if($i==count($c)-1){
                        $mtemp .= '"';    
                    }else{
                        $mtemp .= '","';    
                    }
                }
                $mtemp .= '];'; 
                $res[1] = $mtemp;
                array_pop($res);
                $restemp = '';

                for ($i=0; $i < count($res); $i++) { 
                    $restemp .= $res[$i];
                    $restemp .= '
';
                    if($i == count($res)-1){
                        $restemp .= '
    class '.$m.'
    {
        public static function index(){
            Migrate::attrib_table("'.strtolower($m).'");
            Migrate::attrib_string(255);
        }
    }';
                    }
                }
                $restemp .= '
?>';
                file_put_contents("App/Migrations/Migration Models.php",$restemp);   
                echo("**Migration ".$m." created successfully"."**");
            }else{
                echo('**Migration already exist**');
            }
            
        }
    }
?>