<?php
    class MakeController{
        public static function make_default($controller){
            if(!is_file("App/Controllers/".$controller.".php")){
                $file = $controller.".php";
                $content = "<?php
    include('../../Vendor/Invoker/invoke.php');
    class ".$controller."{
        public static function index(){
            //code here...
        }
        public static function store(){
            //code here...
        }
        public static function show(){
            //code here...
        }
        public static function update(){
            //code here...
        }
        public static function destroy(){
            //code here...
        }
    }
?>";
                $system = fopen("App/Controllers/".$file,"w");
                fwrite($system,$content);
                echo("**".$controller." created successfully"."**");
            }else{
                echo("**".$controller." already exist"."**");
            }
        }
        public static function make_api($controller){
            if(!is_file("App/Controllers/".$controller.".php")){
                $file = $controller.".php";
                $content = "<?php
    include('../../Vendor/Invoker/invoke.api.php');
    class ".$controller."{
        public static function index(){
            //code here...
        }
        public static function store(){
            //code here...
        }
        public static function show(){
            //code here...
        }
        public static function update(){
            //code here...
        }
        public static function destroy(){
            //code here...
        }
    }
?>";
                $system = fopen("App/Controllers/".$file,"w");
                fwrite($system,$content);
                echo("**".$controller." created successfully"."**");
            }else{
                echo("**".$controller." already exist"."**");
            }
        }
    }
?>