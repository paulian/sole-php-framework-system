<?php
    class Load{
        public static $CSS = [
            "addons/css/bootstrap.min.css",
            "addons/css/font-awesome-4.7.0/css/font-awesome.min.css",
            "addons/css/styless.css",
        ];
        public static $JS = [
            "addons/js/jquery-3.3.1.min.js",
            "addons/js/bootstrap.min.js",
            "addons/js/popper.min.js",
            "addons/js/script.js",
        ];    
    }
    
?>