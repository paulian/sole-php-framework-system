function displayname(input,_this) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            _this.siblings('label').html(input.files[0]['name'])
                    
        }
        reader.readAsDataURL(input.files[0]);
    }
}
function search(name){
    fetch("SearchController", {
        method: 'POST',
        body: new URLSearchParams('name=' + name)
    })
    .then(res => res.json())
    .then(res => list(res))
}
function list(res){
    document.getElementById("suggest").remove();
    var a = document.getElementById("searchSuggest");
    a.insertAdjacentHTML('afterbegin','<div id="suggest"></div>');
    var b = document.getElementById("suggest");
    res.forEach(r => {
        b.insertAdjacentHTML('afterbegin','<input onclick="pass(this.value)" value="'+r["name"]+'" class="form-control">');
    });
    
}
function pass(name){
    document.getElementById("searchname").value = name;
    document.getElementById("suggest").remove();
    var a = document.getElementById("searchSuggest");
    a.insertAdjacentHTML('afterbegin','<div id="suggest"></div>');
}
function clean(){
    document.getElementById("suggest").remove();
    var a = document.getElementById("searchSuggest");
    a.insertAdjacentHTML('afterbegin','<div id="suggest"></div>');
}